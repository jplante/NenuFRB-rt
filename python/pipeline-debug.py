#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
import matplotlib.transforms as mtransforms
import tqdm
import pathlib

# Used to process the output of ./test/pipeline --N_f=192 --N_dm=1024 --subbandList=204,396 ~/Downloads/B0809+74_TRACKING_20220228_154637_2_166_169.dump
# TODO: less hardcoding

dt = 1024 / 200e6


def mad_clean(I, threshold, median=None, mad=None):
    if median is None:
        median = np.median(I)
    if mad is None:
        mad = np.median(np.abs(I - median))

    print(f"median: {median}, mad: {mad}")
    return np.clip(I, median - threshold * mad, median + threshold * mad)

def rm_baseline(I, median=None):
    means = I.mean(axis=1)[:, np.newaxis]

    if median is None:
        median = np.median(I)

    return (I - means) * (median / means) + median

def BruteforceDmt(I, fmin, fmax, DMmin, DMmax, N_dm, tstart, tstop, N_t_out, n_step=1024):
    transMat2I = mtransforms.Affine2D()
    transMat2I.rotate(np.pi/2)
    transMat2I.scale(-1, 1)
    transMat2I.scale(tstop / I.shape[1],
                        (fmax - fmin) / I.shape[0])
    transMat2I.translate(0, fmin)

    transI2Mat = transMat2I.inverted()

    N_f, N_t_in = I.shape
    A = np.empty((N_dm, N_t_out), dtype=np.float32)
    x = np.arange(1024)
    f = fmin + (fmax - fmin) / x.size * x

    dDM = (DMmax - DMmin) / N_dm
    dt = (tstop - tstart) / N_t_out

    for i_dm in tqdm.trange(N_dm):
        for i_t in range(N_t_out):
            t = tstart + dt * i_t + Delta(f, fmax, DMmin + i_dm * dDM)

            X = np.vstack((t, f)).T
            idx = transI2Mat.transform(X).astype(int).T

            idx = idx[:, (0 <= idx[0, :]) & (idx[0, :] < I.shape[0]) & (0 <= idx[1, :]) & (idx[1, :] < I.shape[1])]

            A[i_dm, i_t] = I[tuple(idx)].sum()
    return A
            


fold_factor = 1024

# raw = np.fromfile("/home/nenufarobs/Downloads/B0809+74_TRACKING_20220228_154637_2_166_169.dump", dtype=np.complex64).reshape(192, -1)
# raw = raw.real**2 + raw.imag**2

# I_cleaned = raw[..., :(raw.shape[1] // fold_factor) * fold_factor]
# I_cleaned = I_cleaned.reshape(I_cleaned.shape[0], -1, fold_factor).mean(axis=-1)
# 
# I_cleaned = mad_clean(I_cleaned, 10)
# I_cleaned = rm_baseline(I_cleaned)
# I_cleaned = mad_clean(I_cleaned, 3)
# I_cleaned = rm_baseline(I_cleaned)

I = np.fromfile("pipeline-I.dump", dtype=np.float32).reshape(192, -1)
A = np.fromfile("pipeline-A.dump", dtype=np.float32).reshape(1024, -1)

Record = np.dtype([('i_h', np.int32), ('i_w', np.int32), ('snr', np.float32)])
candidates = np.fromfile("pipeline-candidates.dump", dtype=Record)

dt_post_preprocessing = dt * fold_factor
tstart = (I.shape[1] - A.shape[1]) * dt_post_preprocessing
tstop  = tstart + A.shape[1] * dt_post_preprocessing
fmin = 204 / dt / 1e6
fmax = 396 / dt / 1e6
DMmin  = 0
DMmax  = 100

max2d = np.unravel_index(A.argmax(), A.shape)

transMat2I = mtransforms.Affine2D()
transMat2I.rotate(np.pi/2)
transMat2I.scale(-1, 1)
transMat2I.scale((tstop - tstart) / I.shape[1],
                    (fmax - fmin) / I.shape[0])
transMat2I.translate(tstart, fmin)

transMat2A = mtransforms.Affine2D()
transMat2A.rotate(np.pi/2)
transMat2A.scale(-1, 1)
transMat2A.scale((tstop - tstart) / A.shape[1],
                    (DMmax - DMmin) / A.shape[0])
transMat2A.translate(tstart, DMmin)

max2d_data = transMat2A.transform(max2d)


def Delta(fmin, fmax, DM):
    k_DM = 4149

    return k_DM * DM * (fmin**(-2) - fmax**(-2))

t0 = 1.20
DM = 5.8538

x = np.arange(1024)
f = fmin + (fmax - fmin) / x.size * x

# fig, ax = plt.subplots()
# im = ax.imshow(I_cleaned, origin="lower", aspect="auto", norm=mcolors.LogNorm(), interpolation="none")
# plt.colorbar(im)
# ax.set_title("ref")

# fig, ax = plt.subplots()
# im = ax.imshow(np.abs(I_cleaned - I), origin="lower", aspect="auto", norm=mcolors.LogNorm(), interpolation="none")
# plt.colorbar(im)
# ax.set_title("|ref - out|")
# fig.savefig("pipeline-debug-diff.png", dpi=300)

# fig, ax = plt.subplots()
# im = ax.imshow(np.abs(raw - I), origin="lower", aspect="auto", norm=mcolors.LogNorm(), interpolation="none")
# plt.colorbar(im)
# ax.set_title("|raw - out|")


# A_ref_fname = pathlib.Path("A_ref.npz")
# if A_ref_fname.exists():
#     A_ref = np.load(A_ref_fname)["A_ref"]
# else:
#     A_ref = BruteforceDmt(I, fmin, fmax, DMmin, DMmax, 1024, tstart, tstop, A.shape[1], 1024)
#     np.savez(A_ref_fname, A_ref=A_ref)



t = np.linspace(tstart, tstop, A.shape[1])
i_ref, j_ref = transMat2A.inverted().transform((t0, DM)).astype(int)

def sliding_mean_std(X, S):
    n_rows = X.shape[1] - S + 1
    N, n = X.strides
    x2D = np.lib.stride_tricks.as_strided(X, shape=(X.shape[0], n_rows, S), strides=(N, n, n), writeable=False)

    return x2D.mean(axis=-1), x2D.std(axis=-1)

# Ameans, Astds = sliding_mean_std(A, S)
# Astds = A.std(axis=1, keepdims=True)
Astds = np.sqrt((A**2).mean(axis=1, keepdims=True))
# Astds = np.median(np.abs(A), axis=1, keepdims=True)

threshold = 3
def slice_plot(idx, plot_fft=False):

    def plot_time_slice(ax, plot_savgol=False):
        ax.plot(t, A[idx, :])
        ax.axvline(t0, linestyle='--', color="r")
        ax.axhline( threshold * Astds[idx, 0], color="g")
        ax.axhline(-threshold * Astds[idx, 0], color="g")
    
        if plot_savgol:
            from scipy.signal import savgol_filter
            savgol = savgol_filter(A[idx, :], 51, 3, mode="constant")
            ax.plot(t, savgol, color="r")
        
        ax.set_xlabel(f"$t$ (s), dt = {dt_post_preprocessing:.2g} s")
        ax.set_ylabel(rf"$A_{{DM={DMmin + idx * (DMmax - DMmin) / A.shape[0]} \: pc.cm^{{-3}}}}$")
    
    def plot_fft_slice(ax):
        fft = np.fft.fft(A[idx, :])
        fft = np.fft.fftshift(fft)
        
        ax.plot(fft.real**2 + fft.imag**2)
    
    if plot_fft:
        fig, axs = plt.subplots(2, 1)
        plot_time_slice(axs[0])
        plot_fft_slice(axs[1])
    else:
        fig, ax = plt.subplots(1, 1)
        plot_time_slice(ax)

    return fig

fig = slice_plot(i_ref)
fig.savefig("pipeline-debug-slice.png", dpi=300)

fig = slice_plot(max2d[0])
fig.savefig("pipeline-debug-slice-max.png", dpi=300)

fig = slice_plot(-16)
fig.savefig("pipeline-debug-slice--16.png", dpi=300)


# Ts, DMs = np.meshgrid(np.linspace(tstart, tstop, A.shape[1]), np.linspace(DMmin, DMmax, A.shape[0]))
# fig, ax = plt.subplots()
# ax.contour(Ts, A, DMs, levels=100)
# fig.savefig("pipeline-debug-contour.png", dpi=300)
# 
# np.savez("pipeline-debug-A-surface.npz", Ts=Ts, DMs=DMs, A=A)
# plt.show()

fig, ax = plt.subplots()
hits = np.where(A >= threshold * Astds)
snr  = A[hits] / Astds[hits[0]].flatten()
Hits = np.full_like(A, np.nan)
Hits[hits] = snr
fig, ax = plt.subplots()
im = ax.imshow(Hits, origin="lower", aspect="auto", interpolation="none", extent=(tstart, tstop, DMmin, DMmax))
plt.colorbar(im)
fig.savefig("pipeline-debug-Hits-snr.png", dpi=300)

if np.asarray(hits).size:
    detection_idx = np.asarray(hits).T[np.argmax(snr)]
    detection = transMat2A.transform(detection_idx)

    fig, ax = plt.subplots()
    im = ax.scatter(*transMat2A.transform(np.asarray(hits).T).T, s=0.25, c=snr)
    ax.scatter(t0, DM, marker="*", s=0.5, color=(1, 0, 0, 0.5), label=f"target")
    ax.scatter(*detection, marker="*")
    plt.colorbar(im)
    fig.savefig("pipeline-debug-hits-snr.png", dpi=500)
else:
    detection_idx = None
    detection = None

fig, ax = plt.subplots()
pipeline_hits_full = candidates[~np.isnan(candidates["snr"])]
pipeline_hits = np.empty((2, pipeline_hits_full.size))
pipeline_hits[0, :] = pipeline_hits_full["i_h"]
pipeline_hits[1, :] = pipeline_hits_full["i_w"]
pipeline_snr = pipeline_hits_full["snr"]

if pipeline_hits.size:
    im = ax.scatter(*transMat2A.transform(pipeline_hits.T).T, s=0.25, c=pipeline_snr)
    pipeline_detection = transMat2A.transform(pipeline_hits.T[np.argmax(pipeline_snr)])
    ax.scatter(t0, DM, marker="*", label=f"target: {A[i_ref, j_ref] / Astds[i_ref, 0]:.2f}")
    ax.scatter(*pipeline_detection, marker="*", label=f"detection: {A[tuple(detection_idx)] / Astds[detection_idx[0], 0]:.2f}")
    ax.set_xlim(0, tstop)
    ax.set_ylim(DMmin, DMmax)
    ax.set_xlabel(f"$t$ (s), dt = {dt_post_preprocessing:.2g} s")
    ax.set_ylabel("$DM$ (pc.cm$^{-3}$)")
    ax.set_title(f"A (thresholded, min_snr={threshold})")
    ax.legend()
    plt.colorbar(im)
    fig.savefig("pipeline-debug-hits-snr-2.png", dpi=500)
else:
    pipeline_detection = None


figsize = [3, 2.5]
fig, ax = plt.subplots(figsize=figsize, constrained_layout=True)
im = ax.imshow(I,
        origin="lower",
        aspect="auto",
        norm=mcolors.Normalize(), # mcolors.SymLogNorm(1e-1),
        interpolation="none",
        extent=(0, I.shape[1] * dt * fold_factor, fmin, fmax))
plt.colorbar(im)
ax.autoscale(False)

# t_ref = t0 + Delta(f, fmax, DM)
# ax.plot(t_ref, f, color="k", linestyle=(0, (5, 25)), label="ref")
# if detection is not None:
#     t_max = detection[0] + Delta(f,  fmax, detection[1])
#     ax.plot(t_max, f, color="r", linestyle=(0, (5, 25)), label="detection")
# ax.legend()

# ax.set_title("I")
# ax.set_xlabel(f"$t$ (s)") #, dt = {dt_post_preprocessing*1e3:.2f} ms")
# ax.set_ylabel("$f$ (MHz)")
fig.savefig("pipeline-debug-I_cleaned.eps", dpi=300, transparent=True)


fig, ax = plt.subplots(figsize=[5,4], layout="constrained")
im = ax.imshow(A, origin="lower", aspect="auto", norm=mcolors.SymLogNorm(1e-2), interpolation="none", extent=(tstart, tstop, DMmin, DMmax))
ax.autoscale(False)
# plt.colorbar(im)
# ax.scatter(t0, DM, marker="*", label=f"target: {A[i_ref, j_ref] / Astds[i_ref, 0]:.2f}")
# if detection is not None:
#     ax.scatter(*detection, marker="*", label=f"detection: {A[tuple(detection_idx)] / Astds[detection_idx[0], 0]:.2f}")
# ax.legend()
# ax.set_title("A")
# ax.set_xlabel("$t$ (s)")
# ax.set_ylabel("$DM$ (pc.cm$^{-3}$)")
fig.savefig("pipeline-debug-A.png", dpi=300)
