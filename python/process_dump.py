#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl
import matplotlib.colors as mcolors
import matplotlib.dates as mdates
import matplotlib.transforms as mtransforms
import argparse
import pathlib
import configparser
import dataclasses
import datetime
import re
import logging

import pandas as pd
import sklearn.linear_model
import sklearn.cluster
import scipy.stats


logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
formatter = logging.Formatter("%(name)s - %(levelname)s - %(message)s")
ch.setFormatter(formatter)
logger.addHandler(ch)


Record = np.dtype([('i_h', np.int32), ('i_w', np.int32), ('snr', np.float32)])
tight_loop_stat = np.dtype([
    ("t_time_integration_ms", np.float32),
])

med_loop_stat = np.dtype([
    ("t_mad_clean_10_ms",     np.float32),
    ("t_rm_baseline_0_ms",    np.float32),
    ("t_mad_clean_3_ms",      np.float32),
    ("t_rm_baseline_1_ms",    np.float32),
    ("t_rm_mean_ms",          np.float32),
])

loose_loop_stat = np.dtype([
    ("t_dedispersion_ms", np.float32),
    ("t_detection_ms",    np.float32),
])


def read_parset(file: pathlib.Path):
    with file.open('r') as f:
        config_string = '[dummy_section]\n' + f.read()
    config_NenuFAR = configparser.ConfigParser()
    config_NenuFAR.read_string(config_string)
    
    return config_NenuFAR['dummy_section']

@dataclasses.dataclass
class Beam:
    target: str                   = dataclasses.field(default_factory=str)
    toDo: str                     = dataclasses.field(default_factory=str)
    parameters: str               = dataclasses.field(default_factory=str)
    noBeam: int                   = dataclasses.field(default_factory=int)
    angle1: float                 = dataclasses.field(default_factory=float) 
    angle2: float                 = dataclasses.field(default_factory=float)
    directionType: str            = dataclasses.field(default_factory=str)
    startTime: datetime.datetime  = dataclasses.field(default=datetime.datetime.fromtimestamp(0))
    duration: int                 = dataclasses.field(default=0)
    subbandList: range            = dataclasses.field(default=range(0))
    

@dataclasses.dataclass
class NenuFRBConfig:
    N_T_RAW: int                  = dataclasses.field(default=0)
    N_T_TIGHT_LOOP: int           = dataclasses.field(default=0)
    N_T_MEDIUM_LOOP: int          = dataclasses.field(default=0)
    N_T_LOOSE_LOOP: int           = dataclasses.field(default=0)
    N_T_OUT: int                  = dataclasses.field(default=0)
    N_DM: int                     = dataclasses.field(default=0)
    N_CANDIDATES: int             = dataclasses.field(default=0)

    FFTLEN: int                   = dataclasses.field(default=0)
    TIME_INTEGRATION: int         = dataclasses.field(default=0)
    FOLD_FACTOR: int              = dataclasses.field(default=0)

    DT_RAW: float                 = dataclasses.field(default=0.0)
    DT: float                     = dataclasses.field(default=0.0)
    DF_RAW: float                 = dataclasses.field(default=0.0)
    DF: float                     = dataclasses.field(default=0.0)

    DMmin: float                  = dataclasses.field(default=0.0)
    DMmax: float                  = dataclasses.field(default=0.0)

    @classmethod
    def from_file(cls, file):
        parset = read_parset(file)

        cfg = cls()
        
        for kv in parset.items():
            match kv:
                case ["buffers.n_t_raw", value]:
                    cfg.N_T_RAW = int(value)
                case ["buffers.n_t_tight_loop", value]:
                    cfg.N_T_TIGHT_LOOP = int(value)
                case ["buffers.n_t_medium_loop", value]:
                    cfg.N_T_MEDIUM_LOOP = int(value)
                case ["buffers.n_t_loose_loop", value]:
                    cfg.N_T_LOOSE_LOOP = int(value)
                case ["buffers.n_t_out", value]:
                    cfg.N_T_OUT = int(value)
                case ["buffers.n_dm", value]:
                    cfg.N_DM = int(value)
                case ["buffers.n_candidates", value]:
                    cfg.N_CANDIDATES = int(value)
            
                case ["preprocessing.fftlen", value]:
                    cfg.FFTLEN = int(value)
                case ["preprocessing.time_integration", value]:
                    cfg.TIME_INTEGRATION = int(value)
                case ["preprocessing.fold_factor", value]:
                    cfg.FOLD_FACTOR = int(value)
            
                case ["resolution.dt_raw", value]:
                    cfg.DT_RAW = float(value)
                case ["resolution.dt", value]:
                    cfg.DT = float(value)
                case ["resolution.df_raw", value]:
                    cfg.DF_RAW = float(value)
                case ["resolution.df", value]:
                    cfg.DF = float(value)

                case ["dmt.dmmin", value]:
                    cfg.DMmin = float(value)
                case ["dmt.dmmax", value]:
                    cfg.DMmax = float(value)
                
                case [other_key, value]:
                    logger.warn(f"Unknown key: {other_key}")

        
        return cfg

def get_n_elems(parset, elem: str):
    elems = filter(lambda str: str.startswith(elem), parset.keys())
    roots = set(map(lambda str: str.split('.')[0], elems))

    return len(roots)

def parse_beams(parset):
    n_beams = get_n_elems(parset, "beam")
    beams = np.empty((n_beams,), dtype=object)
    for i in range(n_beams):
        beams[i] = Beam()

    items = filter(lambda kv: kv[0].startswith("beam"), parset.items())

    beam_number_extractor = re.compile(r'beam\[(\d+)\]')
    range_extractor = re.compile(r'\[(\d+)\.{2}(\d+)\]')
    for i in items:
        k = i[0].split('.')
        beam_idx = int(beam_number_extractor.match(k[0]).group(1))

        match k[1]:
            case "target":
                beams[beam_idx].target = i[1]
            case "todo":
                beams[beam_idx].toDo = i[1]
            case "parameters":
                beams[beam_idx].parameters = i[1]
            case "nobeam":
                beams[beam_idx].noBeam = int(i[1])
            case "angle1":
                beams[beam_idx].angle1 = float(i[1])
            case "angle2":
                beams[beam_idx].angle2 = float(i[1])
            case "directiontype":
                beams[beam_idx].directionType = i[1]
            case "starttime":
                beams[beam_idx].startTime = datetime.datetime.strptime(i[1], "%Y-%m-%dT%H:%M:%SZ")
            case "duration":
                beams[beam_idx].duration = int(i[1])
            case "subbandlist":
                range_match = range_extractor.match(i[1])
                beams[beam_idx].subbandList = range(int(range_match.group(1)), int(range_match.group(2))+1)

    return beams


def read_data(dump, shape, block_slice=None, dtype=np.float32):
    if not isinstance(dtype, np.dtype):
        dtype = np.dtype(dtype)

    dump = pathlib.Path(dump)
    dump_size = dump.stat().st_size
    n_blocks  = dump_size // (np.product(shape) * dtype.itemsize)
    data = np.memmap(dump, dtype=dtype, shape=(n_blocks, *shape), mode='r')

    logger.debug(f"Found {n_blocks} blocks of data")

    if block_slice is not None:
        data = data[block_slice, ...]

    axes = tuple(range(1, data.ndim-1)) + (0, data.ndim-1) # Move the chunk axis next to time axis
    data = np.ascontiguousarray(data.transpose(axes))
    data.shape = (*data.shape[:-2], data.shape[-2] * data.shape[-1])

    return data


def plot_hm(data, extent=None, norm=None):
    fig, ax = plt.subplots(1, 1)
    im = ax.imshow(data, origin="lower",
            norm=norm,
            aspect="auto",
            extent=extent,
            interpolation="none")
    plt.colorbar(im)
    return fig, ax


def make_locator_formatter():
    locator = mdates.AutoDateLocator()
    locator.intervald[mdates.SECONDLY] = [10, 30]
    formatter = mdates.ConciseDateFormatter(locator,
        formats=['%Y', '%b', '%d', '%H:%M', '%H:%M', '%X'],
        offset_formats=['', '%Y', '%Y-%b', '%Y-%b-%d', '%Y-%b-%d', '%Y-%b-%d'])
    return locator, formatter

def plot_raw(raw_dump_file: pathlib.Path, beam: Beam, cfg: NenuFRBConfig):
    N_f = len(beam.subbandList)
    N_t = cfg.N_T_RAW

    data = read_data(raw_dump_file, (N_f, N_t), dtype=np.complex64)
    data = data.real**2 + data.imag**2

    fig, ax = plot_hm(data, norm=mcolors.LogNorm())
    return fig, ax

def plot_I(I_dump_file: pathlib.Path, beam: Beam, cfg: NenuFRBConfig, block_slice, norm=None, timestamps=None):
    N_f = len(beam.subbandList) * cfg.FFTLEN
    N_t = cfg.N_T_LOOSE_LOOP
  
    data = read_data(I_dump_file, (N_f, N_t), block_slice)

    tstart = 0
    tstop  = data.shape[-1] * cfg.DT

    if timestamps is not None:
        tstart = timestamps[0] + datetime.timedelta(seconds=tstart)
        tstop  = timestamps[0] + datetime.timedelta(seconds=tstop)


    fmin = beam.subbandList.start * cfg.DF_RAW / 1e6
    fmax = beam.subbandList.stop * cfg.DF_RAW / 1e6

    fig, ax = plot_hm(data, extent=(tstart, tstop, fmin, fmax), norm=norm)
    ax.set_xlabel("Time $t$ (h:m:s)")
    ax.set_ylabel("Beamlet central frequency (MHz)")
    ax.set_title(f"I")

    ax2 = ax.twinx()
    ax2.set_ylabel("Subband index")
    ax2.set_ylim(beam.subbandList.start * cfg.FFTLEN, beam.subbandList.stop * cfg.FFTLEN)

    locator, formatter = make_locator_formatter()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    return fig, ax

def plot_tmp_I(tmp_I_dump_file: pathlib.Path, beam: Beam, cfg: NenuFRBConfig, block_slice, norm=None, timestamps=None):
    N_f = len(beam.subbandList) * cfg.FFTLEN
    N_t = cfg.N_T_MEDIUM_LOOP

    N_TMP_I = 6

    data = read_data(tmp_I_dump_file, (N_TMP_I, N_f, N_t), block_slice)

    tstart = 0
    tstop  = data.shape[-1] * cfg.DT

    if timestamps is not None:
        tstart = timestamps[0] + datetime.timedelta(seconds=tstart)
        tstop  = timestamps[0] + datetime.timedelta(seconds=tstop)


    fmin = beam.subbandList.start * cfg.DF_RAW / 1e6
    fmax = beam.subbandList.stop * cfg.DF_RAW / 1e6

    _M = int(np.floor(np.sqrt(N_TMP_I)))
    _N = int(np.ceil(np.sqrt(N_TMP_I)))
    fig, axs = plt.subplots(_M, _N)

    for i, ax, _data in zip(range(N_TMP_I), axs.flat, data):
        im = ax.imshow(_data, origin="lower",
                norm=norm,
                aspect="auto",
                extent=(tstart, tstop, fmin, fmax),
                interpolation="none")
        plt.colorbar(im)

        ax.set_xlabel("Time $t$ (h:m:s)")
        ax.set_ylabel("Beamlet central frequency (MHz)")
        ax.set_title(f"I (step {i})")

        ax2 = ax.twinx()
        ax2.set_ylabel("Subband index")
        ax2.set_ylim(beam.subbandList.start * cfg.FFTLEN, beam.subbandList.stop * cfg.FFTLEN)

        locator, formatter = make_locator_formatter()
        ax.xaxis.set_major_locator(locator)
        ax.xaxis.set_major_formatter(formatter)

    return fig, axs, data


range_pattern = re.compile(r'(\d+), *(\d+)')

def range_from_str(s):
    m = range_pattern.match(s)
    return range(int(m.group(1)), int(m.group(2)))

def slice_from_str(s):
    m = range_pattern.match(s)
    return slice(int(m.group(1)), int(m.group(2)))



def plot_A(A_dump_file: pathlib.Path, beam: Beam, cfg: NenuFRBConfig, block_slice, timestamps=None):
    N_t  = cfg.N_T_LOOSE_LOOP
    N_dm = cfg.N_DM

    data = read_data(A_dump_file, (N_dm, N_t), block_slice)

    tstart = (cfg.N_T_LOOSE_LOOP - cfg.N_T_OUT) * cfg.DT
    tstop  = tstart + data.shape[-1] * cfg.DT

    if timestamps is not None:
        tstart = timestamps[0] + datetime.timedelta(seconds=tstart)
        tstop  = timestamps[0] + datetime.timedelta(seconds=tstop)

    fig, ax = plot_hm(data, extent=(tstart, tstop, cfg.DMmin, cfg.DMmax), norm=mcolors.SymLogNorm(1e-2))
    ax.set_xlabel("Arrival time $t_0$ (h:m:s)")
    ax.set_ylabel("DM ($pc.cm^{-3}$)")
    ax.set_title("DMT output")

    locator, formatter = make_locator_formatter()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    return fig, ax


def regress_period(t_s, labels, k_labels):
    X = np.concatenate([np.full(t_s[labels == label].size, k) for k, label in k_labels]).reshape(-1, 1)
    y = np.concatenate([t_s[labels == label] for k, label in k_labels])

    lm = sklearn.linear_model.LinearRegression()
    lm.fit(X, y)

    # From
    # https://en.wikipedia.org/wiki/Simple_linear_regression#Confidence_intervals
    eps = y - lm.predict(X)
    s = np.sqrt((eps**2).sum() / ((X.size - 2) * ((X - X.mean())**2).sum()))
    t = scipy.stats.t.ppf(0.975, X.size - 2)

    return lm.coef_[0], s*t


def plot_candidates(candidates_dump_file: pathlib.Path, beam: Beam, cfg: NenuFRBConfig, block_slice, timestamps=None, cluster=False, measure_period=False):
    N_cand  = cfg.N_CANDIDATES
    
    data = read_data(candidates_dump_file, (N_cand,), block_slice, dtype=Record)

    N_chunks = data.size // N_cand
    N_t = cfg.N_T_LOOSE_LOOP * N_chunks

    tstart = (cfg.N_T_LOOSE_LOOP - cfg.N_T_OUT) * cfg.DT
    tstop  = tstart + N_t * cfg.DT

    t_s = cfg.DT * (data["i_w"] + np.linspace(0, N_chunks, data.size, endpoint=False, dtype=int))
    dm = cfg.DMmin + (cfg.DMmax - cfg.DMmin) / cfg.N_DM * data["i_h"]

    if timestamps is not None:
        tstart = timestamps[0] + datetime.timedelta(seconds=tstart)
        tstop  = timestamps[0] + datetime.timedelta(seconds=tstop)
        dt = (tstop - tstart) / N_t

    fmin = beam.subbandList.start * cfg.DF_RAW / 1e6
    fmax = beam.subbandList.stop  * cfg.DF_RAW / 1e6


    t_date = np.asarray([tstart + datetime.timedelta(seconds=t) for t in t_s])

    snr = data["snr"]

    fig, ax = plt.subplots()

    if not np.isnan(snr).all():
        if cluster:
            clustering = sklearn.cluster.DBSCAN(eps=0.1, min_samples=int(3*snr.max()))
            X = np.vstack((t_s, dm)).T
            labels = clustering.fit_predict(X, sample_weight=snr)
            inliers = labels >= 0

            dm_median = np.median(dm[inliers])
            dm_mad    = np.median(np.abs(dm[inliers] - dm_median))

            if measure_period:
                inliers   = inliers & (dm_median - 3*dm_mad <= dm) & (dm < dm_median + 3*dm_mad)

            im = ax.scatter(t_date[inliers], dm[inliers], s=1, c=labels[inliers], cmap="tab10")

            if measure_period:
                for unique in np.unique(labels[inliers]):
                    ax.text(tstart + datetime.timedelta(seconds=t_s[labels == unique].mean()), dm[labels == unique].mean(), f"{unique}")

            def print_stats(name, arr):
                mean = arr.mean()
                std  = arr.std()

                median = np.median(arr)
                mad    = np.median(np.abs(arr - median))

                logger.info(f"{name}: {mean} +/- {2*std} ({median} +/- {3 * mad})")

                return mean, std, median, mad

            print_stats("dm", dm[inliers])
            print_stats("snr", snr[inliers])
        else:
            im = ax.scatter(t_date, dm, s=1, c=snr)
            plt.colorbar(im)
        ax.set_xlim(tstart, tstop)
        ax.set_ylim(cfg.DMmin, cfg.DMmax)

    ax.set_xlabel("Arrival time $t_0$ (h:m:s)")
    ax.set_ylabel("DM ($pc.cm^{-3}$)")
    ax.set_title("Single Pulse candidates")

    locator, formatter = make_locator_formatter()
    ax.xaxis.set_major_locator(locator)
    ax.xaxis.set_major_formatter(formatter)

    return fig, ax


def plot_stats(stats_file: pathlib.Path, dtype, beam: Beam, cfg: NenuFRBConfig):
    stats = np.fromfile(stats_file, dtype=dtype)

    stats = pd.DataFrame(stats)

    fig, axs = plt.subplots(len(stats.columns), squeeze=False)
    
    for ax, col in zip(axs.flat, stats.columns):
        ax.plot(stats[col])
        ax.set_title(col)

    return fig, ax
    

if __name__ == "__main__":
    parser = argparse.ArgumentParser(
            prog="process_dump",
            description="Process data and A dumps from NenuFRB")

    parser.add_argument("obs_dir", type=pathlib.Path)

    parser.add_argument("--slice", type=slice_from_str, help="Select which part of the dump we want to display")

    parser.add_argument("--show", action="store_true")
    parser.add_argument("--save", action="store_true")
    parser.add_argument("--cluster", action="store_true", default=True)
    parser.add_argument("--format", default="png")
    parser.add_argument("--figsize", nargs=2, type=float, default=[12., 8.])
    parser.add_argument("--measure_period", action="store_true", default=False)

    args = parser.parse_args()

    mpl.rcParams["figure.figsize"] = args.figsize
    mpl.rcParams["figure.constrained_layout.use"] = True

    parset = read_parset(args.obs_dir / "current.parset")

    beams = parse_beams(parset)

    nenufrb_config = NenuFRBConfig.from_file(args.obs_dir / "conf.parset")

    timestamps = np.fromfile(args.obs_dir / "timestamps.dump", dtype=np.double)[args.slice if args.slice is not None else Ellipsis] # TODO: check (tight loop / loose loop)
    if timestamps.size == 0:
        logger.warn("found no timestamps")
        timestamps = np.array([0.0])
    timestamps = [datetime.datetime.fromtimestamp(t, datetime.timezone.utc) for t in timestamps]

    for i_beam, beam in enumerate(beams):
        raw_dump_file = args.obs_dir / f"raw_data-{i_beam}.dump"
        I_dump_file = args.obs_dir / f"I-{i_beam}.dump"
        tmp_I_dump_file = args.obs_dir / f"med_loop_I-{i_beam}.dump"
        A_dump_file = args.obs_dir / f"A-{i_beam}.dump"
        cand_dump_file = args.obs_dir / f"candidates-{i_beam}.dump"

        tight_loop_stats_file = args.obs_dir / f"tight_loop_stats-{i_beam}.dump"
        med_loop_stats_file = args.obs_dir / f"med_loop_stats-{i_beam}.dump"
        loose_loop_stats_file = args.obs_dir / f"loose_loop_stats-{i_beam}.dump"

        if raw_dump_file.exists() and raw_dump_file.stat().st_size:
            logger.info(f"Processing {raw_dump_file}")

            fig, ax = plot_raw(raw_dump_file, beam, nenufrb_config)
            fig.suptitle(beam.target + f"(beam {i_beam})")

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-raw-{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{raw_dump_file} not found")

        if tmp_I_dump_file.exists() and tmp_I_dump_file.stat().st_size:
            logger.info(f"Processing {tmp_I_dump_file}")

            fig, ax, data = plot_tmp_I(tmp_I_dump_file, beam, nenufrb_config, args.slice, timestamps=timestamps)
            fig.suptitle(beam.target + f"(beam {i_beam})")

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-tmp_I{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{tmp_I_dump_file} not found")

        if I_dump_file.exists() and I_dump_file.stat().st_size:
            logger.info(f"Processing {I_dump_file}")

            fig, ax = plot_I(I_dump_file, beam, nenufrb_config, args.slice, timestamps=timestamps)
            fig.suptitle(beam.target + f"(beam {i_beam})")

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-I{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{I_dump_file} not found")

        if A_dump_file.exists() and A_dump_file.stat().st_size:
            logger.info(f"Processing {A_dump_file}")

            fig, ax = plot_A(A_dump_file, beam, nenufrb_config, args.slice, timestamps=timestamps)
            fig.suptitle(beam.target + f"(beam {i_beam})")

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-A{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{A_dump_file} not found")

        if cand_dump_file.exists() and cand_dump_file.stat().st_size:
            logger.info(f"Processing {cand_dump_file}")

            fig, ax = plot_candidates(cand_dump_file, beam, nenufrb_config, args.slice, timestamps=timestamps, cluster=False, measure_period=args.measure_period)
            fig.suptitle(beam.target + f"(beam {i_beam})")

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-candidates{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)

            if args.cluster:
                fig, ax = plot_candidates(cand_dump_file, beam, nenufrb_config, args.slice, timestamps=timestamps, cluster=True)
                fig.suptitle(beam.target + f"(beam {i_beam})")

                if args.save:
                    outpng = args.obs_dir / f"{beam.target}-clustered{i_beam}.{args.format}"
                    logger.debug(outpng)
                    fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{cand_dump_file} not found")
        

        if tight_loop_stats_file.exists() and tight_loop_stats_file.stat().st_size:
            fig, ax = plot_stats(tight_loop_stats_file, tight_loop_stat, beam, nenufrb_config)

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-tight_loop_stats-{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{tight_loop_stats_file} not found")

        if med_loop_stats_file.exists() and med_loop_stats_file.stat().st_size:
            fig, ax = plot_stats(med_loop_stats_file, med_loop_stat, beam, nenufrb_config)

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-med_loop_stats-{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{med_loop_stats_file} not found")

        if loose_loop_stats_file.exists() and loose_loop_stats_file.stat().st_size:
            fig, ax = plot_stats(loose_loop_stats_file, loose_loop_stat, beam, nenufrb_config)

            if args.save:
                outpng = args.obs_dir / f"{beam.target}-loose_loop_stats-{i_beam}.{args.format}"
                logger.debug(outpng)
                fig.savefig(outpng, dpi=300)
        else:
            logger.warning(f"{loose_loop_stats_file} not found")

    if args.show:
        plt.show()
