#!/usr/bin/env python3

import subprocess
import pandas as pd
import pathlib
import io
import csv

ncu_exe = pathlib.Path("/usr/local/cuda/bin/ncu")

def roofline_point(cmd, kernels):
    _cmd = [ncu_exe,
        "--metrics", "smsp__sass_thread_inst_executed_op_fadd_pred_on.sum.per_cycle_elapsed,smsp__sass_thread_inst_executed_op_fmul_pred_on.sum.per_cycle_elapsed,smsp__sass_thread_inst_executed_op_ffma_pred_on.sum.per_cycle_elapsed,smsp__cycles_elapsed.avg.per_second,dram__bytes.sum.per_second,gpu__time_duration.sum,sm__sass_thread_inst_executed_op_ffma_pred_on.sum.peak_sustained,sm__cycles_elapsed.avg.per_second,dram__bytes.sum.peak_sustained,dram__cycles_elapsed.avg.per_second",
        "--csv"]
    _cmd.extend(["--kernel-id", f"::regex:{'|'.join(kernel for kernel in kernels)}:1"])
    _cmd.extend(cmd)

    p = subprocess.run(_cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    if p.returncode:
        raise RuntimeError(f"ncu exited with error code {p.returncode}. NCU output:\n---stdout---\n{p.stdout.decode()}\n---stderr---\n{p.stderr.decode()}")
    
    stdout_reader = io.BytesIO(p.stdout)

    while not b"==PROF== Disconnected from process" in stdout_reader.readline():
        pass

    df = pd.read_csv(stdout_reader, thousands=',', quoting=csv.QUOTE_ALL, index_col="ID")

    df = pd.DataFrame({"kernel": pd.unique(df["Kernel Name"]),
                       "fadd": df[df["Metric Name"] == "smsp__sass_thread_inst_executed_op_fadd_pred_on.sum.per_cycle_elapsed"]["Metric Value"],
                       "fmul": df[df["Metric Name"] == "smsp__sass_thread_inst_executed_op_fmul_pred_on.sum.per_cycle_elapsed"]["Metric Value"],
                       "ffma": df[df["Metric Name"] == "smsp__sass_thread_inst_executed_op_ffma_pred_on.sum.per_cycle_elapsed"]["Metric Value"],
                       "cycles": df[df["Metric Name"] == "smsp__cycles_elapsed.avg.per_second"]["Metric Value"],
                       "dram": df[df["Metric Name"] == "dram__bytes.sum.per_second"]["Metric Value"],
                       "duration": df[df["Metric Name"] == "gpu__time_duration.sum"]["Metric Value"],
                       "max_ffma": df[df["Metric Name"] == "sm__sass_thread_inst_executed_op_ffma_pred_on.sum.peak_sustained"]["Metric Value"],
                       "sm_cycles": df[df["Metric Name"] == "sm__cycles_elapsed.avg.per_second"]["Metric Value"],
                       "max_dram": df[df["Metric Name"] == "dram__bytes.sum.peak_sustained"]["Metric Value"],
                       "dram_cycles": df[df["Metric Name"] == "dram__cycles_elapsed.avg.per_second"]["Metric Value"],
                       })

    df["Performance"] = (df["fadd"] + df["fmul"] + 2*df["ffma"]) * df["cycles"]
    df["Arithmetic Intensity"] = df["Performance"] / df["dram"]
    df["Max Performance"] = 2 * df["max_ffma"] * df["sm_cycles"]
    df["Max Traffic"] = df["dram_cycles"] * df["max_dram"]
    df["Max Arithmetic Intensity"] = df["Max Performance"] / df["Max Traffic"]

    return df

benchmark_exe = pathlib.Path("/opt/NenuFRB/bin/benchmark")


N_f = 796
N_t_in = 1<<14
N_dm = 4096
N_t_out = 1<<16
N = 1<<10
N_candidates=1024


points = {
    "Time Integration": roofline_point([benchmark_exe, "--mode", "TimeIntegration", f"--N_f={N_f}", f"--N_t_in={N_t_in}"],
                          kernels=["sqmod_kernel"]),
    
    "MAD Clean": roofline_point([benchmark_exe, "--mode", "MADClean", f"--N_f={N_f}", f"--N_t_in={N_t_in}"],
                          kernels=["clean"]),

    "Rm Baseline": roofline_point([benchmark_exe, "--mode", "RmBaseline", f"--N_f={N_f}", f"--N_t_in={N_t_in}"],
                          kernels=["per_row_mean", "rm_baseline"]),

    "Rm Mean": roofline_point([benchmark_exe, "--mode", "RmMean", f"--N_f={N_f}", f"--N_t_in={N_t_in}"],
                          kernels=["global_mean", "rm_mean"]),

    "Dedispersion": roofline_point([benchmark_exe, "--mode", "BruteforceDmt", f"--N_f={N_f}", f"--N_t_in={N_t_in}",
                          f"--N_dm={N_dm}", f"--N_t_out={N_t_out}", f"--N={N}"],
                          kernels=["bruteforce_dmt_kernel"]),

    "Detection": roofline_point([benchmark_exe, "--mode", "Adaptive1D", f"--N_dm={N_dm}", f"--N_t_out={N_t_out}", f"--N_candidates={N_candidates}"],
                             kernels=["per_row_rms", "DeviceMergeSortBlockSortKernel", "DeviceMergeSortPartitionKernel",
                                      "DeviceMergeSortMergeKernel", "copy_records"])
}


colormap = {
    "Time Integration": "#cce6e6",
    "MAD Clean": "#ccffcc",
    "Rm Baseline": "#ffcccc",
    "Rm Mean": "#ffe6cc",
    "Dedispersion": "#bf80bf",
    "Detection":"#e6cce6"
}


import numpy as np
import matplotlib.pyplot as plt
import matplotlib.ticker as mticker
import matplotlib.patches as mpatches

max_arithmetic_intensity = np.mean([x for point in points.values() for x in point["Max Arithmetic Intensity"]])
max_performance          = np.mean([x for point in points.values() for x in point["Max Performance"]])
max_traffic              = np.mean([x for point in points.values() for x in point["Max Traffic"]])

view_min_arithmetic_intensity = 0.05
view_max_arithmetic_intensity = 1000
view_min_performance          = 10e9
view_max_performance          = 2 * max_performance

fig, ax = plt.subplots(layout="constrained", figsize=[6, 4])
ridge = np.array([max_arithmetic_intensity, max_performance])

ax.add_patch(mpatches.Polygon([[view_min_arithmetic_intensity, view_min_arithmetic_intensity * max_traffic],
                               [view_min_arithmetic_intensity, view_min_performance],
                               [max_arithmetic_intensity, view_min_performance],
                               ridge], closed=True, color="#1f77b420"))

ax.add_patch(mpatches.Rectangle([max_arithmetic_intensity, view_min_performance], view_max_arithmetic_intensity - max_arithmetic_intensity, max_performance - view_min_performance, color="#27d31b20"))

ax.text(np.sqrt(max_arithmetic_intensity * view_min_arithmetic_intensity), view_min_performance, "Memory bound",  horizontalalignment="center", verticalalignment="bottom")
ax.text(np.sqrt(max_arithmetic_intensity * view_max_arithmetic_intensity), view_min_performance, "Compute bound", horizontalalignment="center", verticalalignment="bottom")

for algo, point in points.items():
    selected = point[(point["Performance"] > 0) & (point["Arithmetic Intensity"] > 0)]
    arithmetic_intensity = np.average(selected["Arithmetic Intensity"], weights=selected["duration"])
    performance = np.average(selected["Performance"], weights=selected["duration"])

    ax.scatter(arithmetic_intensity, performance, label=algo, facecolor=colormap[algo], edgecolor="k", s=50)

ax.add_patch(mpatches.Polygon([[view_min_arithmetic_intensity, view_min_arithmetic_intensity * max_traffic],
                               ridge,
                               [view_min_arithmetic_intensity, view_max_performance]], closed=True, color="w"))

ax.plot(*np.vstack([[view_min_arithmetic_intensity, view_min_arithmetic_intensity * max_traffic], ridge]).T, color="#1f77b4")
ax.plot(*np.vstack([ridge, [view_max_arithmetic_intensity, view_max_arithmetic_intensity * max_traffic]]).T, color="#1f77b4", linestyle="--")
ax.plot(*np.vstack([[view_min_arithmetic_intensity, max_performance], ridge]).T, color="#1f77b4", linestyle="--")
ax.plot(*np.vstack([ridge, [view_max_arithmetic_intensity, max_performance]]).T, color="#1f77b4")

ax.set_xscale("log")
ax.set_yscale("log")
ax.xaxis.set_major_formatter(mticker.FormatStrFormatter("%g"))
ax.yaxis.set_major_formatter(mticker.EngFormatter("FLOP/s"))
ax.set_xlabel("Arithmetic Intensity (FLOP/B)")
ax.set_ylabel("Performance")


ax.set_xlim(view_min_arithmetic_intensity, view_max_arithmetic_intensity)
ax.set_ylim(view_min_performance, view_max_performance)
topleftcorner = (ax.transData + ax.transAxes.inverted()).transform([view_max_arithmetic_intensity, max_performance])
ax.legend(loc="upper right", bbox_to_anchor=topleftcorner)
fig.savefig("roofline_model.pdf")
