# TODO

## Science

### Multi-time integration factor / boxcar filtering the DMT

Priority: high

Goals:
* Increase chances of detection
* Take hits from multiple time integration values for clustering


## Implementation

### Clustering (DBSCAN ?)

CPU or GPU clustering of FRB candidates

Goals:
* Multiple detections per frame
* False positive removal
  * False positives tend to be isolated
  * True positives tend to "leak" in neighboring
    cells, making clusters of hits
* Super-resolution

### FDMT (Fast Dispersion Measure Transform)


AN ACCURATE AND EFFICIENT ALGORITHM FOR DETECTION OF RADIO BURSTS WITH AN
UNKNOWN DISPERSION MEASURE, FOR SINGLE-DISH TELESCOPES AND INTERFEROMETERS,
Barak Zackay and Eran O. Ofek,
DOI 10.3847/1538-4357/835/1/11 

Priority: low (performance is not a problem currently)

Goals:
* Reduce computation time compared to Bruteforce DMT


## Refactoring

### Move data\_ring allocation from RSP\_CEP to Context.

Goals:
* RING\_WIDTH is chosen by the user / Context, and not hardcoded in RSP\_CEP
* More consistent with the rest of the API


## Bug fixing
