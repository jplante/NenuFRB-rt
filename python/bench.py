#!/usr/bin/env python3
import numpy as np
import pandas as pd
import subprocess
import pathlib
import tqdm
import sys
from typing import List, Callable, Dict
import pickle

import sklearn.linear_model

sodir = (pathlib.Path(__file__).parent / ".." / "lib").resolve()
sys.path.append(sodir.as_posix())
import benchmark

bench = benchmark.BenchmarkContext(n_rep=4, fill=True)
rng = np.random.default_rng()


def explore_grid(to_bench: Callable, grid: Dict[str, np.ndarray], random_sampling=True, N=1024) -> pd.DataFrame:
    """
    Run a benchmark over a grid of parameters
    @to_bench: The benchmark to run as a function.
    @grid: A dictionnary of parameter names and corresponding values to explore
    @random_sampling: If True, `N` random points of the grid are explored.
        This can be used to reduce drastically the complexity of the grid exploration.
    @N: If random_sampling is True, this is the namber of samples that are taken. Else, `N` is not used.
    """
    if random_sampling:
        df = {var: rng.choice(values, N) for var, values in grid.items()}
    else:
        N = np.product([values.size for values in grid.values()])
        meshgrid = np.meshgrid(*grid.values())
        df = {var: arrangement.flatten()
              for var, arrangement in zip(grid.keys(), meshgrid)}

    df["mean_t"] = np.empty(N, dtype=np.float32)
    df["stddev_t"] = np.empty(N, dtype=np.float32)
    df = pd.DataFrame(df)

    for i in tqdm.tqdm(df.index, desc=to_bench.__name__):
        try:
            df.loc[i, ["mean_t", "stddev_t"]] = to_bench(
                **{key: df.loc[i, key].tolist() for key in grid.keys()})
        except:
            df.loc[i, ["mean_t", "stddev_t"]] = (np.nan, np.nan)

    return df


def model_time_integration(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_f", "N_t_in"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])

def model_mad_clean(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_f", "N_t_in"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])

def model_rm_baseline(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_f", "N_t_in"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])

def model_rm_mean(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_f", "N_t_in"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])

def model_bruteforce_dmt(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_dm", "N_t_out", "N"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])

def model_adaptive1d(df: pd.DataFrame):
    linear = sklearn.linear_model.LinearRegression(fit_intercept=False)
    X = df[["N_dm", "N_t_out"]].prod(axis=1)
    X = np.reshape(X.to_numpy(), (-1, 1))
    linear.fit(X, df["mean_t"])
    return linear, linear.score(X, df["mean_t"])


N_f = np.uint64(192) * np.arange(1, 5, dtype=np.uint64)
N_t_in = np.uint64(2) ** np.arange(10, 22, dtype=np.uint64)
N_dm = np.uint64(2) ** np.arange(8, 13, dtype=np.uint64)
N_t_out = np.uint64(2) ** np.arange(10, 22, dtype=np.uint64)
N_candidates = np.uint64(2) ** np.arange(8, 13, dtype=np.uint64)

N = np.array([1024])

fmin = np.linspace(10, 50, 16)
fmax = np.linspace(70, 85, 16)
DMmin = np.array([0.0])
DMmax = np.float64(2) ** np.arange(6, 13)


save_file = pathlib.Path("full_benchmark.pkl")
if save_file.exists():
    with save_file.open("rb") as f:
        _tmp = pickle.load(f)

    stats_time_integration = _tmp["stats_time_integration"]
    stats_mad_clean = _tmp["stats_mad_clean"]
    stats_rm_baseline = _tmp["stats_rm_baseline"]
    stats_rm_mean = _tmp["stats_rm_mean"]
    stats_bruteforce_dmt = _tmp["stats_bruteforce_dmt"]
    stats_adaptive1d = _tmp["stats_adaptive1d"]
else:
    stats_time_integration = explore_grid(bench.time_integration, {"N_f": N_f, "N_t_in": N_t_in}, random_sampling=False)
    stats_mad_clean = explore_grid(bench.mad_clean, {"N_f": N_f, "N_t_in": N_t_in}, random_sampling=False)
    stats_rm_baseline = explore_grid(bench.rm_baseline, {"N_f": N_f, "N_t_in": N_t_in}, random_sampling=False)
    stats_rm_mean = explore_grid(bench.rm_mean, {"N_f": N_f, "N_t_in": N_t_in}, random_sampling=False)

    # N_f = np.array([4*192])
    # N_t_in = np.array([1 << 14])
    # N_dm = np.array([4096])
    # N_t_out = np.array([1 << 16])
    # N = np.array([1024])

    stats_bruteforce_dmt = explore_grid(bench.bruteforce_dmt,
                                    {"N_f": N_f, "N_t_in": N_t_in, "N_dm": N_dm, "N_t_out": N_t_out,
                                        "N": N, "fmin": fmin, "fmax": fmax, "DMmin": DMmin, "DMmax": DMmax},
                                        random_sampling=True, N=256)

    stats_adaptive1d = explore_grid(bench.adaptive1d, {"N_dm": N_dm, "N_t_out": N_t_out, "N_candidates":N_candidates}, random_sampling=False)

    _tmp = {
        "stats_time_integration": stats_time_integration,
        "stats_mad_clean": stats_mad_clean,
        "stats_rm_baseline": stats_rm_baseline,
        "stats_rm_mean": stats_rm_mean,
        "stats_bruteforce_dmt": stats_bruteforce_dmt,
        "stats_adaptive1d": stats_adaptive1d,
    }
    with save_file.open("wb") as f:
        pickle.dump(_tmp, f)

time_integration_model, r2_time_integration = model_time_integration(stats_time_integration.dropna())
print(f"time_integration: {time_integration_model.coef_} * N_f * N_t_in (R² = {r2_time_integration})")

mad_clean_model, r2_mad_clean = model_mad_clean(stats_mad_clean.dropna())
print(f"mad_clean: {mad_clean_model.coef_} * N_f * N_t_in (R² = {r2_mad_clean})")

rm_baseline_model, r2_rm_baseline = model_rm_baseline(stats_rm_baseline.dropna())
print(f"rm_baseline: {rm_baseline_model.coef_} * N_f * N_t_in (R² = {r2_rm_baseline})")

rm_mean_model, r2_rm_mean = model_rm_mean(stats_rm_mean.dropna())
print(f"rm_mean: {rm_mean_model.coef_} * N_f * N_t_in (R² = {r2_rm_mean})")

bruteforce_dmt_model, r2_bruteforce_dmt = model_bruteforce_dmt(stats_bruteforce_dmt.dropna())
print(f"bruteforce_dmt: {bruteforce_dmt_model.coef_} * N_f * N_t_in (R² = {r2_bruteforce_dmt})")

adaptive1d_model, r2_adaptive1d = model_adaptive1d(stats_adaptive1d.dropna())
print(f"adaptive1d: {adaptive1d_model.coef_} * N_f * N_t_in (R² = {r2_adaptive1d})")



import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.ticker import PercentFormatter, FixedLocator, FixedFormatter
from matplotlib.scale import SymmetricalLogScale

dt = 1024 / 200e6
N_f = 192
N_dm = 4086
N_fold = 1024


width = 1
padding = 1.5
fig, ax = plt.subplots(layout="constrained")
ti_bar = Rectangle((0.5, 0), width, time_integration_model.coef_[0]/dt * N_f / 1e6, facecolor="#cce6e6", edgecolor="k")
mc_bar = Rectangle((ti_bar.get_x() + ti_bar.get_width() + padding, 0), width, 2 * mad_clean_model.coef_[0]/dt * N_f / N_fold / 1e6, facecolor="#ccffcc", edgecolor="k")
rb_bar = Rectangle((mc_bar.get_x(), mc_bar.get_y() + mc_bar.get_height()), width, 2 * rm_baseline_model.coef_[0]/dt * N_f / N_fold / 1e6, facecolor="#ffcccc", edgecolor="k")
rm_bar = Rectangle((rb_bar.get_x(), rb_bar.get_y() + rb_bar.get_height()), width, rm_mean_model.coef_[0]/dt * N_f / N_fold / 1e6, facecolor="#ffe6cc", edgecolor="k")
dd_bar = Rectangle((mc_bar.get_x() + mc_bar.get_width() + padding, 0), width, 0.0031099738407647237, facecolor="#bf80bf", edgecolor="k")
de_bar = Rectangle((dd_bar.get_x(), dd_bar.get_y() + dd_bar.get_height()), width, adaptive1d_model.coef_[0]/dt * N_dm / N_fold / 1e6, facecolor="#e6cce6", edgecolor="k")


bars = [ti_bar, mc_bar, rb_bar, rm_bar, dd_bar, de_bar]
xmin = min(bar.get_x() for bar in bars)
xmax = max(bar.get_x() + bar.get_width() for bar in bars)
ymin = min(bar.get_y() for bar in bars)
ymax = max(bar.get_y() + bar.get_height() for bar in bars)
for bar in bars:
    ax.add_patch(bar)

locs = list(set(bar.get_x() + bar.get_width()/2 for bar in bars))
labels = ["Tight loop", "Medium loop", "Loose loop"]

ax.set_xlim(xmin - padding/2, xmax + padding/2)
ax.set_ylim(ymin, 1.2 * ymax)
ax.set_yscale(SymmetricalLogScale(ax.yaxis, linthresh=1e-5, subs=list(range(2, 10))))
ax.yaxis.set_major_formatter(PercentFormatter(1))
ax.xaxis.set_major_locator(FixedLocator(locs))
ax.xaxis.set_major_formatter(FixedFormatter(labels))
ax.set_ylabel("Relative execution time")

print(f"Total: {sum(bar.get_height() for bar in bars):%}")
plt.show()
