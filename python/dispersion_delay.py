#!/usr/bin/env python3

import argparse
import datetime
from scipy.constants import pi, e, c, m_e, parsec, epsilon_0
 
k_DM = e**2 / (8 * pi**2 * epsilon_0 * m_e * c) * parsec / 1e6


def delta(fmin: float, fmax: float) -> float:
    return k_DM * (fmin**(-2) - fmax**(-2))

def Delta(fmin: float, fmax: float, DM: float) -> float:
    return DM * delta(fmin, fmax)

parser = argparse.ArgumentParser(description="Prints the dispersion delay Δt corresponding to the given fmin, fmax, and DM")
parser.add_argument("fmin", type=float, help="Minimum frequency (MHz)")
parser.add_argument("fmax", type=float, help="Maximum frequency (MHz)")
parser.add_argument("DM",   type=float, help="Dispersion Measure (pc.cm⁻³)")

parser.add_argument("-v", "--verbose", action="store_true")

args = parser.parse_args()

Δt = Delta(args.fmin, args.fmax, args.DM)
t_inc = 3.13e-5 * (args.DM / 100)**(1/2) * (args.fmin / 1000) ** (-1) * (args.fmax / 1000) ** (-1) * ((args.fmin + args.fmax) / 2000) ** (1/2)
if args.verbose:
    print(f"k_DM  = {k_DM} MHz².pc⁻¹.cm³.s")
    print(f"δt    = {delta(args.fmin, args.fmax):e} pc⁻¹.cm³.s")
    print(f"Δt    = {Δt}")
    print(f"t_inc = {t_inc}")


Δt = datetime.timedelta(seconds=Δt)

print(f"Δt    = {Δt} (HH:MM:SS)")
print(f"t_inc = {t_inc*1e3} (ms)")
