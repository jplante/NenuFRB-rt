import sys
import argparse
from pathlib import Path
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.colors as colors

dt_header = np.dtype({'names':   ['VERSION_ID',
                                  'SOURCE_INFO',
                                  'CONFIGURATION_ID',
                                  'STATION_ID',
                                  'NOF_BEAMLETS',
                                  'NOF_BLOCKS',
                                  'TIMESTAMP',
                                  'BLOCK_SEQUENCE_NUMBER',
                                  ],
                      'formats': ['<u1',
                                  '<u2',
                                  '<u1',
                                  '<u2',
                                  '<u1',
                                  '<u1',
                                  '<i4',
                                  '<i4',
                                  ],
                      })


fsx2 = np.uint64(200e6 / 512)
def full_bsn(ts, bsn):
    return (np.uint64(ts)*fsx2 + 1)//2 + np.uint64(bsn)

N_f = 4*192
N_t = 2**19 # Full number of samples

fs = 200e6 / 1024

N_BHR = 4

parser = argparse.ArgumentParser()
parser.add_argument("root_dir", type=Path)
parser.add_argument("i", type=int)
args = parser.parse_args()

hdr_fname = args.root_dir / f"Header-{args.i}.dat"
acq_fname = args.root_dir / f"Acquisition-{args.i}.dat"

assert hdr_fname.exists()
assert acq_fname.exists()


headers  = np.fromfile(hdr_fname, dtype=dt_header).reshape(N_BHR, -1)
raw_data = np.fromfile(acq_fname, dtype=np.float32).reshape(N_f, -1)

skip = 0
ts = headers["TIMESTAMP"][..., skip:]
bsn = headers["BLOCK_SEQUENCE_NUMBER"][..., skip:]

ts = ts
bsn = bsn
fbsn = full_bsn(ts, bsn)
n_t = raw_data.shape[1] # Number of time samples after reduction
print("headers n_t:", headers.shape[1])

plt.imshow(raw_data, extent=(0, N_t / fs, 0, N_f), aspect="auto", origin="lower", norm=colors.LogNorm())
plt.show()


# fig = plt.figure(num="ts/bsn")
# axs = [fig.add_subplot(221), fig.add_subplot(223), fig.add_subplot(122)]
# axs[0].plot(ts.T)
# axs[1].plot(bsn.T)
# axs[2].plot(fbsn.astype(float).T)
# 
# axs[0].set_title("Timestamp")
# axs[1].set_title("BSN")
# axs[2].set_title("Full BSN")
# 
# 
# fig.legend(["0", "1", "2", "3"])
# 
# plt.show()

