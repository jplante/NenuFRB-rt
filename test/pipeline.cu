#include <cuda_runtime.h>
#include <cufile.h>

#include "Preprocessing/ChunkedFFT.cuh"
#include "Preprocessing/MADClean.cuh"
#include "Dedispersion/BruteforceDmt.cuh"
#include "Detection/Adaptive1D.cuh"

#include <getopt.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cerrno>

#include <filesystem>
#include <utility>

#include <cutlass/complex.h>
#include <cute/tensor.hpp>

#include "dump.cuh"

namespace fs = std::filesystem;

static constexpr float BaseDt = 1024.0f / 200e6f;

static constexpr size_t FOLD_FACTOR = 1024; // TODO: pass this on the CLI

struct Config
{
    fs::path data_raw_dump;
    size_t N_t_in;
    size_t N_t_out;
    size_t N_f;
    size_t N_dm;
    size_t fftlen;
    size_t N;
    
    std::pair<size_t, size_t> subbandList;
};

template <typename T>
Config parse_args(int argc, char* argv[])
{
    Config cfg;
    cfg.N_t_in = 0;
    cfg.N_t_out = 0;
    cfg.N_f = 0;
    cfg.N_dm = 0;
    cfg.fftlen = 1;
    cfg.N = 0;
    cfg.subbandList = std::make_pair(0, 0);

    auto errstream = [&]() -> std::ostream& { return std::cerr << argv[0] << ": "; };

    while (true)
    {
        static struct option long_options[] = {
            {"N_t_in",      required_argument, 0, 't'},
            {"N_t_out",     required_argument, 0, 'T'},
            {"N_f",         required_argument, 0, 'f'},
            {"N_dm",        required_argument, 0, 'D'},
            {"fftlen",      required_argument, 0, 'l'},
            {"subbandList", required_argument, 0, 'S'},
            {"N",           required_argument, 0, 'N'},
            {0, 0, 0, 0}
        };

        int option_index = 0;

        int c = getopt_long(argc, argv, "", long_options, &option_index);

        if (c == -1)
            break;
        
        switch (c)
        {
        case 't':
            try
            {
                cfg.N_t_in = std::stoull(optarg);
            }
            catch (std::exception& e)
            {
                errstream() << "bad argument for option '--N_t_in': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'T':
            try
            {
                cfg.N_t_out = std::stoull(optarg);
            }
            catch (std::exception& e)
            {
                errstream() << "bad argument for option '--N_t_out': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'f':
            try
            {
                cfg.N_f = std::stoull(optarg);
            }
            catch (std::exception& e)
            {
                errstream() << "bad argument for option '--N_f': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'D':
            try
            {
            cfg.N_dm = std::stoull(optarg);
            }
            catch (std::exception& e)
            {
                errstream() << "bad argument for option '--N_dm': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'l':
            try
            {
                cfg.fftlen = std::stoull(optarg);
            }
            catch (std::exception& e)
            {
                errstream() << "bad argument for option '--fftlen': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        case 'S':
            try
            {
                std::istringstream ss(optarg);
                ss >> cfg.subbandList.first >> cfg.subbandList.second;
            }
            catch(const std::exception& e)
            {
                errstream() << "bad argument for option '--subbandList': " << optarg << " (" << e.what() << ")" << std::endl;
            }
            break;
        case 'N':
            try
            {
                cfg.N = std::stoull(optarg);
            }
            catch(const std::exception& e)
            {
                errstream() << "bad argument for option '--N': " << optarg << " (" << e.what() << ")" << std::endl;
                exit(EXIT_FAILURE);
            }
            break;
        default:
            exit(EXIT_FAILURE);
        }
    }

    if (optind < argc)
    {
        cfg.data_raw_dump = fs::path(argv[optind++]);

        if (!fs::exists(cfg.data_raw_dump))
        {
            errstream() << strerror(ENOENT) << std::endl;
            exit(EXIT_FAILURE);
        }
    }
    else
    {
        errstream() << "no path specified" << std::endl;
        exit(EXIT_FAILURE);
    }

    const auto file_size = fs::file_size(cfg.data_raw_dump);
    if (file_size == 0)
    {
        errstream() << cfg.data_raw_dump << " is empty" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (cfg.N_t_in > 0)
    {  
        if (cfg.N_f > 0)
        {
            const auto expected_size = cfg.N_t_in * cfg.N_f * 2*sizeof(T);
            if (expected_size != file_size)
            {
                errstream() << "expected size " << expected_size << "B != " << file_size << "B" << std::endl;
                exit(EXIT_FAILURE);
            }
        }
        else
            cfg.N_f = file_size / (cfg.N_t_in * 2*sizeof(T));
    }
    else
    {
        if (cfg.N_f > 0)
            cfg.N_t_in = file_size / (cfg.N_f * 2*sizeof(T));
        else
        {
            errstream() << "at least one of '--N_t_in' or '--N_f' must be specified" << std::endl;
            exit(EXIT_FAILURE);
        }
    }

    if ((cfg.N_t_in % cfg.fftlen) != 0)
    {
        errstream() << " fftlen must be a divisor of N_t_in (" << cfg.N_t_in <<
            " % " << cfg.fftlen << " = " << cfg.N_t_in % cfg.fftlen << std::endl;
        exit(EXIT_FAILURE);
    }

    if (cfg.N_t_out == 0)
        cfg.N_t_out = cfg.N_t_in / FOLD_FACTOR;

    if ((cfg.subbandList.first == 0ull)
        || (cfg.subbandList.second <= cfg.subbandList.first))
    {
        errstream() << " invalid subbandList: " << cfg.subbandList.first
            << ' ' << cfg.subbandList.second << std::endl;
        exit(EXIT_FAILURE);
    }

    if (cfg.N_dm == 0)
    {
        errstream() << "Please specify the number of DM trials with --N_dm" << std::endl;
        exit(EXIT_FAILURE);
    }

    if (cfg.N == 0)
    {
        cfg.N = cfg.N_f;
    }

    return cfg;
}

template <typename T_>
class Context
{
public:
    using T = T_;

    Context(const Config& cfg) : _N_t_in(cfg.N_t_in),
                                 _N_t_out(cfg.N_t_out),
                                 _N_f(cfg.N_f),
                                 _N_dm(cfg.N_dm),
                                 _fftlen(cfg.fftlen)
    {
        // Allocate memory
        checkCudaErrors(cudaMalloc(&ptrRaw,          cute::size(raw_layout())          * sizeof(cutlass::complex<T>)));
        checkCudaErrors(cudaMalloc(&ptrI,            cute::size(I_layout())            * sizeof(T)));
        checkCudaErrors(cudaMalloc(&ptrA,            cute::size(A_layout())            * sizeof(T)));
        checkCudaErrors(cudaMalloc(&ptrCandidates,   cute::size(candidates_layout())   * sizeof(Record<T>)));


        // Load data_raw_dump contents to the raw tensor
        int fd = open(cfg.data_raw_dump.c_str(), O_RDONLY | O_DIRECT, 0644);
        if (fd == -1)
        {
            std::stringstream ss;
            ss << "Could not open " << cfg.data_raw_dump << "for reading: " << strerror(errno) << std::endl;
        }

        CUfileHandle_t fh;
		CUfileDescr_t descr;
		memset(&descr, 0, sizeof(CUfileDescr_t));

		descr.handle.fd = fd;
		descr.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;
		checkCudaErrors(cuFileHandleRegister(&fh, &descr));
		checkCudaErrors(cuFileBufRegister(ptrRaw, cute::size(raw_layout()) * sizeof(cutlass::complex<T>), 0));

        cuFileRead(fh, ptrRaw, cute::size(raw_layout()) * sizeof(cutlass::complex<T>), 0, 0);

        checkCudaErrors(cuFileBufDeregister(ptrRaw));
		cuFileHandleDeregister(fh);
        close(fd);
    }

    ~Context()
    {
        checkCudaErrors(cudaFree(ptrCandidates));
        checkCudaErrors(cudaFree(ptrA));
        checkCudaErrors(cudaFree(ptrI));
        checkCudaErrors(cudaFree(ptrRaw));
    }

private:
    cutlass::complex<T>* ptrRaw;
    T* ptrI;
    T* ptrA;
    Record<T>* ptrCandidates;

    decltype(Config::N_t_in)  _N_t_in;
    decltype(Config::N_t_out) _N_t_out;
    decltype(Config::N_f)     _N_f;
    decltype(Config::N_dm)    _N_dm;
    decltype(Config::fftlen)  _fftlen;

public:
    decltype(auto) raw_layout() const
    {
        return cute::make_layout(
            cute::make_shape(_N_f, _N_t_in),
            cute::GenRowMajor{});
    }
    decltype(auto) I_layout() const
    {
        return cute::make_layout(
            cute::make_shape(_N_f * _fftlen, _N_t_in / _fftlen / FOLD_FACTOR),
            cute::GenRowMajor{});
    }
    decltype(auto) A_layout() const
    {
        return cute::make_layout(
            cute::make_shape(_N_dm, _N_t_out),
            cute::GenRowMajor{});
    }
    
    decltype(auto) candidates_layout() const
    {
        return cute::make_layout(
            cute::make_shape(cute::_1024{}), // TODO: make configurable
            cute::GenRowMajor{});
    }

    decltype(auto) raw() const
    { 
        return cute::make_tensor(cute::make_gmem_ptr(ptrRaw), raw_layout());
    }
    decltype(auto) I() const
    { 
        return cute::make_tensor(cute::make_gmem_ptr(ptrI), I_layout());
    }
    decltype(auto) A() const
    { 
        return cute::make_tensor(cute::make_gmem_ptr(ptrA), A_layout());
    }
    decltype(auto) candidates() const
    { 
        return cute::make_tensor(cute::make_gmem_ptr(ptrCandidates), candidates_layout());
    }
};


class ChunkedFFTWrapper
{
public:
    ChunkedFFTWrapper(const size_t fftlen_) : fftlen(fftlen_) {}

    template <typename InputTensor, typename OutputTensor>
    int operator()(const InputTensor& in, OutputTensor& out, const int N_t_in, cudaStream_t stream=NULL)
    {
        switch (fftlen)
        {
            case 1:
                return ChunkedFFT<1, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 4:
                return ChunkedFFT<4, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 8:
                return ChunkedFFT<8, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 16:
                return ChunkedFFT<16, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 32:
                return ChunkedFFT<32, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 64:
                return ChunkedFFT<64, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 128:
                return ChunkedFFT<128, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            case 256:
                return ChunkedFFT<256, FOLD_FACTOR>{}(in, out, N_t_in, stream);
            default:
                std::stringstream ss;
                ss << "Unsupported fftlen: " << fftlen;
                throw std::runtime_error(ss.str());
        }
    }
private:
    const size_t fftlen;
};

class RmBaselineWrapper
{
public:
    RmBaselineWrapper(const size_t fftlen_) : fftlen(fftlen_) {}

    template <typename T1, typename T2>
    decltype(auto) run(const T1& in,
             T2& out,
             const typename T1::value_type* median,
             void* workspace,
             cudaStream_t stream=NULL)
    {
        switch (fftlen)
        {
            case 1:
                return RmBaseline<1>::run(in, out, median, workspace, stream);
            case 2:
                return RmBaseline<2>::run(in, out, median, workspace, stream);
            case 4:
                return RmBaseline<4>::run(in, out, median, workspace, stream);
            case 8:
                return RmBaseline<8>::run(in, out, median, workspace, stream);
            case 16:
                return RmBaseline<16>::run(in, out, median, workspace, stream);
            case 32:
                return RmBaseline<32>::run(in, out, median, workspace, stream);
            case 64:
                return RmBaseline<64>::run(in, out, median, workspace, stream);
            case 128:
                return RmBaseline<128>::run(in, out, median, workspace, stream);
            case 256:
                return RmBaseline<256>::run(in, out, median, workspace, stream);
            default:
                std::stringstream ss;
                ss << "Unsupported fftlen: " << fftlen;
                throw std::runtime_error(ss.str());
        }
    }

private:
    const size_t fftlen;
};


int main(int argc, char* argv[])
{
    using T = float;


    Config cfg = parse_args<T>(argc, argv);
    Context<T> ctx(cfg);
    checkCudaErrors(cuFileDriverOpen());

    auto raw = ctx.raw();
    auto I = ctx.I();
    auto A = ctx.A();
    auto candidates = ctx.candidates();


    std::cout << "raw_layout: " << raw.layout() << std::endl
              << "I_layout:   " << I.layout()   << std::endl
              << "A_layout:   " << A.layout()   << std::endl;

    
    size_t mad_ws_size = MADClean::dry_run(I);
    size_t rmb_ws_size = RmBaseline<1>::dry_run(I);
    size_t mean_ws_size = RmMean::dry_run(I);
    size_t detection_ws_size = Adaptive1D::dry_run(A);

    void* base_ws, * mad_ws, * rmb_ws, * mean_ws, * detection_ws;
    checkCudaErrors(cudaMalloc(&base_ws, mad_ws_size + rmb_ws_size + mean_ws_size + detection_ws_size));

    mad_ws  = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(base_ws));
    rmb_ws  = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mad_ws) + mad_ws_size);
    mean_ws = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(rmb_ws) + rmb_ws_size);
    detection_ws = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mean_ws) + mean_ws_size);


    float t_ms;
    cudaEvent_t start, stop;
    checkCudaErrors(cudaEventCreate(&start));
    checkCudaErrors(cudaEventCreate(&stop));

    T h_median, h_mad;

    checkCudaErrors(cudaEventRecord(start));
    ChunkedFFT<1, FOLD_FACTOR>::run(raw, I);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "chunked_fft " << raw.shape() << " -> " << I.shape() << ": " << t_ms << " ms\n" << std::endl;


#define PREPROCESSING_STAGES 5

#if PREPROCESSING_STAGES >= 1 // Optionnaly disable some parts
    checkCudaErrors(cudaEventRecord(start));
    auto [median, mad] = MADClean::run<CleanType::FILL_MEDIAN>(I, I, 10, mad_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaMemcpy(&h_median, median, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(&h_mad, mad, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "mad clean " << I.shape() << ": " << t_ms << " ms ("
        << "median: " << h_median << ", mad: " << h_mad << ")" << std::endl;
#endif

#if PREPROCESSING_STAGES >= 2
    checkCudaErrors(cudaEventRecord(start));
    RmBaseline<1>::run(I, I, median, rmb_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "rm_baseline " << I.shape() << ": " << t_ms << " ms" << std::endl;
#endif

#if PREPROCESSING_STAGES >= 3
    checkCudaErrors(cudaEventRecord(start));
    std::tie(median, mad) = MADClean::run<CleanType::FILL_MEDIAN>(I, I, 3, mad_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaMemcpy(&h_median, median, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(&h_mad, mad, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "mad clean " << I.shape() << ": " << t_ms << " ms ("
        << "median: " << h_median << ", mad: " << h_mad << ")" << std::endl;
#endif

#if PREPROCESSING_STAGES >= 4
    checkCudaErrors(cudaEventRecord(start));
    RmBaseline<1>::run(I, I, median, rmb_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "rm_baseline " << I.shape() << ": " << t_ms << " ms\n" << std::endl;
#endif

#if PREPROCESSING_STAGES >= 5
    checkCudaErrors(cudaEventRecord(start));
    RmMean::run(I, I, mean_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    T h_mean;
    checkCudaErrors(cudaMemcpy(&h_mean, mean_ws, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "rm_mean " << I.shape() << ": " << t_ms << " ms (mean: " << h_mean << ")\n" << std::endl;
#endif

    const struct Dmt::Config dmt_cfg = {
        .N = cfg.N,

        .fmin = cfg.subbandList.first  / BaseDt / 1e6f,
        .fmax = cfg.subbandList.second / BaseDt / 1e6f,
        .DMmin = 0,
        .DMmax = 100,
        .dt = BaseDt * cfg.fftlen * FOLD_FACTOR
    };

    checkCudaErrors(cudaEventRecord(start));
    BruteforceDmt<Dmt::T0Ref::FMAX>::run(I, A, dmt_cfg);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "dmt " << I.shape() << " -> " << A.shape() << ": " << t_ms << " ms\n" << std::endl;
    checkCudaErrors(cudaStreamSynchronize(NULL));

    dump("pipeline-I.dump", I);
    dump("pipeline-A.dump", A);

    checkCudaErrors(cudaEventRecord(start));
    Adaptive1D::run(A, candidates, 3.0f, detection_ws);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "detection " << I.shape() << " -> " << candidates.shape() << ": " << t_ms << " ms" << std::endl;


    dump("pipeline-candidates.dump", candidates);

    checkCudaErrors(cudaFree(base_ws));
    checkCudaErrors(cudaEventDestroy(stop));
    checkCudaErrors(cudaEventDestroy(start));

    checkCudaErrors(cuFileDriverClose());

    return 0;
}