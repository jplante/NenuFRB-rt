#include <iostream>
#include <iomanip>

#include <Config/ParsetReader.hpp>

int main(int argc, char* argv[])
{
	ParsetReader pr("../test/Config/20220228_155900_20220228_160700_B0329+54_TRACKING.parset");

	auto duration = pr.get("Beam[0]", "duration").as_int();
	auto angle1 = pr.get("Beam[0]", "angle1").as_float();
	auto target = pr.get("Beam[0]", "target").as_string();
	auto startTime = pr.get("Beam[0]", "startTime").as_time();
	auto maList = pr.get("AnaBeam[0]", "maList").as_vector();
	auto lane1 = pr.get("Beam[1]", "lane1").as_range();
	
	std::cout << "n_beams: " << pr.n_elems("Beam") << std::endl;
	std::cout << "Beam[0].duration: " << duration << std::endl;
	std::cout << "Beam[0].angle1: " << angle1 << std::endl;
	std::cout << "Beam[0].target: " << target << std::endl;
	std::cout << "Beam[0].startTime: " << std::put_time(&startTime, "%x %X") << std::endl;
	std::cout << "AnaBeam[0].maList: ";
	for (auto& v: maList)
		std::cout << v << ' ';
	std::cout << std::endl;
	std::cout << "Beam[1].lane1: " << lane1.first << ' ' << lane1.second << std::endl;

	return 0;
}

