#include <Dedispersion/dmt.hpp>
#include <Dedispersion/BruteforceDmt.cuh>
#include <Frb/frb.hpp>
#include <utils/array.cuh>
#include <cute/tensor.hpp>
#include <utils/ring_buffer_stride.hpp>

#include <iostream>
#include <vector>


#ifdef USE_ROOT
#include <TCanvas.h>
#include <TH2.h>
#include <string>
#include <sstream>
#endif // USE_ROOT


constexpr size_t N_T_IN = 1 << 20;
constexpr size_t N_F = 256;

constexpr size_t N_T_OUT = 1 << 22;
constexpr size_t N_DM = 128;

struct TestConfig {
	size_t N_t_in;
	size_t N_t_out;
	size_t N_f;
	size_t N_dm;

	float fmin;
	float fmax;
	float DMmin;
	float DMmax;
	float dt;

	Dmt::Implem dmt_implem;
	
	uint64_t t0;
	float width;
	struct Frb* sim_frbs;
	size_t N_frbs;

	size_t N_t_total;
};

int parse_args(int argc, char* argv[], struct TestConfig* config)
{
	// TODO parse



	std::vector<struct Frb> sim_frbs;
	struct Frb frb {
		.timestamp = static_cast<uint64_t>(6.5f / config->dt),
		.dm = 60
	};
	sim_frbs.push_back(frb);
	
	config->sim_frbs = (struct Frb*) malloc(sim_frbs.size() * sizeof(struct Frb));
	memcpy(config->sim_frbs, sim_frbs.data(), sim_frbs.size() * sizeof(struct Frb));
	config->N_frbs = sim_frbs.size();

	return 0;
}


int run_test(struct TestConfig* config)
{
	return 0;
}


template <typename Input2DArray>
__global__ void generate_input_kernel(Input2DArray I, const struct TestConfig cfg)
{
	using InputT = typename Input2DArray::value_type;
	constexpr InputT zero_element(0.0f);
	constexpr InputT one_element(1.0f);

	const float df = (cfg.fmax - cfg.fmin) / cfg.N_f;

	const int64_t width_cells = static_cast<int64_t>(cfg.width / cfg.dt);

	for (int tid = blockIdx.x * blockDim.x + threadIdx.x; tid < cfg.N_f * cfg.N_t_in; tid += gridDim.x * blockDim.x)
	{
		int i_f = tid / cfg.N_t_in;
		int i_t = tid % cfg.N_t_in;

		for (int j = 0; j < cfg.N_frbs; j++)
		{
			const int64_t delay = static_cast<int64_t>(Delta(cfg.fmin, cfg.fmin + i_f * df, cfg.sim_frbs[j].dm) / cfg.dt);
			
			const int64_t cur_t = cfg.t0 + i_t;
			const int64_t frb_t = static_cast<int64_t>(cfg.sim_frbs[j].timestamp) - delay;
			I(i_f, i_t) = ((cur_t - width_cells <= frb_t) && (frb_t < cur_t + width_cells)) ? one_element : zero_element;
		}
	}

}



template <typename Input2DArray>
inline void generate_input(Input2DArray I, const struct TestConfig cfg, cudaStream_t stream=NULL)
{
	generate_input_kernel<<<80, 512, 0, stream>>>(I, cfg);
}


int main(int argc, char* argv[])
{
	int ret;


	struct TestConfig config {
		.N_t_in = 1 << 19,
		.N_t_out = 1 << 21,
		.N_f = 256,
		.N_dm = 128,
		
		.fmin = 70,
		.fmax = 85,
		.DMmin = 0,
		.DMmax = 100,
		.dt = 1024.0f / 200e6f,

		.dmt_implem = Dmt::Implem::Bruteforce,

		.t0 = 0,
		.width = 0.1,
		.N_t_total = 1 << 21
	};

	
	ret = parse_args(argc, argv, &config);
	if (ret < 0)
	{
		std::cerr << "Error parsing cmdline: " << strerror(-ret) << std::endl;
		return -ret;
	}

	Dmt::Config cfg = {
		.N_t_in = config.N_t_in,
		.N_t_out = config.N_t_out,
		.N_f = config.N_f,
		.N_dm = config.N_dm,

		.fmin = config.fmin,
		.fmax = config.fmax,
		.DMmin = config.DMmin,
		.DMmax = config.DMmax,
		.dt = config.dt,
	};
	BruteforceDmt dmt(cfg);


	struct Frb* d_sim_frbs;
	checkCudaErrors(cudaMalloc(&d_sim_frbs, config.N_frbs * sizeof(struct Frb)));
	checkCudaErrors(cudaMemcpy(d_sim_frbs, config.sim_frbs, config.N_frbs * sizeof(struct Frb), cudaMemcpyHostToDevice));

	
	float* I, * A;
	checkCudaErrors(cudaMalloc(&I, N_F * N_T_IN * sizeof(float)));
	checkCudaErrors(cudaMemset(I, 0, N_F * N_T_IN * sizeof(float)));
	checkCudaErrors(cudaMalloc(&A, N_DM * N_T_OUT * sizeof(float)));
	checkCudaErrors(cudaMemset(A, 0, N_DM * N_T_OUT * sizeof(float)));
	auto I_ring = cute::make_tensor(
		cute::make_gmem_ptr(I),
		cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, N_T_IN>{}),
		cute::make_stride(cute::constant<size_t, N_T_IN>{}, cute::RingBufferStride<1, N_T_IN, int>{})
	);
	auto A_ring = cute::make_tensor(
		cute::make_gmem_ptr(A),
		cute::make_shape(cute::constant<size_t, N_DM>{}, cute::constant<size_t, N_T_OUT>{}),
		cute::make_stride(cute::constant<size_t, N_T_OUT>{}, cute::RingBufferStride<1, N_T_OUT, int>{})
	);


#ifdef USE_ROOT
	float* I_frag_h, *I_frag_h_dptr, * A_frag_h, * A_frag_h_dptr;
	checkCudaErrors(cudaHostAlloc(&I_frag_h, N_T_IN * cfg.N_f * sizeof(float), cudaHostAllocMapped));
	checkCudaErrors(cudaHostAlloc(&A_frag_h, N_T_OUT * cfg.N_dm * sizeof(float), cudaHostAllocMapped));
	checkCudaErrors(cudaHostGetDevicePointer(&I_frag_h_dptr, I_frag_h, 0));
	checkCudaErrors(cudaHostGetDevicePointer(&A_frag_h_dptr, A_frag_h, 0));
#endif // USE_ROOT

	struct TestConfig d_config = config;
	d_config.sim_frbs = d_sim_frbs;

	cudaEvent_t start_gen, stop_gen, start_dmt, stop_dmt;
	checkCudaErrors(cudaEventCreate(&start_gen, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&stop_gen, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&start_dmt, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&stop_dmt, cudaEventDefault));

	for (int i = 0; i < config.N_t_total / config.N_t_in; i++)
	{
		checkCudaErrors(cudaEventRecord(start_gen, NULL));
		generate_input(I_ring, d_config);
		I_ring.advance_prod_head(config.N_t_in);
		checkCudaErrors(cudaEventRecord(stop_gen, NULL));

		checkCudaErrors(cudaEventRecord(start_dmt, NULL));
		dmt.dmt(I_ring, A_ring);
		checkCudaErrors(cudaEventRecord(stop_dmt, NULL));
		A_ring.advance_prod_head(config.N_t_in);

		checkCudaErrors(cudaEventSynchronize(stop_dmt));

		float t_gen, t_dmt;
		checkCudaErrors(cudaEventElapsedTime(&t_gen, start_gen, stop_gen));
		checkCudaErrors(cudaEventElapsedTime(&t_dmt, start_dmt, stop_dmt));

		std::cout << "t_gen: " << t_gen << " ms\nt_dmt: " << t_dmt << " ms" << std::endl;



#ifdef USE_ROOT
		memcpy_from_array(I_frag_h_dptr, I_ring, cfg.N_t_in);
		memcpy_from_array(A_frag_h_dptr, A_ring, cfg.N_t_out);

		TH2* h_I = new TH2F("h_I", "I", 512, i * cfg.N_t_in * cfg.dt, (i+1) * cfg.N_t_in * cfg.dt, cfg.N_f, cfg.fmin, cfg.fmax);
		TH2* h_A = new TH2F("h_A", "A", 512, i * cfg.N_t_in * cfg.dt, (i * cfg.N_t_in + cfg.N_t_out) * cfg.dt, cfg.N_dm, cfg.DMmin, cfg.DMmax);

		h_I->SetStats(0);
		h_A->SetStats(0);

		TCanvas* c = new TCanvas("Canvas", "Canvas");

		checkCudaErrors(cudaStreamSynchronize(NULL));

		float df  = (cfg.fmax - cfg.fmin) / cfg.N_f;
		float dDM = (cfg.DMmax - cfg.DMmin) / cfg.N_dm;

		for (int j = 0; j < cfg.N_f; j++)
		{
			for (int k = 0; k < cfg.N_t_in; k++)
			{
				h_I->Fill((i * cfg.N_t_in + k) * cfg.dt, cfg.fmin + j * df, I_frag_h[j * cfg.N_t_in + k]);
			}
		}

		h_I->Draw("Colz");

		std::stringstream ss;
		ss << "h_I-" << i << ".png";
		std::string fname = ss.str();
		c->Print(fname.c_str());


		for (int j = 0; j < cfg.N_dm; j++)
		{
			for (int k = 0; k < cfg.N_t_out; k++)
			{
				h_A->Fill((i * cfg.N_t_in + k) * cfg.dt, cfg.DMmin + j * dDM, A_frag_h[j * cfg.N_t_out + k]);
			}
		}

		h_A->Draw("Colz");

		ss.str("");
		ss << "h_A-" << i << ".png";
		fname = ss.str();
		c->Print(fname.c_str());

		delete c;
		delete h_A;
		delete h_I;
#endif // USE_ROOT

		I_ring.advance_cons_head(config.N_t_in);
		A_ring.advance_cons_head(config.N_t_in);


		d_config.t0 += cfg.N_t_in;
	}


	checkCudaErrors(cudaEventDestroy(start_gen));
	checkCudaErrors(cudaEventDestroy(stop_gen));
	checkCudaErrors(cudaEventDestroy(start_dmt));
	checkCudaErrors(cudaEventDestroy(stop_dmt));
	

	checkCudaErrors(cudaFree(I));
	checkCudaErrors(cudaFree(A));
	checkCudaErrors(cudaFree(d_sim_frbs));

	return 0;
}
