#include <Dedispersion/dmt.hpp>
#include <Dedispersion/BruteforceDmt.cuh>
#include <Frb/frb.hpp>
#include <Detection/Adaptive1D.cuh>
#include <utils/array.cuh>
#include <utils/ring_adapter.cuh>

#include <iostream>
#include <vector>

#include <random>
#include <curand_kernel.h>


#ifdef USE_ROOT
#include <TCanvas.h>
#include <TH2.h>
#include <TGraph.h>
#include <string>
#include <sstream>
#include <vector>
#endif // USE_ROOT


constexpr size_t N_T_IN = 1 << 20;
constexpr size_t N_F = 256;

constexpr size_t N_T_OUT = 1 << 22;
constexpr size_t N_DM = 128;

struct TestConfig {
	size_t N_t_in;
	size_t N_t_out;
	size_t N_f;
	size_t N_dm;

	float fmin;
	float fmax;
	float DMmin;
	float DMmax;
	float dt;

	Dmt::Implem dmt_implem;
	
	uint64_t t0;
	float width;
	struct Frb* sim_frbs;
	size_t N_frbs;

	size_t N_t_total;
};

int parse_args(int argc, char* argv[], struct TestConfig* config)
{
	// TODO parse



	std::vector<struct Frb> sim_frbs;
	struct Frb frb {
		.timestamp = static_cast<uint64_t>(6.5f / config->dt),
		.dm = 60
	};
	sim_frbs.push_back(frb);
	
	config->sim_frbs = (struct Frb*) malloc(sim_frbs.size() * sizeof(struct Frb));
	memcpy(config->sim_frbs, sim_frbs.data(), sim_frbs.size() * sizeof(struct Frb));
	config->N_frbs = sim_frbs.size();

	return 0;
}


int run_test(struct TestConfig* config)
{
	return 0;
}


template <typename Input2DArray>
__global__ void generate_input_kernel(Input2DArray I, const unsigned long long seed, const struct TestConfig cfg)
{
	using InputT = typename Input2DArray::value_type;
	constexpr InputT zero_element(0.0f);
	constexpr InputT one_element(1.0f);

	const float df = (cfg.fmax - cfg.fmin) / cfg.N_f;

	const int64_t width_cells = static_cast<int64_t>(cfg.width / cfg.dt);


	curandState_t state;
	curand_init(seed, blockIdx.x * blockDim.x + threadIdx.x, 0, &state);

	for (int tid = blockIdx.x * blockDim.x + threadIdx.x; tid < cfg.N_f * cfg.N_t_in; tid += gridDim.x * blockDim.x)
	{
		int i_f = tid / cfg.N_t_in;
		int i_t = tid % cfg.N_t_in;

		for (int j = 0; j < cfg.N_frbs; j++)
		{
			const int64_t delay = static_cast<int64_t>(Delta(cfg.fmin, cfg.fmin + i_f * df, cfg.sim_frbs[j].dm) / cfg.dt);
			
			const int64_t cur_t = cfg.t0 + i_t;
			const int64_t frb_t = static_cast<int64_t>(cfg.sim_frbs[j].timestamp) - delay;
			I(i_f, i_t) = (((cur_t - width_cells <= frb_t) && (frb_t < cur_t + width_cells)) ? one_element : zero_element) + curand_normal(&state) * 0.01f;
		}
	}

}


template <typename Input2DArray>
inline void generate_input(Input2DArray I, unsigned long long seed, const struct TestConfig cfg, cudaStream_t stream=NULL)
{
	generate_input_kernel<<<80, 512, 0, stream>>>(I, seed, cfg);
}


int main(int argc, char* argv[])
{
	int ret;


	struct TestConfig config {
		.N_t_in = 1 << 19,
		.N_t_out = 1 << 21,
		.N_f = 256,
		.N_dm = 128,
		
		.fmin = 70,
		.fmax = 85,
		.DMmin = 0,
		.DMmax = 100,
		.dt = 1024.0f / 200e6f,

		.dmt_implem = Dmt::Implem::Bruteforce,

		.t0 = 0,
		.width = 0.1,
		.N_t_total = 1 << 21
	};

	
	ret = parse_args(argc, argv, &config);
	if (ret < 0)
	{
		std::cerr << "Error parsing cmdline: " << strerror(-ret) << std::endl;
		return -ret;
	}

	Dmt::Config cfg = {
		.N_t_in = config.N_t_in,
		.N_t_out = config.N_t_out,
		.N_f = config.N_f,
		.N_dm = config.N_dm,

		.fmin = config.fmin,
		.fmax = config.fmax,
		.DMmin = config.DMmin,
		.DMmax = config.DMmax,
		.dt = config.dt,
	};
	BruteforceDmt dmt(cfg);


	struct Frb* d_sim_frbs;
	checkCudaErrors(cudaMalloc(&d_sim_frbs, config.N_frbs * sizeof(struct Frb)));
	checkCudaErrors(cudaMemcpy(d_sim_frbs, config.sim_frbs, config.N_frbs * sizeof(struct Frb), cudaMemcpyHostToDevice));

	
	float* I, * A;
	checkCudaErrors(cudaMalloc(&I, N_F * N_T_IN * sizeof(float)));
	checkCudaErrors(cudaMemset(I, 0, N_F * N_T_IN * sizeof(float)));
	checkCudaErrors(cudaMalloc(&A, N_DM * N_T_OUT * sizeof(float)));
	checkCudaErrors(cudaMemset(A, 0, N_DM * N_T_OUT * sizeof(float)));
	RingAdapter2D<N_T_IN, float> I_ring(I, N_F);
	RingAdapter2D<N_T_OUT, float> A_ring(A, N_DM);

	struct Record* candidates;
	checkCudaErrors(cudaMalloc(&candidates, MAX_HIT_PER_DM * cfg.N_dm * sizeof(struct Record)));


	Adaptive1D detector;


#ifdef USE_ROOT
	float* I_frag_h, *I_frag_h_dptr, * A_frag_h, * A_frag_h_dptr;
	Record* h_candidates;
	checkCudaErrors(cudaHostAlloc(&I_frag_h, cfg.N_t_in * cfg.N_f * sizeof(float), cudaHostAllocMapped));
	checkCudaErrors(cudaHostAlloc(&A_frag_h, cfg.N_t_out * cfg.N_dm * sizeof(float), cudaHostAllocMapped));
	checkCudaErrors(cudaHostGetDevicePointer(&I_frag_h_dptr, I_frag_h, 0));
	checkCudaErrors(cudaHostGetDevicePointer(&A_frag_h_dptr, A_frag_h, 0));

	checkCudaErrors(cudaHostAlloc(&h_candidates, MAX_HIT_PER_DM * cfg.N_dm * sizeof(struct Record), cudaHostAllocDefault));

	std::vector<float> t_hit(MAX_HIT_PER_DM * cfg.N_dm),
	                   dm_hit(MAX_HIT_PER_DM * cfg.N_dm);
#endif // USE_ROOT

	struct TestConfig d_config = config;
	d_config.sim_frbs = d_sim_frbs;

	cudaEvent_t start_gen, stop_gen, start_dmt, stop_dmt, start_detection, stop_detection;
	checkCudaErrors(cudaEventCreate(&start_gen, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&stop_gen, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&start_dmt, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&stop_dmt, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&start_detection, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&stop_detection, cudaEventDefault));


	std::random_device rd;
	std::mt19937 rng(rd());

	for (int i = 0; i < config.N_t_total / config.N_t_in; i++)
	{
		checkCudaErrors(cudaEventRecord(start_gen, NULL));
		generate_input(I_ring, rng(), d_config);
		I_ring.advance_prod_head(config.N_t_in);
		checkCudaErrors(cudaEventRecord(stop_gen, NULL));

		checkCudaErrors(cudaEventRecord(start_dmt, NULL));
		dmt.dmt(I_ring, A_ring);
		checkCudaErrors(cudaEventRecord(stop_dmt, NULL));
		A_ring.advance_prod_head(config.N_t_in);


		checkCudaErrors(cudaEventRecord(start_detection, NULL));
		checkCudaErrors(cudaMemsetAsync(candidates, 0xff, MAX_HIT_PER_DM * cfg.N_dm * sizeof(struct Record), NULL));
		detector.detect(A_ring, candidates, cfg.N_t_in, cfg.N_t_in);
		checkCudaErrors(cudaEventRecord(stop_detection, NULL));

		checkCudaErrors(cudaEventSynchronize(stop_detection));

		float t_gen, t_dmt, t_detection;
		checkCudaErrors(cudaEventElapsedTime(&t_gen, start_gen, stop_gen));
		checkCudaErrors(cudaEventElapsedTime(&t_dmt, start_dmt, stop_dmt));
		checkCudaErrors(cudaEventElapsedTime(&t_detection, start_detection, stop_detection));

		std::cout << "t_gen: " << t_gen << " ms\nt_dmt: " << t_dmt << " ms\nt_detection: " << t_detection << " ms" << std::endl;



#ifdef USE_ROOT
		memcpy_from_array(I_frag_h_dptr, I_ring, cfg.N_t_in);
		memcpy_from_array(A_frag_h_dptr, A_ring, cfg.N_t_out);
		checkCudaErrors(cudaMemcpyAsync(h_candidates, candidates, MAX_HIT_PER_DM * cfg.N_dm * sizeof(struct Record), cudaMemcpyDeviceToHost, NULL));

		TH2* h_I = new TH2F("h_I", "I", 512, i * cfg.N_t_in * cfg.dt, (i+1) * cfg.N_t_in * cfg.dt, cfg.N_f, cfg.fmin, cfg.fmax);
		TH2* h_A = new TH2F("h_A", "A", 512, i * cfg.N_t_in * cfg.dt, (i * cfg.N_t_in + cfg.N_t_out) * cfg.dt, cfg.N_dm, cfg.DMmin, cfg.DMmax);

		h_I->SetStats(0);
		h_A->SetStats(0);

		TCanvas* ic = new TCanvas("ICanvas", "ICanvas");
		ic->SetLogz(1);

		checkCudaErrors(cudaStreamSynchronize(NULL));


		float df  = (cfg.fmax - cfg.fmin) / cfg.N_f;
		float dDM = (cfg.DMmax - cfg.DMmin) / cfg.N_dm;

		for (int j = 0; j < cfg.N_f; j++)
		{
			for (int k = 0; k < cfg.N_t_in; k++)
			{
				h_I->Fill((i * cfg.N_t_in + k) * cfg.dt, cfg.fmin + j * df, I_frag_h[j * cfg.N_t_in + k]);
			}
		}

		h_I->Draw("Colz");

		std::stringstream ss;
		ss << "h_I-" << i << ".png";
		std::string fname = ss.str();
		ic->Print(fname.c_str());
		delete ic;
		delete h_I;

		TCanvas* ac = new TCanvas("ACanvas", "ACanvas");
		ac->SetLogz(1);


		for (int j = 0; j < cfg.N_dm; j++)
		{
			for (int k = 0; k < cfg.N_t_out; k++)
			{
				h_A->Fill((i * cfg.N_t_in + k) * cfg.dt, cfg.DMmin + j * dDM, A_frag_h[j * cfg.N_t_out + k]);
			}
		}

		h_A->Draw("Colz");

		int n_hit = 0;
		for (int j = 0; j < cfg.N_dm; j++)
		{
			for (int k = 0; k < MAX_HIT_PER_DM; k++)
			{
				Record r = h_candidates[j * MAX_HIT_PER_DM + k];
				if (!std::isnan(r.i_h) && !std::isnan(r.i_w))
				{
					t_hit[n_hit] = (i * cfg.N_t_in + r.i_w) * cfg.dt;
					dm_hit[n_hit] = cfg.DMmin + r.i_h * dDM;

					n_hit++;
				}
			}
		}

		TGraph* g_hit(NULL);
		if (n_hit > 0)
		{
			std::cout << n_hit << " candidates !!" << std::endl;
			g_hit = new TGraph(n_hit, t_hit.data(), dm_hit.data());
			g_hit->Draw("same *");
		}
		

		ss.str("");
		ss << "h_A-" << i << ".png";
		fname = ss.str();
		ac->Print(fname.c_str());

		delete ac;
		if (n_hit > 0)
			delete g_hit;
		delete h_A;
#endif // USE_ROOT

		I_ring.advance_cons_head(config.N_t_in);
		A_ring.advance_cons_head(config.N_t_in);


		d_config.t0 += cfg.N_t_in;
	}


	checkCudaErrors(cudaEventDestroy(start_gen));
	checkCudaErrors(cudaEventDestroy(stop_gen));
	checkCudaErrors(cudaEventDestroy(start_dmt));
	checkCudaErrors(cudaEventDestroy(stop_dmt));
	checkCudaErrors(cudaEventDestroy(start_detection));
	checkCudaErrors(cudaEventDestroy(stop_detection));


#ifdef USE_ROOT
	checkCudaErrors(cudaFreeHost(I_frag_h));
	checkCudaErrors(cudaFreeHost(A_frag_h));
#endif // USE_ROOT
	

	checkCudaErrors(cudaFree(candidates));
	checkCudaErrors(cudaFree(I));
	checkCudaErrors(cudaFree(A));
	checkCudaErrors(cudaFree(d_sim_frbs));
	

	return 0;
}
