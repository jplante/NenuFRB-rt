#include <cuda.h>
#include <cuda_runtime.h>
#include "helper_cuda.cuh"

#include <rte_eal.h>
#include <rte_ethdev.h>
#include <rte_log.h>
#include <rte_lcore.h>
#include <rte_gpudev.h>
#include <rte_cycles.h>

#include <signal.h>

#ifdef USE_ROOT
#include "array.cuh"
#include <TCanvas.h>
#include <TH2.h>
#include <TGraph.h>
#include <string>
#include <sstream>
#include <thread>
#include <fstream>
#endif // USE_ROOT

#include "Acquisition/RSP_CEP.cuh"

#define RTE_LOGTYPE_RECV_GPUDIRECT RTE_LOGTYPE_USER1

constexpr float FMIN = 10;
constexpr float FMAX = 85;
constexpr float DT = 1024.0f / 200e6f;
constexpr float DF = (FMAX - FMIN) / N_F;


volatile bool stop;
void signal_handler(int sig)
{
	switch (sig)
	{
	case SIGINT:
	case SIGTERM:
	case SIGUSR1:
		stop = 1;
		break;
	default:
		RTE_LOG(WARNING, RECV_GPUDIRECT, "Unkown signal %d\n", sig);
	}
}


#ifdef USE_ROOT
template <typename Input2DArray, typename Header2DArray>
void plot_heatmap(Input2DArray I, Header2DArray header_ring, const int64_t offset, const uint32_t N_t, const uint32_t iter, int dev_id, float* raw_data, float* raw_data_dptr, struct header_t* header, struct header_t* header_dptr, cudaStream_t stream, const size_t n_t)
{
	checkCudaErrors(cudaSetDevice(dev_id));

	/*
	std::stringstream canvas_ss;
	canvas_ss << "Canvas-h_I-" << iter;
	std::string canvas_s = canvas_ss.str();

	std::stringstream hist_ss;
	hist_ss << "h_I-" << iter;
	std::string hist_s = hist_ss.str();

	std::stringstream fname_ss;
	fname_ss << "Acquisition-h_I-" << iter << ".png";
	std::string fname_s = fname_ss.str();

	I.set_cons_head(offset);

	heatmap_x_array<<<60, 512, n_t * sizeof(float), stream>>>(raw_data_dptr, I, N_t, n_t);

	TCanvas* c = new TCanvas(canvas_s.c_str(), "Canvas");
	TH2* h_I = new TH2F(hist_s.c_str(), "I", n_t/2, iter * N_t * DT, (iter+1) * N_t * DT, N_F, FMIN, FMAX);

	c->SetLogz(1);
	h_I->SetStats(0);

	checkCudaErrors(cudaStreamSynchronize(stream));


	for (int i = 0; i < N_F; i++)
	{
		for (int j = 0; j < n_t; j++)
		{
			h_I->Fill((iter * N_t + N_t / n_t * j) * DT, FMIN + i * DF, raw_data[i * n_t + j]);
		}
	}

	h_I->Draw("Colz");

	c->Print(fname_s.c_str());


	delete c;
	delete h_I;
	*/

	I.set_cons_head(offset);
	checkCudaErrors(cudaMemcpy2DAsync(raw_data, N_t * sizeof(typename Input2DArray::value_type),
				I.data() + (offset % I.width()), I.width() * sizeof(typename Input2DArray::value_type),
				N_t * sizeof(typename Input2DArray::value_type), I.height(), cudaMemcpyDeviceToHost, stream));

	header_ring.set_cons_head(offset);
	checkCudaErrors(cudaMemcpy2DAsync(header, N_t * sizeof(struct header_t),
				header_ring.data() + (offset % header_ring.width()), header_ring.width() * sizeof(struct header_t),
				N_t * sizeof(struct header_t), 4, cudaMemcpyDeviceToHost, stream));
	checkCudaErrors(cudaStreamSynchronize(stream));

	std::stringstream datname_ss;
	datname_ss << "Acquisition-" << iter << ".dat";
	std::ofstream data_fd(datname_ss.str(), std::ios::out | std::ios::binary);
	data_fd.write(reinterpret_cast<char*>(raw_data), N_F * n_t * sizeof(float));

	std::stringstream header_fname;
	header_fname << "Header-" << iter << ".dat";
	std::ofstream header_fd(header_fname.str(), std::ios::out | std::ios::binary);
	header_fd.write((char*) header, N_BHR * N_t * sizeof(header_t));
}
#endif // USE_ROOT

int main(int argc, char* argv[])
{
	int ret;
	stop = 0;
	signal(SIGINT,  signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGUSR1, signal_handler);


	constexpr uint32_t N_t = 1 << 19;
	constexpr uint32_t n_t = 1 << 19;

	RSP_CEP rsp_cep;

	ret = rte_eal_init(argc, argv);
	if (ret < 0)
	{
		RTE_LOG(ERR, RECV_GPUDIRECT, "Cannot init EAL\n");
		return EXIT_FAILURE;
	}

	RTE_LOG(DEBUG, RECV_GPUDIRECT, "Successfully init EAL\n");

	argc -= ret;
	argv += ret;

	unsigned nb_lcores = rte_lcore_count();
	if (nb_lcores < 2)
	{
		RTE_LOG(ERR, RECV_GPUDIRECT, "At least 2 lcores must be available, found %u\n", nb_lcores);
		return EXIT_FAILURE;
	}

	const int16_t port = rte_eth_find_next(0);
	const int16_t dpdk_gpu_id = rte_gpu_find_next(0, RTE_GPU_ID_ANY);
	const unsigned poll_lcore = rte_get_next_lcore(0, true, false);

	// Recover CUDA device id from PCI id
	int cuda_gpu_id;
	struct rte_gpu_info gpu_info;
	rte_gpu_info_get(dpdk_gpu_id, &gpu_info);
	checkCudaErrors(cudaDeviceGetByPCIBusId(&cuda_gpu_id, gpu_info.name));

	checkCudaErrors(cudaSetDevice(cuda_gpu_id));

	cudaStream_t main_stream;
	checkCudaErrors(cudaStreamCreate(&main_stream));


	rsp_cep.init(port, dpdk_gpu_id);
	auto data_ring = rsp_cep.get_data_ring();
	auto header_ring = rsp_cep.get_header_ring();


#ifdef USE_ROOT
	uint32_t iter = 0;

	float* raw_data, * raw_data_dptr;
	cudaStream_t stream;
	
	checkCudaErrors(cudaHostAlloc(&raw_data, data_ring.height() * n_t * sizeof(float), cudaHostAllocMapped));
	checkCudaErrors(cudaHostGetDevicePointer(&raw_data_dptr, raw_data, 0));

	struct header_t* header, * header_dptr;
	checkCudaErrors(cudaHostAlloc(&header, N_BHR * N_t * sizeof(struct header_t), cudaHostAllocMapped));
	checkCudaErrors(cudaHostGetDevicePointer(&header_dptr, header, 0));
	checkCudaErrors(cudaStreamCreate(&stream));
#endif

	rsp_cep.start(poll_lcore);

	int64_t last_timestamp = 0L;
	while (!stop && !last_timestamp)
		last_timestamp = rsp_cep.get_latest_timestamp();


	while (!stop)
	{
		int64_t new_timestamp = rsp_cep.get_latest_timestamp();

		if (new_timestamp - last_timestamp >= N_t)
		{
			struct rte_eth_stats stats;
			rte_eth_stats_get(port, &stats);
			RTE_LOG(INFO, RECV_GPUDIRECT, "Received %u elements (missed: %lu)\n", N_t, stats.imissed);
			
#ifdef USE_ROOT
			// std::thread plot_thread(plot_heatmap<decltype(data_ring), decltype(header_ring)>, data_ring, header_ring, last_timestamp, N_t, iter, cuda_gpu_id, raw_data, raw_data_dptr, header, header_dptr, stream, n_t);
			// plot_thread.detach();
			plot_heatmap(data_ring, header_ring, last_timestamp, N_t, iter, cuda_gpu_id, raw_data, raw_data_dptr, header, header_dptr, stream, n_t);

#endif // USE_ROOT
			last_timestamp = new_timestamp;
			iter++;
		}
	}

	rsp_cep.stop();

#ifdef USE_ROOT
	checkCudaErrors(cudaStreamDestroy(stream));
	checkCudaErrors(cudaFreeHost(raw_data));
	checkCudaErrors(cudaFreeHost(header));
#endif

	checkCudaErrors(cudaStreamDestroy(main_stream));


	struct rte_eth_stats stats;
	rte_eth_stats_get(port, &stats);

	RTE_LOG(INFO, RECV_GPUDIRECT, "Stats: ipackets = %lu - imissed = %lu - ierrors = %lu\n", stats.ipackets, stats.imissed, stats.ierrors);


	return EXIT_SUCCESS;
}

