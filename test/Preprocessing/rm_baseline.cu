#include "Preprocessing/MADClean.cuh"

#include <cuda_runtime.h>
#include "utils/helper_cuda.cuh"
#include <random>
#include <curand.h>
#include <cute/tensor.hpp>
#include <algorithm>
#include <iostream>
// #include <execution>

int main(int argc, char* argv[])
{
    static constexpr int M = 192 * 256;
    static constexpr int N = 1 << 11;
    using T = float;
    static constexpr int FFTLEN = 1;

    auto layout = cute::make_layout(
        cute::make_shape(cute::constant<size_t, M>{}, cute::constant<size_t, N>{}),
        cute::make_stride(cute::constant<size_t, N>{}, cute::constant<size_t, 1>{})
    );

    T* dptrA, * hptrA;
    checkCudaErrors(cudaMalloc(&dptrA, cute::size(layout) * sizeof(T)));
    checkCudaErrors(cudaHostAlloc(&hptrA, cute::size(layout) * sizeof(T), cudaHostAllocDefault));


    cudaEvent_t start, stop;
    checkCudaErrors(cudaEventCreate(&start, cudaEventDefault));
    checkCudaErrors(cudaEventCreate(&stop,  cudaEventDefault));

    curandGenerator_t gen;
	curandCreateGenerator(&gen,
	            CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen,
	            std::random_device()());
	curandGenerateUniform(gen, dptrA, cute::size(layout));

    checkCudaErrors(cudaMemcpy(hptrA, dptrA, cute::size(layout) * sizeof(T), cudaMemcpyDeviceToHost));

    auto dA = cute::make_tensor(cute::make_gmem_ptr(dptrA), layout);
    auto hA = cute::make_tensor(cute::make_gmem_ptr(hptrA), layout);

    std::vector<T> ref_means(M);
    T* h_res;
    checkCudaErrors(cudaHostAlloc(&h_res, cute::size(layout) * sizeof(T), cudaHostAllocDefault));

    size_t ws_size_med = Median::dry_run(dA);
    size_t ws_size_rmb = RmBaseline<FFTLEN>::dry_run(dA);

    void* _workspace;
    checkCudaErrors(cudaMalloc(&_workspace, ws_size_med + ws_size_rmb + sizeof(T)));

    void* ws_med = _workspace;
    void* ws_rmb = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(ws_med) + ws_size_med);
    T* median = reinterpret_cast<T*>(reinterpret_cast<uintptr_t>(ws_rmb) + ws_size_rmb);

    Median::run(dA, median, ws_med);


    checkCudaErrors(cudaEventRecord(start));
    RmBaseline<FFTLEN>::run(dA, dA, median, ws_rmb);
    checkCudaErrors(cudaEventRecord(stop));
    checkCudaErrors(cudaEventSynchronize(stop));

    for (size_t i = 0; i < N; i++)
    {
        T acc = T();
        auto row = hA(cute::make_coord(i, cute::_));

        for (int j = 0; j < cute::size(row); j++)
        {
            acc += row(j);
        }

        ref_means[i] = acc / cute::size(row);
    }
    T* h_median = hptrA + cute::size(layout) / 2;
    std::nth_element(hptrA, h_median, hptrA + cute::size(layout));

    for (size_t i = 0; i < N; i++)
    {
        T acc = T();
        auto row = hA(cute::make_coord(i, cute::_));

        for (int j = 0; j < cute::size(row); j++)
        {
            acc += row(j);
            row(j) = (row(j) - ref_means[i]) * *h_median / ref_means[i] + *h_median;
        }
    }

    checkCudaErrors(cudaMemcpy(h_res, dptrA, cute::size(layout) * sizeof(T), cudaMemcpyDeviceToHost));


    float t_ms;
    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "rm_baseline over " << cute::size(dA) << " elements computed in " << t_ms << " ms" << std::endl;

    for (int i = 0; i < cute::size(layout); i++)
    {
        if (std::abs(h_res[i] - hptrA[i]) > 1e-5f)
        {
            std::stringstream ss("Means failed: ");
            ss << h_res[i] << " != " << hptrA[i] << std::endl;
            throw std::runtime_error(ss.str());
        }
    }

    checkCudaErrors(cudaFree(_workspace));
    checkCudaErrors(cudaFreeHost(h_res));
    checkCudaErrors(cudaFreeHost(hptrA));
    checkCudaErrors(cudaFree(dptrA));
    return 0;
}
