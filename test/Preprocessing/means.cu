#include "Preprocessing/MADClean.cuh"

#include <cuda_runtime.h>
#include "utils/helper_cuda.cuh"
#include <random>
#include <curand.h>
#include <cute/tensor.hpp>
#include <algorithm>
#include <iostream>
// #include <execution>

int main(int argc, char* argv[])
{
    static constexpr int M = 192 * 256;
    static constexpr int N = 1 << 11;
    using T = float;

    auto layout = cute::make_layout(
        cute::make_shape(cute::constant<size_t, M>{}, cute::constant<size_t, N>{}),
        cute::make_stride(cute::constant<size_t, N>{}, cute::constant<size_t, 1>{})
    );

    T* dptrA, * hptrA;
    checkCudaErrors(cudaMalloc(&dptrA, cute::size(layout) * sizeof(T)));
    checkCudaErrors(cudaHostAlloc(&hptrA, cute::size(layout) * sizeof(T), cudaHostAllocDefault));


    cudaEvent_t start, stop;
    checkCudaErrors(cudaEventCreate(&start, cudaEventDefault));
    checkCudaErrors(cudaEventCreate(&stop,  cudaEventDefault));

    curandGenerator_t gen;
	curandCreateGenerator(&gen,
	            CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen,
	            std::random_device()());
	curandGenerateUniform(gen, dptrA, cute::size(layout));

    checkCudaErrors(cudaMemcpy(hptrA, dptrA, cute::size(layout) * sizeof(T), cudaMemcpyDeviceToHost));

    auto dA = cute::make_tensor(cute::make_gmem_ptr(dptrA), layout);
    auto hA = cute::make_tensor(cute::make_gmem_ptr(hptrA), layout);

    std::vector<T> ref_means(M);
    T* h_means, *d_means;
    checkCudaErrors(cudaHostAlloc(&h_means, N * sizeof(T), cudaHostAllocDefault));
    checkCudaErrors(cudaMalloc(&d_means, N * sizeof(T)));

    auto means_tensor = cute::make_tensor(cute::make_gmem_ptr(d_means), cute::make_layout(cute::constant<size_t, M>{}));

    checkCudaErrors(cudaEventRecord(start));
    perRowMean::run(dA, means_tensor);
    checkCudaErrors(cudaEventRecord(stop));

    for (size_t i = 0; i < N; i++)
    {
        T acc = T();
        auto row = hA(cute::make_coord(i, cute::_));

        for (int j = 0; j < cute::size(row); j++)
        {
            acc += row(j);
        }

        ref_means[i] = acc / cute::size(row);
    }

    checkCudaErrors(cudaMemcpy(h_means, d_means, N * sizeof(T), cudaMemcpyDeviceToHost));


    float t_ms;
    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << cute::size<0>(dA) << " means over " << cute::size<1>(dA) << " elements computed in " << t_ms << " ms" << std::endl;

    for (int i = 0; i < N; i++)
    {
        if (std::abs(ref_means[i] - h_means[i]) > 1e-5f)
        {
            std::stringstream ss("Means failed: ");
            ss << ref_means[i] << " != " << h_means[i] << std::endl;
            throw std::runtime_error(ss.str());
        }
    }

    checkCudaErrors(cudaFreeHost(h_means));
    checkCudaErrors(cudaFree(d_means));
    checkCudaErrors(cudaFreeHost(hptrA));
    checkCudaErrors(cudaFree(dptrA));
    return 0;
}
