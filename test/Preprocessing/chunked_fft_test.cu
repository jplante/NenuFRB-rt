#include <cuda_runtime.h>
#include "Preprocessing/ChunkedFFT.cuh"
#include "utils/helper_cuda.cuh"
#include <thrust/complex.h>
#include <utils/array.cuh>
#include <fstream>
#include <random>
#include <curand.h>
#include <cute/tensor.hpp>


template <typename Output2DArray>
__global__ void sines(Output2DArray out, float dt, float max_freq)
{
	for (int i_f = blockIdx.x; i_f < cute::size<0>(out); i_f += gridDim.x)
	{
		for (int i_t = threadIdx.x; i_t < cute::size<1>(out); i_t += gridDim.x)
		{
			const float f = i_f * max_freq / cute::size<0>(out);
			const float t = i_t * dt;
			out(i_f, i_t) = thrust::complex<float>{cospif(2 * f * t), sinpif(2 * f * t)};
		}
	}
}


int main(int argc, char* argv[])
{
	constexpr size_t N_F = 192;
	constexpr size_t N_T = 1 << 19;
	constexpr size_t FFTLEN = 256;
	
	const float dt       = 4.0f / N_T;
	const float max_freq = 192;

	thrust::complex<float>* raw_data_ptr, * raw_data_hptr;
	float* I_ptr, * I_hptr;

	checkCudaErrors(cudaMalloc(&raw_data_ptr, N_F * N_T * sizeof(thrust::complex<float>)));
	checkCudaErrors(cudaHostAlloc(&raw_data_hptr, N_F * N_T * sizeof(thrust::complex<float>), cudaHostAllocDefault));
	checkCudaErrors(cudaMalloc(&I_ptr, N_F * N_T * sizeof(float)));
	checkCudaErrors(cudaHostAlloc(&I_hptr, N_F * N_T * sizeof(float), cudaHostAllocDefault));

	cudaStream_t s1, s2;
	checkCudaErrors(cudaStreamCreate(&s1));
	checkCudaErrors(cudaStreamCreate(&s2));

	cudaEvent_t e1, e2, e3, e4;
	checkCudaErrors(cudaEventCreate(&e1, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&e2, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&e3, cudaEventDefault));
	checkCudaErrors(cudaEventCreate(&e4, cudaEventDefault));

	auto raw_data = cute::make_tensor(
		cute::make_gmem_ptr(raw_data_ptr),
		cute::make_layout(
			cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, N_T>{}),
			cute::GenRowMajor{}
		)
	);

	auto I = cute::make_tensor(
		cute::make_gmem_ptr(I_ptr),
		cute::make_layout(
			cute::make_shape(cute::constant<size_t, N_F * FFTLEN>{}, cute::constant<size_t, N_T / FFTLEN>{}),
			cute::GenRowMajor{}
		)
	);
	
	sines<<<N_F, 256, 0, s1>>>(raw_data, dt, max_freq);

	/*
	curandGenerator_t gen;
	curandCreateGenerator(&gen,
	            CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen,
	            std::random_device()());
	curandGenerateUniform(gen, reinterpret_cast<float*>(raw_data_ptr), 2 * N_F * N_T);
	*/

	checkCudaErrors(cudaEventRecord(e1, s1));
	checkCudaErrors(cudaStreamWaitEvent(s2, e1, 0));
	checkCudaErrors(cudaMemcpyAsync(raw_data_hptr, raw_data_ptr, cute::size(raw_data) * sizeof(typename decltype(raw_data)::value_type), cudaMemcpyDeviceToHost, s2));
	checkCudaErrors(cudaEventRecord(e2, s2));

	ChunkedFFT<FFTLEN, 1>::run(raw_data, I, N_T, s1);
	checkCudaErrors(cudaEventRecord(e3, s1));
	checkCudaErrors(cudaStreamWaitEvent(s2, e3, 0));
	checkCudaErrors(cudaMemcpyAsync(I_hptr, I_ptr, cute::size(I) * sizeof(typename decltype(I)::value_type), cudaMemcpyDeviceToHost, s2));
	checkCudaErrors(cudaEventRecord(e4, s2));


	checkCudaErrors(cudaEventSynchronize(e2));
	std::ofstream raw_data_fd("test_fft_raw_data.dump", std::ios::out | std::ios::binary);
	raw_data_fd.write(reinterpret_cast<char*>(raw_data_hptr), cute::size(raw_data) * sizeof(typename decltype(raw_data)::value_type));
	raw_data_fd.close();

	checkCudaErrors(cudaEventSynchronize(e4));
	std::ofstream I_fd("test_fft_I.dump", std::ios::out | std::ios::binary);
	I_fd.write(reinterpret_cast<char*>(I_hptr), cute::size(I) * sizeof(typename decltype(I)::value_type));
	I_fd.close();


	checkCudaErrors(cudaEventDestroy(e4));
	checkCudaErrors(cudaEventDestroy(e3));
	checkCudaErrors(cudaEventDestroy(e2));
	checkCudaErrors(cudaEventDestroy(e1));

	checkCudaErrors(cudaStreamDestroy(s2));
	checkCudaErrors(cudaStreamDestroy(s1));

	// curandDestroyGenerator(gen);

	checkCudaErrors(cudaFreeHost(I_hptr));
	checkCudaErrors(cudaFree(I_ptr));
	checkCudaErrors(cudaFreeHost(raw_data_hptr));
	checkCudaErrors(cudaFree(raw_data_ptr));

	return 0;
}

