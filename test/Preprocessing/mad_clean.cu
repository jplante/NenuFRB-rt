#include "Preprocessing/MADClean.cuh"

#include <cuda_runtime.h>
#include "utils/helper_cuda.cuh"
#include <random>
#include <curand.h>
#include <cute/tensor.hpp>
#include <algorithm>
#include <iostream>
// #include <execution>

int main(int argc, char* argv[])
{
    static constexpr int M = 1 << 11;
    static constexpr int N = 192 * 256;
    using T = float;

    auto layout = cute::make_layout(
        cute::make_shape(cute::constant<size_t, M>{}, cute::constant<size_t, N>{}),
        cute::make_stride(cute::constant<size_t, N>{}, cute::constant<size_t, 1>{})
    );

    T* dptrA, * hptrA;
    checkCudaErrors(cudaMalloc(&dptrA, cute::size(layout) * sizeof(T)));
    checkCudaErrors(cudaHostAlloc(&hptrA, (cute::size(layout) + 2) * sizeof(T), cudaHostAllocDefault));


    cudaEvent_t start, stop;
    checkCudaErrors(cudaEventCreate(&start, cudaEventDefault));
    checkCudaErrors(cudaEventCreate(&stop,  cudaEventDefault));

    curandGenerator_t gen;
	curandCreateGenerator(&gen,
	            CURAND_RNG_PSEUDO_DEFAULT);
	curandSetPseudoRandomGeneratorSeed(gen,
	            std::random_device()());
	curandGenerateUniform(gen, dptrA, cute::size(layout));

    checkCudaErrors(cudaMemcpy(hptrA, dptrA, cute::size(layout) * sizeof(T), cudaMemcpyDeviceToHost));

    auto dA = cute::make_tensor(cute::make_gmem_ptr(dptrA), layout);

    size_t workspace_size = MADClean::dry_run(dA);

    T* workspace;
    checkCudaErrors(cudaMalloc(&workspace, workspace_size));
    checkCudaErrors(cudaEventRecord(start));
    auto[median, mad] = MADClean::run(dA, dA, 3, workspace);
    checkCudaErrors(cudaEventRecord(stop));


    T* median_ref, *median_gpu;

    median_ref = hptrA + cute::size(layout) / 2;
    median_gpu = hptrA + cute::size(layout);

    std::vector<T> absolute_deviation(cute::size(layout));


    T* mad_gpu = hptrA + cute::size(layout) + 1;

    std::nth_element(hptrA, median_ref, hptrA + cute::size(layout)); // Could be std::execution::par_unseq but I have a compiler error on gcc-9
    std::transform(hptrA, hptrA + cute::size(layout), absolute_deviation.begin(), [=](const T& x) { return std::abs(x - *median_ref); });

    auto mad_ref = absolute_deviation.begin() + absolute_deviation.size() / 2;
    std::nth_element(absolute_deviation.begin(), mad_ref, absolute_deviation.end());
    checkCudaErrors(cudaMemcpy(median_gpu, median, sizeof(T), cudaMemcpyDeviceToHost));
    checkCudaErrors(cudaMemcpy(mad_gpu, mad, sizeof(T), cudaMemcpyDeviceToHost));


    float t_ms;
    checkCudaErrors(cudaEventElapsedTime(&t_ms, start, stop));
    std::cout << "mad clean over " << cute::size(dA) << " elements compute in " << t_ms << " ms" << std::endl;
    std::cout << "median_ref = " << *median_ref << ", median_gpu = " << *median_gpu << std::endl;
    std::cout << "mad_ref = " << *mad_ref << ", mad_gpu = " << *mad_gpu << std::endl;
    if (std::abs(*median_ref - *median_gpu) / *median_ref > 1e-1)
    {
        throw std::runtime_error("wrong result");
    }
    

    checkCudaErrors(cudaFree(workspace));
    checkCudaErrors(cudaFreeHost(hptrA));
    checkCudaErrors(cudaFree(dptrA));
    return 0;
}
