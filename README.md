# NenuFRB

NenuFAR FRB predetection pipeline implementation.


## References

This repository implements the pipeline described [here](https://adass2021.ac.za/uploads/X2-001/upload/X2-001_latest.pdf) (proceedings not yet published), with the method described in the proceedings of SPIE Astronomical Telescopes + Instrumentation 2022 (Plante, J., Gratadour, D., Matias, L., Viou, C., & Agostini, E. (2022, August). A high performance data acquisition on COTS hardware for astronomical instrumentation. In Software and Cyberinfrastructure for Astronomy VII (Vol. 12189, pp. 323-332). SPIE.).


## Install

### Dependencies

#### Hardware

* PCIe Nvidia GPU with Compute Capability >= 7.0 (required for cuFFTDx)
* PCIe NIC (only tested with Mellanox Connectx >= 5)

#### Software

* CUDA (tested on CUDA 12.0, should be working for CUDA >= 11.4)
* GDRCopy
* DPDK (>= 22.03) with [CUDA and GRDCopy support enabled](https://doc.dpdk.org/guides/gpus/cuda.html#build-dependencies)
* CUTLASS (> 3.0) (only CuTe is used currently)


### Build

Once the NenuFRB repository has been cloned, run from the NenuFRB repository:

```bash
mkdir build && cd build
cmake .. -DCMAKE_BUILD_TYPE=Release -DCMAKE_CUDA_ARCHITECTURES=80-real -DCMAKE_INSTALL_PREFIX=<install location>
make -j8
make install
```

This builds the whole project, and installs the main excecutable of the project, `NenuFRB`.
This executable can be run to acquire and process LaNewBa RSP_CEP packets searching for radio transients such as FRBs.

Note: `make install` may require root privilege to install in some locations. Root privileges
will be required anyways to use `setcap` on the resulting `NenuFRB` executable.


## Pipeline

Quick pipeline description

### Acquisition

The data acquisition system is implemented as the `RSP_CEP` class. The constructor takes the id of a NIC and a GPU as seen from DPDK.
It takes charge of configuring and allocating required resources for the acquisition.

The `start` function can then be used to start the acquisition of LaNewBa RSP\_CEP packets.

### Compute

A classic single pulse detection pipeline has been implemented, with variations on some algorithms.

The computing part of the pipeline is split into three loops with different periods:
* Tight loop: time integration
* Medium loop: preprocessing
* Loose loop: Dispersion Measure Transform (DMT), detection

#### Time integration

Increase SNR by making the time resolution coarser, and optionally making the frequency resolution thinner.

The time integration is implemented in class `ChunkedFFT` (name subject to change).

Two parameters can be tuned on this class: FFTLEN and TIME_INTEGRATION. FFTLEN gives the size of the StFT used,
and TIME_INTEGRATION gives the number of consecutive time samples that are averaged.

In v0.4, FFTLEN > 1 is not validated.
FFTLEN > 1 and TIME_INTEGRATION > 1 at the same time is not implemented yet.

#### Preprocessing

Preprocessing consists in RFI mitigation and signal normalization.

RFI mitigation is done using a simple MAD filter. Normalization is done by dividing each frequency channel
by its mean and multiplying by the global median of the signal.


#### Dedispersion

Dedispersion is done incoherently using the Dispersion Measure Transform (DMT).
Currently, only Bruteforce DMT has been implemented, but other algorithms (FDMT especially) could be implemented in the future.

A variation to the standard algorithm is implemented, to be able to run it in an overlap-add fashion. Using this
variation, no overalp is required, and memory footprint of the pipeline can be reduced. This variation should be
compatible with FDMT.

### Detection

Detection is implemented by computing SNR per DM channel in the DMT output. Then, peaks are found using an arbitrary threshold.
This threshold can be lowered to increase sensitivity at the cost of specificity.

Clustering is planned after the detection stage. A prototype implementation has been used in the Python script `process_dump.py`,
but it still has to be implemented in the actual pipeline.

## Implementation details

The pipeline relies on [CuTe](https://github.com/NVIDIA/cutlass/tree/main/media/docs/cute) to describe arrays in a reusable way.

## Contributing

Contributions are welcome, please contact me (julien.plante@obspm.fr) or post an issue if you have any question. Feel free to open a merge request if you introduce some feature.
