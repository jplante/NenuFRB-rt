#pragma once

#include <cstdint>

struct Frb
{
	uint64_t timestamp; // Timestamp of arrival time (lower frequency component received)
	float    dm;        // Dispersion measure
} __attribute__((aligned(16)));


