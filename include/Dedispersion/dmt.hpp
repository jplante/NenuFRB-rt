#pragma once

#include <string>


template <typename T>
constexpr T pi             = T(3.141592653589793L);     // pi

template <typename T>
constexpr T e              = T(1.602176634e-19L);       // Elementary charge (C)

template <typename T>
constexpr T m_e            = T(9.1093837015e-31L);      // Electron mass (kg)

template <typename T>
constexpr T c              = T(299792458.0L);           // Speed of light (m/s)

template <typename T>
constexpr T epsilon_0      = T(8.8541878128e-12L);      // Vacuum permittivity

template <typename T>
constexpr T parsec         = T(3.085677581491367e+16L); // 1 parsec (pc) in meters (m)


// Dispersion constant (MHz² pc⁻¹ cm³ s). WARNING: non-SI units (to avoid precision issues)
template <typename T>
constexpr T k_DM = T(e<long double>*e<long double> /
	(8.0L * pi<long double>*pi<long double> * epsilon_0<long double> * m_e<long double> * c<long double>) * parsec<long double> / 1e6L);



template <typename T>
__host__ __device__ __forceinline__ constexpr T Delta(const T fmin, const T fmax, const T DM)
{
	constexpr T _1(1);

	return k_DM<T> * DM * (_1/(fmin*fmin) - _1/(fmax*fmax));
}



class Dmt
{
public:
	struct Config {
		size_t N;

		float fmin;
		float fmax;
		float DMmin;
		float DMmax;
		float dt;
	};

	enum class T0Ref
	{
		FMIN, // t0 is taken at fmin
		FMAX // t0 is taken at fmax
	};

	Dmt() = delete;
	Dmt(const struct Config& config)
		: cfg(config) {}
	~Dmt();

	template <typename Input2DArray, typename Output2DArray>
	int dmt(Input2DArray I, Output2DArray A);



	enum class Implem {
		Bruteforce,
		Undefined,
	};

	static Implem implem_from_string(const std::string& s)
	{
		if (s == "BruteforceDMT")
		{
			return Implem::Bruteforce;
		}
	
		return Implem::Undefined;
	}

protected:
	struct Config cfg;
};



