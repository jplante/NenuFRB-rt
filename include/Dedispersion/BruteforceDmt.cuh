#pragma once

#include <type_traits>

#include <cuda.h>
#include <cuda_runtime.h>
#include <cooperative_groups.h>

#include <utils/helper_cuda.cuh>
#include <numeric>

#include "dmt.hpp"
#include <cute/tensor.hpp>


namespace cg = cooperative_groups;



template <int THREADS_PER_BLOCK, int ITEMS_PER_THREAD, Dmt::T0Ref T0Ref, typename Input2DArray, typename Output2DArray>
__global__ void bruteforce_dmt_kernel(const Input2DArray I, Output2DArray A, const __grid_constant__ struct Dmt::Config cfg);


template <Dmt::T0Ref T0Ref_>
struct BruteforceDmt // Implements dmt
{
	static constexpr Dmt::T0Ref T0Ref = T0Ref_;
	static constexpr int THREADS_PER_BLOCK = 256;
	static constexpr int ITEMS_PER_THREAD = 1;

	template <typename InputEngine, typename InputLayout,
		typename OutputEngine, typename OutputLayout>
	static int run(const cute::Tensor<InputEngine, InputLayout>& I,
		cute::Tensor<OutputEngine, OutputLayout>& A,
		const struct Dmt::Config& cfg,
		cudaStream_t stream=NULL)
	{
		const dim3 blockDim(THREADS_PER_BLOCK);
		const dim3 gridDim(cute::size<0>(A) * CUB_QUOTIENT_CEILING(cute::size<1>(A), ITEMS_PER_THREAD * THREADS_PER_BLOCK));

		bruteforce_dmt_kernel<THREADS_PER_BLOCK, ITEMS_PER_THREAD, T0Ref>
			<<<gridDim, blockDim, 0, stream>>>(I, A, cfg);

		return 0;
	}
};


template <Dmt::T0Ref T0Ref>
struct utils;

template <>
struct utils<Dmt::T0Ref::FMIN>
{
	template <typename T>
	static __device__ __forceinline__
	T delay(const T fmin,
	        const T,      // fmax unused in this specialization
	        const T Df,
	        const T dm)
	{
		return Delta(fmin, fmin + Df, dm);
	}

	template <typename Index, typename I1, typename I2>
	static __device__ __forceinline__
	Index delay_and_offset(const Index offset,
	                       const Index delay,
	                       const I1, // Unused in this specialization
	                       const I2)
	{
		return offset - delay;
	}
};

template <>
struct utils<Dmt::T0Ref::FMAX>
{
	template <typename T>
	static __device__ __forceinline__
	T delay(const T fmin,      // fmin unused in this specialization
	        const T fmax,
	        const T Df,
	        const T dm)
	{
		return Delta(fmin + Df, fmax, dm);
	}

	template <typename Index, typename I1, typename I2>
	static __device__ __forceinline__
	Index delay_and_offset(const Index offset,
	                       const Index delay,
	                       const I1 N_t_in,
	                       const I2 N_t_out)
	{
		return offset + delay - (N_t_out - N_t_in);
	}
};


template <int THREADS_PER_BLOCK, int ITEMS_PER_THREAD, Dmt::T0Ref T0Ref,
	typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
__global__ void __launch_bounds__(THREADS_PER_BLOCK)
bruteforce_dmt_kernel(const cute::Tensor<InputEngine, InputLayout> I, cute::Tensor<OutputEngine, OutputLayout> A,
	const __grid_constant__ struct Dmt::Config cfg)
{
	using Index = int64_t;
	using InputT  = typename InputEngine::value_type;
	using OutputT = typename OutputEngine::value_type;

	constexpr OutputT output_zero_element = OutputT();

	__shared__ Index channels[THREADS_PER_BLOCK];
	__shared__ Index delays[THREADS_PER_BLOCK];


	const auto N_f = cute::size<0>(I);
	const auto N_t_in = cute::size<1>(I);
	const auto N_dm = cute::size<0>(A);
	const auto N_t_out = cute::size<1>(A);

	const float df  = (cfg.fmax  - cfg.fmin)  / N_f;
	const float dDM = (cfg.DMmax - cfg.DMmin) / N_dm;

	const float dx = (cfg.fmax - cfg.fmin) / cfg.N;

	const auto tChunks = CUB_QUOTIENT_CEILING(N_t_out, ITEMS_PER_THREAD * THREADS_PER_BLOCK);

	cg::grid_group grid = cg::this_grid();
	cg::thread_block block = cg::this_thread_block();

	for (int bid = grid.block_rank(); bid < N_dm * tChunks; bid += grid.num_blocks())
	{
		const int i_dm = bid / tChunks;
		const int i_t_chunk = bid % tChunks;

		OutputT s[ITEMS_PER_THREAD];

#pragma unroll
		for (int k = 0; k < ITEMS_PER_THREAD; k++)
		{
			s[k] = output_zero_element;
		}

		for (int i = 0; i < cfg.N; i += THREADS_PER_BLOCK)
		{
			{
				int j = threadIdx.x;
				channels[j] = static_cast<Index>((i + j) * N_f / cfg.N);
				delays[j] = static_cast<Index>(
					utils<T0Ref>::delay(cfg.fmin, cfg.fmax, (i + j) * dx, cfg.DMmin + i_dm * dDM) / cfg.dt);
			}
			block.sync();

			for (int j = 0; j < THREADS_PER_BLOCK; j++)
			{
#pragma unroll
				for (int k = 0; k < ITEMS_PER_THREAD; k++)
				{
					const Index full_i_t =
						i_t_chunk * ITEMS_PER_THREAD * THREADS_PER_BLOCK
					  + k * THREADS_PER_BLOCK
					  + block.thread_rank();
					  
					Index idx = utils<T0Ref>::delay_and_offset(full_i_t, delays[j], N_t_in, N_t_out);

					if ((Index(0) <= idx) && (idx < N_t_in))
					{
						const auto x = I(cute::make_coord(channels[j], idx));
						if (!isnan(x))
							s[k] += x;
					}
				}
			}
		}


#pragma unroll
		for (int k = 0; k < ITEMS_PER_THREAD; k++)
		{
			const Index full_i_t =
				i_t_chunk * ITEMS_PER_THREAD * THREADS_PER_BLOCK
				+ k * THREADS_PER_BLOCK
				+ block.thread_rank();

			if (full_i_t < N_t_out - N_t_in)
			{
				A(cute::make_coord(i_dm, full_i_t)) += s[k];
			}
			else if (full_i_t < N_t_out)
			{
				A(cute::make_coord(i_dm, full_i_t))  = s[k];
			}
		}
	}
}

