#pragma once

#include <cufftdx.hpp>
#include <stdexcept>
#include <sstream>
#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>

#include <cub/block/block_reduce.cuh>

#include <cute/tensor.hpp>

namespace kernel {


namespace cg = cooperative_groups;

template <typename T>
__device__ __forceinline__ T logmod(const T re, const T im);

template <>
__device__ __forceinline__ __half logmod<__half>(const __half re, const __half im)
{
	const __half a = 10.0f;
	__half2 z = __halves2half2(re, im);
	z = __hmul2(z, z);

	return a * hlog10(__hadd(__low2half(z), __high2half(z)));
}

template <>
__device__ __forceinline__ float logmod<float>(const float re, const float im)
{
	return 20.0f * log10f(hypotf(re, im));
}

template <typename T>
__device__ __forceinline__ double logmod(const double re, const double im)
{
	return 20.0 * log10(hypot(re, im));
}

template <typename T>
__device__ __forceinline__ T sqmod(const T re, const T im)
{
	return re*re + im*im;
}


template <typename FFT, typename Input2DArray, typename Output2DArray>
__global__ __launch_bounds__(FFT::block_dim.x * FFT::block_dim.y * FFT::block_dim.z)
void chunked_fft(const Input2DArray data, Output2DArray I, const int N_t_in)
{
	using complex_type = typename FFT::value_type;

	static_assert(!FFT::requires_workspace, "FFTSize is too big: FFT requires a workspace, which is not yet supported.");

	using InputT  = typename Input2DArray::value_type; // Must be a C++ compliant complex (defines .real(), .imag())
	using OutputT = typename Output2DArray::value_type; // Must be a scalar (float, double, ...)
	static constexpr int WARP_SIZE = 32;
	static constexpr int ElementsPerBlock = WARP_SIZE * cufftdx::size_of<FFT>::value;
	const int n_tiles = N_t_in / ElementsPerBlock;

	complex_type thread_data[FFT::storage_size];
	extern __shared__ complex_type shared_mem[];

	__shared__ OutputT shared_output[WARP_SIZE][cufftdx::size_of<FFT>::value+1]; // Shared output (I = XX + YY), for efficient transpose
	// (see https://developer.nvidia.com/blog/efficient-matrix-transpose-cuda-cc/)

	cg::thread_block block = cg::this_thread_block();
	cg::thread_block_tile<32, cg::thread_block> warp = cg::tiled_partition<32>(block);


	for (int i_f = blockIdx.y; i_f < cute::size<0>(data); i_f += gridDim.y)
	{
		for (int i_t = blockIdx.x; i_t < n_tiles; i_t += gridDim.x)
		{

			for (unsigned int local_fft_id = threadIdx.y; local_fft_id < WARP_SIZE; local_fft_id += FFT::ffts_per_block)
			{
				const unsigned int offset = (i_t * WARP_SIZE + local_fft_id) * cufftdx::size_of<FFT>::value;

#pragma unroll
				for (int i = 0; i < FFT::elements_per_thread; i++)
				{
					// reinterpret_cast needed here because of strange complex type in cufftDx.
					// There might be a cleaner solution to keep type checking... define conversion operator ?
					thread_data[i] = *reinterpret_cast<const complex_type*>(
							&data(i_f, offset + i * FFT::stride + threadIdx.x));
				}

				FFT().execute(thread_data, shared_mem);
				
#pragma unroll
				for (int i = 0; i < FFT::elements_per_thread; i++)
				{
					shared_output[local_fft_id][(i * FFT::stride + threadIdx.x // Actual index, and add fftshift
							+ cufftdx::size_of<FFT>::value/2) % cufftdx::size_of<FFT>::value]
						= sqmod(thread_data[i].real(), thread_data[i].imag());
				}
			}
			block.sync();


			for (int j_f = warp.meta_group_rank(); j_f < cufftdx::size_of<FFT>::value; j_f += warp.meta_group_size())
			{
				// j_t = warp.thread_rank()

				I(i_f * cufftdx::size_of<FFT>::value + j_f, i_t * WARP_SIZE + warp.thread_rank())
					= shared_output[warp.thread_rank()][j_f];
			}
			block.sync();
		}
	}

}

template <int TIME_INTEGRATION, int THREADS_PER_BLOCK, typename InputTensor, typename OutputTensor>
__global__ __launch_bounds__(THREADS_PER_BLOCK) void 
sqmod_kernel(const InputTensor in, OutputTensor out)
{
	using T = typename OutputTensor::value_type;
	cg::grid_group grid = cg::this_grid();
	cg::thread_block block = cg::this_thread_block();


	using BlockReduce = cub::BlockReduce<T, THREADS_PER_BLOCK>;
	__shared__ typename BlockReduce::TempStorage temp_storage;

	const auto tChunks = CUB_QUOTIENT_FLOOR(cute::size<1>(in), TIME_INTEGRATION);
	for (int bid = grid.block_rank(); bid < cute::size<0>(in) * tChunks; bid += grid.num_blocks())
	{
		const int i_f = bid / tChunks;
		const int i_t_chunk = bid % tChunks;

		T acc = T();

		const int offset = i_t_chunk * TIME_INTEGRATION;
		for (int i_t = block.thread_rank(); i_t < TIME_INTEGRATION; i_t += THREADS_PER_BLOCK)
		{
			const auto z = in(cute::make_coord(i_f, offset + i_t));
			acc += sqmod(z.real(), z.imag());
		}

		const T aggregate = BlockReduce(temp_storage).Sum(acc);

		if (block.thread_rank() == 0)
			out(cute::make_coord(i_f, i_t_chunk)) = aggregate / TIME_INTEGRATION;
	}
}
} // namespace kernel


template <size_t FFTSize_, size_t TIME_INTEGRATION_>
class ChunkedFFT
{
public:
	static constexpr size_t FFTSize = FFTSize_;
	static constexpr size_t TIME_INTEGRATION = TIME_INTEGRATION_;
	static constexpr int THREADS_PER_BLOCK = 256;

public:
	template <typename Input2DArray, typename Output2DArray>
	static int run(Input2DArray data, Output2DArray I, const int N_t_in, cudaStream_t stream=NULL)
	{
		using InputT = typename Input2DArray::value_type;

		using FFTBase = decltype(cufftdx::Size<FFTSize>()
					+ cufftdx::Precision<typename InputT::value_type>()
					+ cufftdx::Type<cufftdx::fft_type::c2c>()
					+ cufftdx::Direction<cufftdx::fft_direction::forward>()
					+ cufftdx::ElementsPerThread<4>()
					+ cufftdx::SM<800>()
					+ cufftdx::Block());

		using FFT = decltype(FFTBase() + cufftdx::FFTsPerBlock<FFTBase::suggested_ffts_per_block>());

		if (N_t_in % (cufftdx::size_of<FFT>::value * 32))
		{
			std::stringstream ss;
			ss << "N_t_in (" << N_t_in << ") must be a multiple of 32*FFTSize (" << 32 * FFTSize << ')';
			throw std::runtime_error(ss.str());
		}

		const size_t tileSize = cufftdx::size_of<FFT>::value * 32;
		dim3 gridDim(N_t_in / tileSize, cute::size<0>(data));

		kernel::chunked_fft<FFT><<<gridDim, FFT::block_dim, FFT::shared_memory_size, stream>>>(
				data, I, N_t_in);

		return 0;
	}
};


template <size_t TIME_INTEGRATION_>
class ChunkedFFT<1, TIME_INTEGRATION_>
{
public:
	static constexpr size_t FFTSize = 1;
	static constexpr size_t TIME_INTEGRATION = TIME_INTEGRATION_;
	static constexpr int THREADS_PER_BLOCK = 256;

public:
	template <typename Input2DArray, typename Output2DArray>
	static int run(Input2DArray data, Output2DArray I, cudaStream_t stream=NULL)
	{
		using InputT = typename Input2DArray::value_type;

		const dim3 gridDim(CUB_QUOTIENT_CEILING(cute::size(data), TIME_INTEGRATION));

		kernel::sqmod_kernel<TIME_INTEGRATION, THREADS_PER_BLOCK><<<gridDim, THREADS_PER_BLOCK, 0, stream>>>(data, I);

		return 0;
	}
};
