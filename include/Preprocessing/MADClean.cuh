#pragma once

#include <cuda.h>
#include <cute/tensor.hpp>

#ifdef __CUDACC__
#include <cooperative_groups.h>
#include <cub/util_macro.cuh>
#include <cub/block/block_merge_sort.cuh>
#include <cub/block/block_reduce.cuh>
#include <cub/iterator/transform_input_iterator.cuh>
#endif // __CUDACC__

#include <type_traits>
#include <limits>


enum class CleanType
{
    SATURATE,
    FILL_ZERO,
    FILL_NAN,
    FILL_MEDIAN
};

#ifdef __CUDACC__
namespace kernel {
namespace cg = cooperative_groups;

struct Less
{
    template <typename T>
    __device__ __forceinline__ bool operator()(const T& lhs, const T& rhs)
    {
        return lhs < rhs;
    }
};


namespace detail {

enum class MedianEpilogueType
{
    ACCURATE,
    FAST,
};

template <MedianEpilogueType type=MedianEpilogueType::ACCURATE>
struct MedianEpilogue
{
    /**
     * @pre temp_storage must be set to 0
    */
    template <int ITEMS_PER_THREAD, typename T, typename TyGroup>
    __device__ __forceinline__ static void run(
        const TyGroup& group,
        T& out, int n_valid,
        const T (&thread_data)[ITEMS_PER_THREAD],
        T& temp_storage)
    {
        // Take the middle to have a better approximation, but more heavy

        if ((n_valid % 2) == 0)
        {
            const auto idx = n_valid / 2;

            if (group.thread_rank() == ((idx - 1) / ITEMS_PER_THREAD))
                atomicAdd(&temp_storage, thread_data[(idx - 1) % ITEMS_PER_THREAD] / 2);
            if (group.thread_rank() == (idx / ITEMS_PER_THREAD))
                atomicAdd(&temp_storage, thread_data[idx % ITEMS_PER_THREAD] / 2);

        }
        else
        {
            const auto idx = n_valid / 2;

            if (group.thread_rank() == idx / ITEMS_PER_THREAD)
                temp_storage = thread_data[idx % ITEMS_PER_THREAD];
        }

        group.sync();

        if (group.thread_rank() == 0)
            out = temp_storage;
    }
};


template <>
struct MedianEpilogue<MedianEpilogueType::FAST>
{
    template <int ITEMS_PER_THREAD, typename T, typename TyGroup>
    __device__ __forceinline__ static void run(
        const TyGroup& group,
        T& out, int n_valid,
        const T (&thread_data)[ITEMS_PER_THREAD],
        T&)
    {
        const auto idx = CUB_QUOTIENT_CEILING(n_valid, 2);
        if (group.thread_rank() == idx / ITEMS_PER_THREAD)
            out = thread_data[idx % ITEMS_PER_THREAD];
    }
};
} // namespace detail

template <int THREADS_PER_BLOCK, int ITEMS_PER_THREAD, typename Tensor, typename Workspace>
__global__ __launch_bounds__(THREADS_PER_BLOCK)
void median_reduce(const Tensor tensor, Workspace workspace)
{
    using T = typename Tensor::value_type;
    static_assert(std::is_same_v<T, typename Workspace::value_type>);
    static constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;

    const auto compact_tensor_layout = cute::make_layout(
        tensor.shape(),
        cute::GenRowMajor{}
    );


    using BlockMergeSort = cub::BlockMergeSort<T, THREADS_PER_BLOCK, ITEMS_PER_THREAD>;
    using BlockReduce = cub::BlockReduce<int, THREADS_PER_BLOCK>;

    __shared__ union {
        typename BlockMergeSort::TempStorage sort;
        typename BlockReduce::TempStorage    reduce;
        struct {
            T median;
            int nans;
        } epilog;
     } temp_storage;


    cg::grid_group   grid  = cg::this_grid();
    cg::thread_block block = cg::this_thread_block();

    const int n_tiles = CUB_QUOTIENT_CEILING(cute::size(tensor), ITEMS_PER_BLOCK);
    for (int i = grid.block_rank(); i < n_tiles; i += grid.num_blocks())
    {
        T thread_data[ITEMS_PER_THREAD];
        int nans = 0;

        const int n_valid = min(ITEMS_PER_BLOCK, (int)cute::size(tensor) - i * ITEMS_PER_BLOCK);
#pragma unroll
        for (int j = 0; j < ITEMS_PER_THREAD; j++)
        {
            const size_t idx = (size_t) i * ITEMS_PER_BLOCK
                + j * THREADS_PER_BLOCK + block.thread_rank();
            
            
            thread_data[j] = (idx < cute::size(tensor)) ? tensor(compact_tensor_layout.get_hier_coord(idx)) : std::numeric_limits<T>::max();
            if (isnan(thread_data[j]))
            {
                thread_data[j] = std::numeric_limits<T>::max();
                nans++;
            }
        }

        nans = BlockReduce(temp_storage.reduce).Sum(nans);
        block.sync();

        BlockMergeSort(temp_storage.sort).Sort(thread_data, Less());
        block.sync();

        if (block.thread_rank() == 0)
            temp_storage.epilog.nans = nans;
        if (block.thread_rank() == 32)
            temp_storage.epilog.median = T();
        block.sync();

        
        detail::MedianEpilogue<detail::MedianEpilogueType::FAST>::run(
            block,
            workspace[i],
            n_valid - temp_storage.epilog.nans,
            thread_data,
            temp_storage.epilog.median);
    }
}


namespace detail {
template <CleanType ct>
struct ClipDispatch;

template <>
struct ClipDispatch<CleanType::SATURATE>
{
    template <typename T>
    static __device__ __forceinline__ T clip(const T& x, const T& min, const T& max, const T&)
    {
        return cute::min(cute::max(x, min), max);
    }
};

template <>
struct ClipDispatch<CleanType::FILL_ZERO>
{
    template <typename T>
    static __device__ __forceinline__ T clip(const T& x, const T& min, const T& max, const T&)
    {
        return ((min <= x) && (x <= max)) ? x : T();
    }
};

template <>
struct ClipDispatch<CleanType::FILL_NAN>
{
    template <typename T>
    static __device__ __forceinline__ T clip(const T& x, const T& min, const T& max, const T&)
    {
        return ((min <= x) && (x <= max)) ? x : std::numeric_limits<T>::quiet_NaN();
    }
};

template <>
struct ClipDispatch<CleanType::FILL_MEDIAN>
{
    template <typename T>
    static __device__ __forceinline__ T clip(const T& x, const T& min, const T& max, const T& median)
    {
        return ((min <= x) && (x <= max)) ? x : median;
    }
};
} // namespace detail

template <CleanType ct, int THREADS_PER_BLOCK, typename Tensor>
__global__ __launch_bounds__(THREADS_PER_BLOCK)
void clean(const Tensor in,
    Tensor out,
    const typename Tensor::value_type  threshold,
    const typename Tensor::value_type* median,
    const typename Tensor::value_type* mad)
{
    using T = typename Tensor::value_type;
    cg::grid_group grid = cg::this_grid();

    const auto compact_layout = cute::make_layout(
        in.shape(),
        cute::GenRowMajor{}
    );

    const T _median = *median;
    const T _mad    = *mad;

    for (size_t i = grid.thread_rank(); i < size(in); i += grid.num_threads())
    {
        const auto coord = compact_layout.get_hier_coord(i);
        out(coord) = detail::ClipDispatch<ct>::clip(
            in(coord),
            _median - threshold * _mad,
            _median + threshold * _mad,
            _median);
    }
}


template <int THREADS_PER_BLOCK, int ITEMS_PER_THREAD, typename T1, typename T2>
__global__ __launch_bounds__(THREADS_PER_BLOCK) 
void per_row_mean(const T1 in, T2 means)
{
    using T = typename T1::value_type;
    using BlockReduce = cub::BlockReduce<T, THREADS_PER_BLOCK>;


    __shared__ typename BlockReduce::TempStorage temp_storage;

    cg::grid_group grid = cg::this_grid();
    cg::thread_block block = cg::this_thread_block();

    constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;
    const auto colChunks = CUB_QUOTIENT_CEILING(cute::size<1>(in), ITEMS_PER_BLOCK);

    for (int i = grid.block_rank(); i < cute::size<0>(in); i += grid.num_blocks())
    {
        T thread_data[ITEMS_PER_THREAD];
#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
            thread_data[k] = T();

        for (int j = block.thread_rank(); j < colChunks*ITEMS_PER_BLOCK; j += ITEMS_PER_BLOCK)
        {
#pragma unroll
            for (int k = 0; k < ITEMS_PER_THREAD; k++)
            {
                const int col_idx = j + k * THREADS_PER_BLOCK;
                if (col_idx < cute::size<1>(in))
                {
                    const auto x = in(cute::make_coord(i, col_idx));
                    if (!isnan(x))
                        thread_data[k] += x;
                }
            }
        }

#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
            thread_data[k] /= cute::size<1>(in);

        T mean = BlockReduce(temp_storage).Sum(thread_data);
        if (block.thread_rank() == 0)
            means(i) = mean;
    }
}

template <int THREADS_PER_BLOCK, int ITEMS_PER_THREAD, typename Tensor>
__global__ __launch_bounds__(THREADS_PER_BLOCK) 
void global_mean(const Tensor in, typename Tensor::value_type* mean)
{
    using T = typename Tensor::value_type;
    using BlockReduce = cub::BlockReduce<T, THREADS_PER_BLOCK>;

    const auto compact_in_layout = cute::make_layout(
        in.shape(),
        cute::GenRowMajor{}
    );


    __shared__ typename BlockReduce::TempStorage temp_storage;

    cg::grid_group grid = cg::this_grid();
    cg::thread_block block = cg::this_thread_block();

    constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;
    const auto nChunks = CUB_QUOTIENT_CEILING(cute::size(in), ITEMS_PER_BLOCK);

    T thread_data[ITEMS_PER_THREAD];
#pragma unroll
    for (int k = 0; k < ITEMS_PER_THREAD; k++)
        thread_data[k] = T();

    for (int i = grid.block_rank(); i < nChunks; i += grid.num_blocks())
    {
#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
        {
            const int idx = i * ITEMS_PER_BLOCK + k * THREADS_PER_BLOCK + block.thread_rank();
            if (idx < cute::size(in))
            {
                const auto x = in(compact_in_layout.get_hier_coord(idx));
                if (!isnan(x))
                    thread_data[k] += x;
            }
        }
    }

#pragma unroll
    for (int k = 0; k < ITEMS_PER_THREAD; k++)
        thread_data[k] /= cute::size(in);

    T local_mean = BlockReduce(temp_storage).Sum(thread_data);
    if (block.thread_rank() == 0)
        atomicAdd(mean, local_mean);
}

template <int THREADS_PER_BLOCK,
    int ITEMS_PER_THREAD,
    int FFTLEN,
    typename T1,
    typename T2,
    typename T3,
    typename T4>
__global__ __launch_bounds__(THREADS_PER_BLOCK) 
void rm_baseline(const T1 in,
                 T2 out,
                 T3 original_spectrum,
                 T4 spectrum,
                 const typename T1::value_type* median)
{
    using T = typename T1::value_type;
    
    cg::grid_group grid = cg::this_grid();
    cg::thread_block block = cg::this_thread_block();

    static constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;

    const auto tChunks = CUB_QUOTIENT_CEILING(cute::size<1>(in), (cute::constant<size_t, ITEMS_PER_BLOCK>{}));
    const auto nBlocks = cute::size<0>(in) * tChunks;

    const T _median = *median;

    for (int i = grid.block_rank(); i < nBlocks; i += grid.num_blocks())
    {
        int ichan = i / tChunks; // Frequency channel index
        int ifft  = i % tChunks; // Time sample chunk index

        const T original_spectrum_v = original_spectrum(ichan / FFTLEN);
        const T spectrum_v          = spectrum(ichan);

        T x[ITEMS_PER_THREAD];

        const int offset = ifft * ITEMS_PER_BLOCK + block.thread_rank();

        {
            auto row = in(cute::make_coord(ichan, cute::_));

#pragma unroll
            for (int k = 0; k < ITEMS_PER_THREAD; k++)
            {
                const auto idx = offset + k * THREADS_PER_BLOCK;
                if (idx < cute::size(row))
                    x[k] = row(idx);
            }
        }
        
        {
        auto row = out(cute::make_coord(ichan, cute::_));

#pragma unroll
            for (int k = 0; k < ITEMS_PER_THREAD; k++)
            {
                const auto idx = offset + k * THREADS_PER_BLOCK;

                const T norm_factor = (original_spectrum_v > 1e-5f) ? _median / original_spectrum_v : 1.0f; // Avoid exploding values on 0-mean
                if (idx < cute::size(row))
                    row(idx) = (x[k] - spectrum_v) * norm_factor + _median;
            }
        }
    }
}

template <int THREADS_PER_BLOCK,
    int ITEMS_PER_THREAD,
    typename T1,
    typename T2>
__global__ __launch_bounds__(THREADS_PER_BLOCK) 
void rm_mean(const T1 in,
                 T2 out,
                 const typename T1::value_type* mean)
{
    using T = typename T1::value_type;

    const auto compact_in_layout = cute::make_layout(
        in.shape(),
        cute::GenRowMajor{}
    );

    const auto compact_out_layout = cute::make_layout(
        out.shape(),
        cute::GenRowMajor{}
    );

    cg::grid_group grid = cg::this_grid();
    cg::thread_block block = cg::this_thread_block();

    constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;
    const auto nChunks = CUB_QUOTIENT_CEILING(cute::size(in), ITEMS_PER_BLOCK);

    const T _mean = *mean;
    T thread_data[ITEMS_PER_THREAD];

    for (int i = grid.block_rank(); i < nChunks; i += grid.num_blocks())
    {
#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
        {
            const int idx = i * ITEMS_PER_BLOCK + k * THREADS_PER_BLOCK + block.thread_rank();
            if (idx < cute::size(in))
            {
                thread_data[k] = in(compact_in_layout.get_hier_coord(idx));
            }
        }

#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
            thread_data[k] -= _mean;

#pragma unroll
        for (int k = 0; k < ITEMS_PER_THREAD; k++)
        {
            const int idx = i * ITEMS_PER_BLOCK + k * THREADS_PER_BLOCK + block.thread_rank();
            if (idx < cute::size(out))
                out(compact_out_layout.get_hier_coord(idx)) = thread_data[k];
        }
    }
}

} // namespace kernel
#endif // __CUDACC__


class Median
{
public:
    static constexpr size_t THREADS_PER_BLOCK = 256;
    static constexpr size_t ITEMS_PER_THREAD  = 4;

    template <typename InputEngine, typename InputLayout>
    static size_t dry_run(const cute::Tensor<InputEngine, InputLayout>& tensor)
    {
        using T = typename InputEngine::value_type;

        constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;
        size_t ws_size = CUB_QUOTIENT_CEILING(cute::size(tensor), ITEMS_PER_BLOCK);
        return (ws_size + CUB_QUOTIENT_CEILING(ws_size, ITEMS_PER_BLOCK)) * sizeof(T);
    }

#ifdef __CUDACC__
    template <typename InputEngine, typename InputLayout>
    static void run(
        const cute::Tensor<InputEngine, InputLayout>& tensor,
        typename InputEngine::value_type* median,
        void* workspace,
        cudaStream_t stream=NULL)
    {
        using T = typename InputEngine::value_type;
        constexpr int ITEMS_PER_BLOCK = THREADS_PER_BLOCK * ITEMS_PER_THREAD;

        size_t ws_size = CUB_QUOTIENT_CEILING(tensor.size(), ITEMS_PER_BLOCK);

        T* workspaces[2] = {
            reinterpret_cast<T*>(workspace), // ws_size T
            reinterpret_cast<T*>(workspace) + ws_size // CUB_QUOTIENT_CEILING(ws_size, ITEMS_PER_BLOCK)) T
        };

        uint8_t buf_idx(0);
        auto _workspace1 = cute::make_tensor(cute::make_gmem_ptr(workspaces[buf_idx]), ws_size);
        kernel::median_reduce<THREADS_PER_BLOCK, ITEMS_PER_THREAD>
            <<<ws_size, THREADS_PER_BLOCK, 0, stream>>>(tensor, _workspace1);

        buf_idx ^= 1;

        while (ws_size > 1)
        {
            ws_size = CUB_QUOTIENT_CEILING(ws_size, ITEMS_PER_BLOCK);
            auto _workspace2 = cute::make_tensor(cute::make_gmem_ptr<T>((ws_size > 1) ? workspaces[buf_idx] : median), ws_size);

            kernel::median_reduce<THREADS_PER_BLOCK, ITEMS_PER_THREAD>
                <<<ws_size, THREADS_PER_BLOCK, 0, stream>>>(_workspace1, _workspace2);

            _workspace1 = _workspace2;
            buf_idx ^= 1;
        }
    }
#endif // __CUDACC__

};

#ifdef __CUDACC__
// Computes the Mean of rows of a rank 2 tensor.
// TODO: adapt to any shape
class perRowMean
{
public:

    static constexpr size_t THREADS_PER_BLOCK = 256;
    static constexpr size_t ITEMS_PER_THREAD  = 4;

    template <typename InputEngine, typename InputLayout,
        typename MeansEngine, typename MeansLayout>
    static void run(const cute::Tensor<InputEngine, InputLayout>& in,
        cute::Tensor<MeansEngine, MeansLayout>& means,
        cudaStream_t stream=NULL)
    {
        dim3 gridDim(cute::size<0>(in));

        kernel::per_row_mean<THREADS_PER_BLOCK, ITEMS_PER_THREAD>
            <<<gridDim, THREADS_PER_BLOCK, 0, stream>>>(in, means);
    }
};

// TODO: merge with perRowMean: mean<0, 1, ...> based on cute's size, get, ... functions
class globalMean
{
public:

    static constexpr size_t THREADS_PER_BLOCK = 256;
    static constexpr size_t ITEMS_PER_THREAD  = 4;

    template <typename Engine, typename Layout>
    static void run(const cute::Tensor<Engine, Layout>& in,
        typename Engine::value_type* means,
        cudaStream_t stream=NULL)
    {
        using T = typename Engine::value_type;

        dim3 gridDim(CUB_QUOTIENT_CEILING(cute::size(in), THREADS_PER_BLOCK * ITEMS_PER_THREAD));

        cudaMemsetAsync(means, 0x00, sizeof(T), stream);
        kernel::global_mean<THREADS_PER_BLOCK, ITEMS_PER_THREAD>
            <<<gridDim, THREADS_PER_BLOCK, 0, stream>>>(in, means);
    }
};

namespace detail {
template <typename T>
class AbsoluteDeviation
{
public:
    AbsoluteDeviation(const T* median) : median(median) {}
    __device__ __forceinline__ T operator()(const T& x) const {
        return cute::abs(x - *median);
    }
private:
    const T* median;
};
} // namespace detail
#endif // __CUDACC__

class MADClean
{
public:
    static constexpr int THREADS_PER_BLOCK = 256;


    template <typename InputEngine, typename InputLayout>
    static size_t dry_run(const cute::Tensor<InputEngine, InputLayout>& in)
    {
            return Median::dry_run(in) + 2 * sizeof(typename InputEngine::value_type);
    }

#ifdef __CUDACC__
    template <CleanType ct=CleanType::FILL_MEDIAN, typename InputEngine, typename InputLayout,
        typename OutputEngine, typename OutputLayout>
    static std::pair<typename InputEngine::value_type*, typename InputEngine::value_type*>
    run(const cute::Tensor<InputEngine, InputLayout>& in,
        cute::Tensor<OutputEngine, OutputLayout>& out,
        const typename InputEngine::value_type& threshold,
        void* workspace,
        cudaStream_t stream=NULL)
    {
        using T = typename InputEngine::value_type;
        T* _workspace = reinterpret_cast<T*>(workspace);
        dim3 clip_dim(CUB_QUOTIENT_CEILING(size(in), THREADS_PER_BLOCK));

        T* median = _workspace++;
        T* mad    = _workspace++;


        Median::run(in, median, _workspace, stream);

        detail::AbsoluteDeviation absolute_deviation_engine(median);

        using AbsoluteDeviationIter = cub::TransformInputIterator<T,
            decltype(absolute_deviation_engine),
            typename InputEngine::iterator>;
        
        AbsoluteDeviationIter absolute_deviation_iter(in.data(), absolute_deviation_engine);
        using AbsoluteDeviationTensor = cute::Tensor<
            cute::ConstViewEngine<AbsoluteDeviationIter>, 
            InputLayout>;
        AbsoluteDeviationTensor absolute_deviation_tensor(absolute_deviation_iter, in.layout());

        Median::run(absolute_deviation_tensor, mad, _workspace, stream);
        kernel::clean<ct, THREADS_PER_BLOCK><<<clip_dim, THREADS_PER_BLOCK, 0, stream>>>(
            in, out, threshold, median, mad
        );

        return std::make_pair(median, mad);
    }
#endif // __CUDACC__
};


template <int FFTLEN>
struct RmBaseline
{
    static constexpr int ITEMS_PER_THREAD = 4;
    static constexpr int THREADS_PER_BLOCK = 256;

    template <typename InputEngine, typename InputLayout>
    static size_t dry_run(const cute::Tensor<InputEngine, InputLayout>& in)
    {
        using T = typename InputEngine::value_type;

        return (cute::size<0>(in) + cute::size<0>(in) / FFTLEN) * sizeof(T);
    }

#ifdef __CUDACC__
    template <typename InputEngine, typename InputLayout,
        typename OutputEngine, typename OutputLayout>
    static void run(const cute::Tensor<InputEngine, InputLayout>& in,
             cute::Tensor<OutputEngine, OutputLayout>& out,
             const typename InputEngine::value_type* median,
             void* workspace,
             cudaStream_t stream=NULL)
    {
        using T = typename InputEngine::value_type;


        T* _workspace = reinterpret_cast<T*>(workspace);
        auto spectrum = cute::make_tensor(
            cute::make_gmem_ptr(_workspace),
            cute::make_layout(
                cute::make_shape(cute::size<0>(in)),
                cute::GenRowMajor{}
            )
        );

        auto spectrum_reshaped = cute::make_tensor(
            cute::make_gmem_ptr(_workspace),
            cute::make_layout(
                cute::make_shape(cute::size<0>(in) / cute::constant<size_t, FFTLEN>{}, cute::constant<size_t, FFTLEN>{}),
                cute::GenRowMajor{}
            )
            );

        auto original_spectrum = cute::make_tensor(
            cute::make_gmem_ptr(_workspace + cute::size(spectrum)),
            cute::make_layout(
                cute::make_shape(cute::size<0>(in) / cute::constant<size_t, FFTLEN>{}),
                cute::GenRowMajor{}
            )
        );

        perRowMean::run(in, spectrum, stream);
        perRowMean::run(spectrum_reshaped, original_spectrum, stream);

        dim3 gridDim(cute::size<0>(in) * CUB_QUOTIENT_CEILING(cute::size<1>(in), ITEMS_PER_THREAD * THREADS_PER_BLOCK));
        kernel::rm_baseline<THREADS_PER_BLOCK, ITEMS_PER_THREAD, FFTLEN>
            <<<gridDim, THREADS_PER_BLOCK, 0, stream>>>(in, out, original_spectrum, spectrum, median);
    }
#endif // __CUDACC__
};


struct RmMean
{
    static constexpr int ITEMS_PER_THREAD = 4;
    static constexpr int THREADS_PER_BLOCK = 256;

    template <typename InputEngine, typename InputLayout>
    static size_t dry_run(const cute::Tensor<InputEngine, InputLayout>& in)
    {
        return sizeof(typename InputEngine::value_type);
    }

#ifdef __CUDACC__
    template <typename InputEngine, typename InputLayout,
        typename OutputEngine, typename OutputLayout>
    static void run(const cute::Tensor<InputEngine, InputLayout>& in,
             cute::Tensor<OutputEngine, OutputLayout>& out,
             void* workspace,
             cudaStream_t stream=NULL)
    {
        using T = typename InputEngine::value_type;

        T* mean = reinterpret_cast<T*>(workspace);

        globalMean::run(in, mean, stream);

        dim3 gridDim(CUB_QUOTIENT_CEILING(cute::size(in), ITEMS_PER_THREAD * THREADS_PER_BLOCK));
        kernel::rm_mean<THREADS_PER_BLOCK, ITEMS_PER_THREAD>
            <<<gridDim, THREADS_PER_BLOCK, 0, stream>>>(in, out, mean);
    }
#endif // __CUDACC__
};
