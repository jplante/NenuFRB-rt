#pragma once

#include <cuda.h>
#include <cute/tensor.hpp>
#include <limits>

#ifdef __CUDACC__
#include <cooperative_groups.h>
#include <cub/block/block_reduce.cuh>
#include <cub/device/device_merge_sort.cuh>
#include <cute/tensor.hpp>

namespace cg = cooperative_groups;
#endif // __CUDACC__


template <typename T>
struct Record
{
	using value_type = T;
	int i_h;
	int i_w;
	T snr;
};

#ifdef __CUDACC__
namespace kernel {

namespace detail {
__device__ __forceinline__ float _sqrt(const float x)
{
	return sqrtf(x);
}

__device__ __forceinline__ double _sqrt(const double x)
{
	return sqrt(x);
}
} // namespace detail

template <typename T, int THREADS_PER_BLOCK, int ITEMS_PER_THREAD>
class BlockRms
{
	using BlockReduce = cub::BlockReduce<T, THREADS_PER_BLOCK>;

public:
	using TempStorage = typename BlockReduce::TempStorage;

	__device__ __forceinline__
	BlockRms(TempStorage& temp_storage) : _temp_storage(temp_storage) {}

	template <typename Engine, typename Layout>
	__device__ __forceinline__ T Rms(cg::thread_block& block, const cute::Tensor<Engine, Layout>& X)
	{
		static_assert(std::is_same_v<T, typename Engine::value_type>);
		static constexpr int ITEMS_PER_BLOCK = ITEMS_PER_THREAD * THREADS_PER_BLOCK;

		const auto compact_X_layout = cute::make_layout(
			X.shape(),
			cute::GenRowMajor{}
		);

		T thread_data[ITEMS_PER_THREAD];

#pragma unroll
		for (int k = 0; k < ITEMS_PER_THREAD; k++)
			thread_data[k] = T();

		for (int i = block.thread_rank(); i < cute::size(X); i += ITEMS_PER_BLOCK)
		{
#pragma unroll
			for (int k = 0; k < ITEMS_PER_THREAD; k++)
			{
				const int idx = i + k * THREADS_PER_BLOCK;
				if (idx < cute::size(X))
				{
					const T x = X(compact_X_layout.get_hier_coord(idx));
					thread_data[k] += x*x;
				}
			}
		}

#pragma unroll
		for (int k = 0; k < ITEMS_PER_THREAD; k++)
			thread_data[k] /= cute::size(X);

		return detail::_sqrt(BlockReduce(_temp_storage).Sum(thread_data));
	}

private:
	TempStorage& _temp_storage;
};


template <int ITEMS_PER_THREAD, int THREADS_PER_BLOCK,
	typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
__global__ __launch_bounds__(THREADS_PER_BLOCK)
void per_row_rms(const cute::Tensor<InputEngine, InputLayout> in,
	cute::Tensor<OutputEngine, OutputLayout> rmss)
{
	using InputTensor  = cute::Tensor<InputEngine,  InputLayout>;
	using OutputTensor = cute::Tensor<OutputEngine, OutputLayout>;

	using InputT  = typename InputEngine::value_type;
	using OutputT = typename OutputEngine::value_type;

	using BlockRms_ = BlockRms<InputT, THREADS_PER_BLOCK, ITEMS_PER_THREAD>;

	__shared__ typename BlockRms_::TempStorage temp_storage;

	auto grid  = cg::this_grid();
	auto block = cg::this_thread_block();

	for (int i_h = grid.block_rank(); i_h < cute::size<0>(in); i_h += grid.num_blocks())
	{
		const auto row = in(cute::make_coord(i_h, cute::_));
		const InputT rms = BlockRms_(temp_storage).Rms(block, row);

		if (block.thread_rank() == 0)
			rmss(i_h) = rms;
	}
}

 template<
	typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
__global__ void copy_records(const cute::Tensor<InputEngine, InputLayout> full_sorted,
	cute::Tensor<OutputEngine, OutputLayout> result)
{
	auto grid = cg::this_grid();

	using RecordT = typename OutputEngine::value_type;
	using T = typename RecordT::value_type;
	for (int i = grid.thread_rank(); i < cute::size(result); i += grid.num_threads())
	{
		RecordT record = (i < cute::size(full_sorted)) ? full_sorted(i)
			: RecordT{-1, -1, std::numeric_limits<T>::lowest()};

		if (record.snr == std::numeric_limits<T>::lowest())
		{
			record = RecordT {-1, -1, std::numeric_limits<T>::quiet_NaN()};
		}
		
		result(i) = record;
	}
}
} // namespace kernel


namespace detail {

template <typename T>
struct get_shape;

template <typename Shape, typename Stride>
struct get_shape<cute::Layout<Shape, Stride>>
{
	using type = Shape;
};

template <typename T>
using get_shape_t = typename get_shape<T>::type;

template <typename InputEngine, typename InputLayout,
	typename RmsEngine, typename RmsLayout,
	typename IndexType=size_t,
	typename DifferenceType=int64_t>
class A2RecordIterator
{
private:
	using T = typename InputEngine::value_type;

	static_assert(std::is_same_v<
		typename InputEngine::value_type,
		typename RmsEngine::value_type>);

public:
	using difference_type   = DifferenceType;
	using value_type        = Record<T>;
	using pointer           = value_type*;
	using reference         = value_type&;
	using iterator_category = std::random_access_iterator_tag;

	using index_type        = IndexType;

	A2RecordIterator(const cute::Tensor<InputEngine, InputLayout>& in_,
				     const cute::Tensor<RmsEngine,   RmsLayout>&   rmss_,
					 const T threshold_)
		: in(in_), rmss(rmss_),
		  compact_in_layout(cute::make_layout(in_.shape(), cute::GenRowMajor{})),
		  threshold(threshold_) {}


	/// Array subscript
	__device__ __forceinline__ value_type operator[](index_type n) const
	{
		const auto coord = compact_in_layout.get_hier_coord(idx + n);
		const T snr = in(coord) / rmss(cute::get<0>(coord));

		return value_type {
			static_cast<int>(cute::get<0>(coord)),
			static_cast<int>(cute::get<1>(coord)),
			(snr > threshold) ? snr : std::numeric_limits<T>::lowest()
		};
	}

	// Definition of all member functions, just in case (based on CUB's transform iterator)
	/// Postfix increment
	__device__ __forceinline__ decltype(auto) operator++(int)
	{
		A2RecordIterator retval = *this;
		idx++;
		return retval;
	}

	/// Prefix increment
	__device__ __forceinline__ decltype(auto) operator++()
	{
		idx++;
		return *this;
	}

	/// Addition
	__device__ __forceinline__ decltype(auto) operator+(index_type n) const
	{
		A2RecordIterator retval(*this);
		retval.idx += n;
		return retval;
	}

	/// Addition assignment
	__device__ __forceinline__ decltype(auto) operator+=(index_type n)
	{
		idx += n;
		return *this;
	}

	/// Subtraction
	__device__ __forceinline__ decltype(auto) operator-(index_type n) const
	{
		A2RecordIterator retval(*this);
		retval.idx -= n;
		return retval;
	}

	/// Subtraction assignment
	__device__ __forceinline__ decltype(auto) operator-=(index_type n)
	{
		idx -= n;
		return *this;
	}

	/// Distance
	__device__ __forceinline__ difference_type operator-(A2RecordIterator other) const
	{
		return static_cast<difference_type>(idx) - static_cast<difference_type>(other.idx);
	}

	/// Dereference
	__device__ __forceinline__ value_type operator*() const
	{
		return this->operator[](0);
	}

	/// Equal to
	__device__ __forceinline__ bool operator==(const A2RecordIterator& rhs) const
	{
		return (idx == rhs.idx);
	}

	/// Not equal to
	__device__ __forceinline__ bool operator!=(const A2RecordIterator& rhs) const
	{
		return (idx != rhs.idx);
	}

private:
	const cute::Tensor<InputEngine, InputLayout> in;
	const cute::Tensor<RmsEngine,   RmsLayout>   rmss;
	const cute::Layout<get_shape_t<InputLayout>, cute::GenRowMajor::Apply<get_shape_t<InputLayout>>> compact_in_layout;
	const T threshold;
	index_type idx{ 0ull };
};

struct RecordGreaterThan
{
	template <typename T>
	__device__ __forceinline__
	bool operator()(const Record<T>& lhs, const Record<T>& rhs)
	{
		return lhs.snr > rhs.snr;
	}
};
} // namespace detail
#endif // __CUDACC__


class Adaptive1D
{
	static constexpr int THREADS_PER_BLOCK = 256;
	static constexpr int ITEMS_PER_THREAD  = 4;

#ifdef __CUDACC__
	template <typename InputEngine, typename InputLayout>
	__host__ static size_t device_sort_ws_size(const cute::Tensor<InputEngine, InputLayout>& in)
	{
		using T = typename InputEngine::value_type;
		size_t ws_size = 0;
		cub::DeviceMergeSort::SortKeysCopy(nullptr,
			ws_size,
			reinterpret_cast<Record<T>*>(NULL),
			reinterpret_cast<Record<T>*>(NULL),
			static_cast<size_t>(cute::size(in)),
			detail::RecordGreaterThan{});

		return ws_size;
	}
#endif // __CUDACC__

public:
	static constexpr int size = THREADS_PER_BLOCK * ITEMS_PER_THREAD;

	template <typename InputEngine, typename InputLayout>
	__host__ static size_t dry_run(const cute::Tensor<InputEngine, InputLayout>& in)
	{
		using T = typename InputEngine::value_type;
		return cute::size<0>(in) * sizeof(T) // for rms
			+ cute::size(in) * sizeof(Record<T>) // For sort result
			+ device_sort_ws_size(in); // For CUB's sort
	}

#ifdef __CUDACC__
	template <typename InputEngine, typename InputLayout,
		      typename OutputEngine, typename OutputLayout>
	static int run(const cute::Tensor<InputEngine, InputLayout>& in,
					cute::Tensor<OutputEngine, OutputLayout>& out,
					const typename InputEngine::value_type threshold,
					void* workspace,
					cudaStream_t stream=NULL)
	{
		using InputT = typename InputEngine::value_type;

		static_assert(std::is_convertible_v<
			Record<typename InputEngine::value_type>,
				   typename OutputEngine::value_type>, "OutputT is not a valid struct to describe hits");


		uintptr_t _workspace = reinterpret_cast<uintptr_t>(workspace);
		auto rmss = cute::make_tensor(
			cute::make_gmem_ptr(reinterpret_cast<InputT*>(_workspace)),
			cute::make_layout(
				cute::make_shape(cute::size<0>(in)),
				cute::GenRowMajor{}
			)
		);

		auto records_sorted_full = cute::make_tensor(
			cute::make_gmem_ptr(reinterpret_cast<Record<InputT>*>(
				_workspace + cute::size(rmss) * sizeof(InputT))),
			cute::make_layout(
				cute::make_shape(cute::size(in)),
				cute::GenRowMajor{}
			)
		);

		void* sort_ws = reinterpret_cast<void*>(
			_workspace + cute::size(rmss) * sizeof(InputT)
			           + cute::size(records_sorted_full) * sizeof(Record<InputT>)
		);
	

		kernel::per_row_rms<ITEMS_PER_THREAD, THREADS_PER_BLOCK>
			<<<static_cast<unsigned>(cute::size<0>(in)), THREADS_PER_BLOCK, 0, stream>>>(in, rmss);

		detail::A2RecordIterator a2record_iter(in, rmss, threshold);
		
		size_t sort_ws_size = device_sort_ws_size(in); // Has to be an lvalue

		cub::DeviceMergeSort::SortKeysCopy(sort_ws,
			sort_ws_size,
			a2record_iter,
			records_sorted_full.data().get(),
			static_cast<size_t>(cute::size(in)),
			detail::RecordGreaterThan{},
			stream);


		kernel::copy_records<<<
			CUB_QUOTIENT_CEILING(cute::size(out), THREADS_PER_BLOCK),
			THREADS_PER_BLOCK,
			0,
			stream>>>(records_sorted_full, out);
		
		return 0;
	}

#endif // __CUDACC__
};
