// Custom stride to use in CuTe layouts
// Enables better flexibility than the good ol'
// RingAdapter2D
// -> Easily change shape, layout, looping axis / axes
//    enable ColMajor or RowMajor storage, or even more exotic
//    layouts, dynamic or static everything, ...

#pragma once
#include <cute/numeric/integral_constant.hpp>

namespace cute {

template <int stride_, int mod_, typename Index_>
struct RingBufferStride : Int<stride_>
{
  using Int<stride_>::value;
  using value_type = typename Int<stride_>::value_type;
  using type = RingBufferStride<stride_, mod_, Index_>;

  using Index = Index_;
  static constexpr int stride = stride_;
  static constexpr int mod = mod_;

  constexpr __host__ __device__ __forceinline__
  RingBufferStride(const Index head=Index()) noexcept : head(head) {}

  Index head;
};

template <int stride, int mod, typename Index>
CUTE_HOST_DEVICE constexpr
Underscore
operator*(Underscore, RingBufferStride<stride, mod, Index>) {
  return {};
}

template <int stride, int mod, typename Index>
CUTE_HOST_DEVICE constexpr
Underscore
operator*(RingBufferStride<stride, mod, Index>, Underscore) {
  return {};
}

#define CUTE_BINARY_OP_MODULO(OP)                                              \
  template <int stride, int mod, typename Index, class T>                      \
  CUTE_HOST_DEVICE constexpr                                                   \
  decltype(stride OP ((T() + Index()) % mod))                                  \
  operator OP (RingBufferStride<stride, mod, Index> s, const T t) {            \
    return stride OP ((t + s.head) % mod);                                     \
  }                                                                            \
  template <class T, int stride, typename Index, int mod>                      \
  CUTE_HOST_DEVICE constexpr                                                   \
  decltype(((T() + Index()) % mod) OP stride)                                  \
  operator OP (const T t, RingBufferStride<stride, mod, Index> s) {            \
    return ((t + s.head) % mod) OP stride;                                     \
  }

CUTE_BINARY_OP_MODULO(*);

#undef CUTE_BINARY_OP_MODULO


template <int I, typename Engine, typename Layout, typename Index>
auto advance_head(cute::Tensor<Engine, Layout>& tensor, const Index offset)
{
  static_assert(I == 1, "Hardcoded for I == 1 for simplicity for now");
  
  auto ring_stride = stride<I>(tensor);
  auto new_head = (ring_stride.head + offset) % ring_stride.mod;
  ring_stride.head = new_head;

  tensor = make_tensor(
    tensor.data(),
    make_layout(
      tensor.shape(),
      make_shape(stride<0>(tensor), ring_stride)
      )
  );

  return new_head;
}

template <int I, typename Engine, typename Layout, typename Index>
auto set_head(cute::Tensor<Engine, Layout>& tensor, const Index offset)
{
  static_assert(I == 1, "Hardcoded for I == 1 for simplicity for now");
  
  auto ring_stride = stride<I>(tensor);
  auto new_head = offset % ring_stride.mod;
  ring_stride.head = new_head;

  tensor = make_tensor(
    tensor.data(),
    make_layout(
      tensor.shape(),
      make_shape(stride<0>(tensor), ring_stride)
      )
  );

  return new_head;
}


/* Cleaner way to do it, in theory (doesn't work. TODO: debug)
template <int I, typename Stride, typename Index>
auto advance_head(std::remove_const_t<Stride>& stride, const Index offset)
{
  return get<I>(stride).advance_head(offset);
}

template <int I, typename Shape, typename Stride, typename Index>
auto advance_head(cute::Layout<Shape, Stride>& layout, const Index offset)
{
  return advance_head<I>(layout.stride(), offset);
}

template <int I, typename Engine, typename Shape, typename Stride, typename Index>
auto advance_head(cute::Tensor<Engine, cute::Layout<Shape, std::remove_const_t<Stride>>>& tensor, const Index offset)
{
  return advance_head<I>(tensor.layout(), offset);
}

template <int I, typename Stride, typename Index>
auto set_head(std::remove_const_t<Stride>& stride, const Index offset)
{
  return get<I>(stride).set_head(offset);
}

template <int I, typename Shape, typename Stride, typename Index>
auto set_head(cute::Layout<Shape, std::remove_const_t<Stride>>& layout, const Index offset)
{
  return set_head<I>(layout.stride(), offset);
}

template <int I, typename Engine, typename Shape, typename Stride, typename Index>
auto set_head(cute::Tensor<Engine, cute::Layout<Shape, std::remove_const_t<Stride>>>& tensor, const Index offset)
{
  return set_head<I>(tensor.layout(), offset);
}
*/
} // namespace cute
