#pragma once

#include <cuda.h>
#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>
#include <cub/block/block_reduce.cuh>
#include <type_traits>


namespace cg = cooperative_groups;

namespace kernel
{

template <int THREADS_PER_BLOCK, typename Input2DArray>
__global__ __launch_bounds__(THREADS_PER_BLOCK)
void memcpy_from_array(typename Input2DArray::value_type* dst, const Input2DArray I, const size_t n_elems)
{
	for (int tid = blockIdx.x * blockDim.x + threadIdx.x; tid < n_elems * I.height(); tid += blockDim.x * gridDim.x)
	{
		int i_w = tid % n_elems;
		int i_h = tid / n_elems;

		dst[tid] = I(i_h, i_w);
	}
}


template <int fold_factor, int THREADS_PER_BLOCK, typename Op, typename Input2DArray>
__global__ __launch_bounds__(THREADS_PER_BLOCK)
void xfold_array(typename Op::OutputT* dst, const Input2DArray I, const size_t W)
{
	static_assert(fold_factor >= THREADS_PER_BLOCK, "Not yet implemented for a fold lesser than block size");
	using InputT  = typename Input2DArray::value_type;
	using OutputT = typename Op::OutputT;

	static_assert(std::is_same_v<InputT, typename Op::InputT>);
	constexpr OutputT zero = OutputT();

	using BlockReduce = cub::BlockReduce<OutputT, THREADS_PER_BLOCK>;
	__shared__ typename BlockReduce::TempStorage temp_storage;


	Op op;

	cg::grid_group   grid  = cg::this_grid();
	cg::thread_block block = cg::this_thread_block();

	const size_t width_chunks = (W + fold_factor - 1) / fold_factor;

	for (int gid = grid.block_rank(); gid < I.height() * width_chunks; gid += grid.num_blocks())
	{
		const int i_h = gid / width_chunks;
		const int i_c = gid % width_chunks;

		OutputT acc(zero);
		for (int i_w = block.thread_rank(); i_w < fold_factor; i_w += block.num_threads())
		{
			const InputT x = I(i_h, i_c * fold_factor + i_w);
			acc += op(x);
		}

		acc = BlockReduce(temp_storage).Sum(acc / fold_factor);

		if (block.thread_rank() == 0)
			dst[i_h * width_chunks + i_c] = acc;
	}
}
} // namespace kernel

template <typename Input2DArray>
void memcpy_from_array(typename Input2DArray::value_type* dst, const Input2DArray I, const size_t n_elems, cudaStream_t stream=NULL)
{
	constexpr int nThreads = 512;
	const size_t nBlocks = (n_elems * I.height() + nThreads - 1) / nThreads;

	kernel::memcpy_from_array<nThreads><<<nBlocks, nThreads, 0, stream>>>(dst, I, n_elems);
}

template <typename T>
struct NoOp
{
	using InputT  = T;
	using OutputT = T;

	__host__ __device__ __forceinline__ OutputT operator()(const InputT x) const
	{
		return x;
	}
};

template <typename InputT_>
struct SqmodOp
{
	using InputT  = InputT_;
	using OutputT = typename InputT::value_type;
	
	__host__ __device__ __forceinline__ OutputT operator()(const InputT x) const
	{
		return x.real() * x.real() + x.imag() * x.imag();
	}
};

template <int fold_factor, typename Op, typename Input2DArray>
void xfold_array(typename Op::OutputT* dst, const Input2DArray& I, const size_t W, cudaStream_t stream=NULL)
{
	constexpr int nThreads = 512;
	const size_t width_chunks = (W + fold_factor - 1) / fold_factor;

	kernel::xfold_array<fold_factor, nThreads, Op>
		<<<I.height() * width_chunks, nThreads, 0, stream>>>(dst, I, W);
}

template <typename T>
class Array1D
{
	using value_type = T;

public:
	__host__ __device__ Array1D() = delete;
	__host__ __device__ Array1D(T* data) : data(data) {}
	__host__ __device__  ~Array1D() { };
	__host__ __device__ __forceinline__  T& operator()(const int64_t i);
	__host__ __device__ __forceinline__  const T& operator()(const int64_t i) const;
	__host__ __device__ __forceinline__  int64_t size() const;

/*
	template <typename Group, typename Shape, typename cuda::thread_scope Scope>
	__host__ __device__ __forceinline__  void memcpy_async(const Group &group, T* __restrict__ _dst, const int64_t i, const Shape &shape, cuda::pipeline<Scope>& pipeline);
	template <typename Group, typename Shape, typename cuda::thread_scope Scope>
	__host__ __device__ __forceinline__  void memcpy_async(const Group &group, const int64_t i, T* __restrict__ _dst, const Shape &shape, cuda::pipeline<Scope>& pipeline);
*/

protected:
	T* data;
};

template <typename T>
class Array2D
{
	using value_type = T;

public:
	__host__ __device__ Array2D() = delete;
	__host__ __device__ Array2D(T* data) : data(data) {}
	__host__ __device__  ~Array2D() { };
	__host__ __device__ __forceinline__  T& operator()(const int64_t i, const int64_t j);
	__host__ __device__ __forceinline__  const T& operator()(const int64_t i, const int64_t j) const;
	__host__ __device__ __forceinline__  int64_t height() const;
	__host__ __device__ __forceinline__  int64_t width() const;

/*
	template <typename Group, typename Shape, typename cuda::thread_scope Scope>
	__host__ __device__ __forceinline__  void memcpy_async(const Group &group, T* __restrict__ _dst, const int64_t i, const int64_t j, const Shape &shape, cuda::pipeline<Scope>& pipeline);
	template <typename Group, typename Shape, typename cuda::thread_scope Scope>
	__host__ __device__ __forceinline__  void memcpy_async(const Group &group, const int64_t i, const int64_t j, T* __restrict__ _dst, const Shape &shape, cuda::pipeline<Scope>& pipeline);
*/

protected:
	T* data;
};

