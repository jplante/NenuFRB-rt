#pragma once

template <size_t _size>
class Bitset
{
    using T = uint32_t;
    static constexpr size_t size_bits = 8 * sizeof(T);
	static constexpr size_t n_elems = (_size + size_bits - 1) / size_bits;

public:
    __host__ __device__ __forceinline__ bool get(size_t i) const
    {
        return data[i / size_bits] & (T(1) << (i % size_bits));
    }

    __host__ __device__ __forceinline__ void set(size_t i)
    {
        data[i / size_bits] |= (T(1) << (i % size_bits));
    }

    __host__ __device__ __forceinline__ void set_all()
    {
	for (int i = 0; i < n_elems * sizeof(T); i++)
		reinterpret_cast<char*>(data)[i] = '\xff';
    }

    template <typename TyGroup>
    __device__ __forceinline__ void set_all(TyGroup& group)
    {
	for (int i = group.thread_rank(); i < n_elems * sizeof(T); i += group.num_threads())
		reinterpret_cast<char*>(data)[i] = '\xff';
    }


    __host__ __device__ __forceinline__ void reset(size_t i)
    {
        data[i / size_bits] &= ~(T(1) << (i % size_bits));
    }

    __host__ __device__ __forceinline__ void reset_all()
    {
	for (int i = 0; i < n_elems * sizeof(T); i++)
		reinterpret_cast<char*>(data)[i] = '\x00';
    }

    template <typename TyGroup>
    __device__ __forceinline__ void reset_all(TyGroup& group)
    {
	for (int i = group.thread_rank(); i < n_elems * sizeof(T); i += group.num_threads())
		reinterpret_cast<char*>(data)[i] = '\x00';
    }

	__host__ __device__ __forceinline__ bool all() const
	{
		bool _all = true;
#pragma unroll
		for (int i = 0; i < n_elems; i++)
			_all &= (data[i] == UINT32_MAX);
		return _all;
	}

	__host__ __device__ __forceinline__ int popc() const
	{
		int _popc = 0;
#pragma unroll
		for (int i = 0; i < n_elems; i++)
		{
#ifdef __CUDACC__
			_popc += __popc(data[i]);
#else
			_popc += __builtin_popcount(data[i]);
#endif
		}
		return _popc;
	}

private:
    T data[n_elems];
};

