// No header guard to enable functionality incrementally
// -> __CUFILE_H_ may not be defined on the first include
// but still needed later on, on another include

#include <iostream>
#include <sstream>
#include <stdexcept>

#ifndef checkCudaErrors
#define checkCudaErrors(val) check((val), #val, __FILE__, __LINE__)
#endif


#if defined(__cuda_cuda_h__) && !defined(check_CUresult)
#define check_CUresult
inline void check(CUresult result, char const *const func, const char *const file, int const line)
{
	if (result)
	{
		const char* errstr;
		cuGetErrorName(result, &errstr);
		std::ostringstream ss;
		ss << "CUDA Driver error at " << file << ':' << line << " code=" << static_cast<unsigned int>(result)
			<< '(' << errstr << ')' << " \"" << func << '"';

		throw std::runtime_error(ss.str());
	}
}
#endif

#if defined(__CUDA_RUNTIME_H__) && !defined(check_cudaError_t)
#define check_cudaError_t
inline void check(cudaError_t result, char const *const func, const char *const file, int const line)
{
	if (result)
	{
		std::ostringstream ss;
		ss << "CUDA Runtime error at " << file << ':' << line << " code=" << static_cast<unsigned int>(result)
			<< '(' << cudaGetErrorName(result) << ')' << " \"" << func << '"';

		throw std::runtime_error(ss.str());
	}
}
#endif

#if defined(__CUFILE_H_) && !defined(check_CUfileError_t)
#define check_CUfileError_t
inline void check(CUfileError_t result, char const *const func, const char *const file, int const line)
{
	if (result.err)
	{
		if (result.err > CUFILEOP_BASE_ERR)
		{
			std::ostringstream ss;
			ss << "cuFile error at " << file << ':' << line << " code=" << static_cast<unsigned int>(result.err)
				<< '(' << cufileop_status_error((CUfileOpError)result.err) << ')' << " \"" << func << '"';

			throw std::runtime_error(ss.str());
		}
		else
			check(result.cu_err, func, file, line);
	}
}
#endif


