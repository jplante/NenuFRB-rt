#pragma once

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cufile.h>
#include <cute/tensor.hpp>
#include <filesystem>

#include "helper_cuda.cuh"

template <typename Engine, typename Layout>
void dump(const std::filesystem::path& path, const cute::Tensor<Engine, Layout>& tensor)
{
    using Tensor = cute::Tensor<Engine, Layout>;

    int fd = open(path.c_str(), O_CREAT | O_WRONLY | O_DIRECT | O_TRUNC, 0644);
    if (fd == -1)
    {
        std::stringstream ss;
        ss << "Could not open " << path << "for writing: " << strerror(errno) << std::endl;
    }

    CUfileHandle_t fh;
    CUfileDescr_t descr;
    memset(&descr, 0, sizeof(CUfileDescr_t));

    descr.handle.fd = fd;
    descr.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;
    checkCudaErrors(cuFileHandleRegister(&fh, &descr));
    checkCudaErrors(cuFileBufRegister(tensor.data().get(), cute::size(tensor) * sizeof(typename Tensor::value_type), 0));

    cuFileWrite(fh, tensor.data().get(), cute::size(tensor) * sizeof(typename Tensor::value_type), 0, 0);

    checkCudaErrors(cuFileBufDeregister(tensor.data().get()));
    cuFileHandleDeregister(fh);
    close(fd);
}
