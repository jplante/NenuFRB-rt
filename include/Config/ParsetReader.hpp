#pragma once

#include <string>
#include <vector>
#include <istream>
#include <unordered_map>
#include <chrono>


class ParsetReader
{
public:
	class Value
	{
	public:
		using TimePoint = std::chrono::time_point<std::chrono::steady_clock>;

		Value() = default;
		Value(const std::string& s) : value(s) {}
		bool as_bool() const;
		int as_int() const;
		float as_float() const;
		std::vector<int> as_vector() const;
		const std::string& as_string() const;
		std::pair<int, int> as_range() const;
		std::tm as_time() const;
	private:
		std::string value;
	};

	using KeyT    = std::string;
	using ValueT  = Value;
	using ObjectT = std::unordered_map<KeyT, ValueT>;
	using ParSetT = std::unordered_map<KeyT, ObjectT>;

public:
	ParsetReader(const std::string& fname) { read(fname); }
	ParsetReader(std::istream& stream) { read(stream); }

	int read(const std::string& fname);
	int read(std::istream& stream);

	const ValueT& get(const std::string& object, const std::string& field) const;

	int n_elems(std::string list_name) const;

private:
	ParSetT parset;
};

