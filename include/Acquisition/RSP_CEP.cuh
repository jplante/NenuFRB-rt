#pragma once

#include <cstdint>
#include <rte_ip.h>
#include <rte_gpudev.h>

#include <cute/tensor.hpp>
#include <cutlass/complex.h>
#include <ring_buffer_stride.hpp>



template <uint8_t BM>
struct complex_t;

template <>
struct complex_t<0> {
	int16_t re;
	int16_t im;
};

template <>
struct complex_t<1> {
	int8_t re;
	int8_t im;
};

template <>
struct complex_t<2> {
	int8_t re : 4;
	int8_t im : 4;
};


template <uint8_t BM>
struct beamlet_t
{
	complex_t<BM> X;
	complex_t<BM> Y;
};


struct header_t
{
  uint8_t       VERSION_ID;
  uint8_t       RSP_ID          : 5,
                                : 1,
                ERR             : 1,
                F200M           : 1;

  uint8_t       BM              : 2,
                                : 2,
                NOF_BEAMLET_HI  : 4;

  uint8_t       CONFIGURATION_ID;
  uint16_t      STATION_ID;
  uint8_t       NOF_BEAMLET_LO;
  uint8_t       NOF_BLOCKS;
  uint32_t      TIMESTAMP;
  uint32_t      BLOCK_SEQUENCE_NUMBER;
} __attribute__((packed));



constexpr uint32_t fs_coders = 200'000'000u;  // 2MHz out of the coders
constexpr uint32_t fsx2 = fs_coders / 512; // 1024 FFT -> actual frequency received is fs_coders / 1024
										   // Because fs_coders is not divisible by 1024 but is by 512
										   // We divide here by 512, and one last time by 2 after multiplying
										   // to the timestamp to obtain the final position in memory.

/** Full BSN, counting from POSIX epoch at 200e6/1024 Hz
 */
template <typename T>
static __host__ __device__ __forceinline__ T full_bsn(const struct header_t* h)
{
	return (static_cast<T>(h->TIMESTAMP)*T(fsx2) + T(1))/T(2) + static_cast<T>(h->BLOCK_SEQUENCE_NUMBER);
}


class RSP_CEP
{
public:

	static constexpr int N_BHR = 4;

	static constexpr rte_be32_t BHR_MCAST_0_IP   = RTE_STATIC_BSWAP32(RTE_IPV4(224, 2, 3, 1));
	static constexpr rte_be16_t BHR_MCAST_0_PORT = RTE_STATIC_BSWAP16(0x04DA);

	static constexpr rte_be32_t BHR_MCAST_1_IP   = RTE_STATIC_BSWAP32(RTE_IPV4(224, 2, 3, 2));
	static constexpr rte_be16_t BHR_MCAST_1_PORT = RTE_STATIC_BSWAP16(0x15D2);

	static constexpr rte_be32_t BHR_MCAST_2_IP   = RTE_STATIC_BSWAP32(RTE_IPV4(224, 2, 3, 3));
	static constexpr rte_be16_t BHR_MCAST_2_PORT = RTE_STATIC_BSWAP16(0x04DA);

	static constexpr rte_be32_t BHR_MCAST_3_IP   = RTE_STATIC_BSWAP32(RTE_IPV4(224, 2, 3, 4));
	static constexpr rte_be16_t BHR_MCAST_3_PORT = RTE_STATIC_BSWAP16(0x15D2);


	static constexpr uint16_t MTU = 9000;
	static constexpr size_t RING_WIDTH = 1ul << 14;
	static constexpr size_t REDUNDANCY = 8;
	static constexpr uint16_t BEAMLETS_PER_BHR = 192;
	static constexpr uint16_t N_F = N_BHR * BEAMLETS_PER_BHR; // Maximum number of beamlets
	static constexpr bool START_ON_EVEN = true;


	using value_type = cutlass::complex<float>;	
	static constexpr int NUM_THREADS = 512;
	static constexpr int NUM_BLOCKS = 4;

	RSP_CEP();
	~RSP_CEP();
	
	RSP_CEP(const int16_t nic_dpdk_id, const int16_t gpu_dpdk_id) { init(nic_dpdk_id, gpu_dpdk_id); }
	int init(const int16_t nic_dpdk_id, const int16_t gpu_dpdk_id);

	RSP_CEP(const struct rte_pci_addr* nic_pci_addr, const struct rte_pci_addr* gpu_pci_addr) { init(nic_pci_addr, gpu_pci_addr); }
	int init(const struct rte_pci_addr* nic_pci_addr, const struct rte_pci_addr* gpu_pci_addr);

	constexpr auto data_ring() const
	{
		return cute::make_tensor(
			cute::make_gmem_ptr(data_ring_storage),
			cute::make_layout(
				cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, RING_WIDTH>{}),
				cute::make_stride(
					cute::constant<size_t, REDUNDANCY * RING_WIDTH>{},
					cute::RingBufferStride<1, REDUNDANCY * RING_WIDTH, size_t>{})
			)
		);
	}

	constexpr auto header_ring() const
	{
		return cute::make_tensor(
			cute::make_gmem_ptr(header_ring_storage),
			cute::make_layout(
				cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, RING_WIDTH>{}),
				cute::make_stride(
					cute::constant<size_t, REDUNDANCY * RING_WIDTH>{},
					cute::RingBufferStride<1, REDUNDANCY * RING_WIDTH, size_t>{})
			)
		);
	}

	int start(const unsigned lcore_id);
	int stop();



	uint64_t latest_timestamp() { return RTE_GPU_VOLATILE(*latest_timestamp_h); }

private:

	constexpr auto full_data_ring() const
	{
		return cute::make_tensor(
			cute::make_gmem_ptr(data_ring_storage),
			cute::make_layout(
				cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, REDUNDANCY * RING_WIDTH>{}),
				cute::make_stride(
					cute::constant<size_t, REDUNDANCY * RING_WIDTH>{},
					cute::RingBufferStride<1, REDUNDANCY * RING_WIDTH, size_t>{})
			)
		);
	}

	constexpr auto full_header_ring() const
	{
		return cute::make_tensor(
			cute::make_gmem_ptr(header_ring_storage),
			cute::make_layout(
				cute::make_shape(cute::constant<size_t, N_F>{}, cute::constant<size_t, REDUNDANCY * RING_WIDTH>{}),
				cute::make_stride(
					cute::constant<size_t, REDUNDANCY * RING_WIDTH>{},
					cute::RingBufferStride<1, REDUNDANCY * RING_WIDTH, size_t>{})
			)
		);
	}

	uint16_t port;
	int16_t dpdk_gpu_id;
	int     cuda_gpu_id;

	value_type* data_ring_storage;
	struct header_t* header_ring_storage;


	struct rte_mempool* mpool;

	struct rte_pktmbuf_extmem* extmem;
	struct rte_flow* udp_only_flow;

	struct rte_gpu_comm_list* comm_list;
	enum   rte_gpu_comm_list_status* packet_processing_status;


	uint32_t running = false;
	uint32_t* d_quit_flag;
	uint32_t* h_quit_flag;
	uint64_t* latest_timestamp_h, * latest_timestamp_d;
	unsigned lcore;

	cudaStream_t persistent_stream;


	static int poll_nic(void* args);
};


