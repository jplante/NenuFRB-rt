
message(STATUS "Setting capabilities on ${CMAKE_INSTALL_PREFIX}/bin/NenuFRB")
execute_process(COMMAND sudo setcap cap_net_raw,cap_dac_override=eip ${CMAKE_INSTALL_PREFIX}/bin/NenuFRB)
