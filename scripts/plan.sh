#!/bin/bash

NENUFRB_ROOT="$(dirname $0)/.."
NENUFRB_CMD="$NENUFRB_ROOT/build/src/NenuFRB -a c9:00.0 -a c3:00.0 -l 64-66 --log-level=user1:debug -- --dump_I --dump_A"
PROCESS_DUMP_CMD="/home/nenufarobs/.conda/envs/py310/bin/ipython -- $NENUFRB_ROOT/python/process_dump.py --save"

for parset in $1/*.parset
do
    startTime=`awk -F= '$1 == "Observation.startTime" {print $2}' $parset`
    stopTime=`awk -F= '$1 == "Observation.stopTime" {print $2}' $parset`
    parsetBasename=`basename $parset`
    outDir=$(dirname $parset)/"${parsetBasename%.*}"
    mkdir -p $outDir

    curTime=`date "+%Y-%m-%dT%H:%M:%SZ"`

    if [[ $curTime < $stopTime ]]
    then
        startTimeTouch=`date -d $startTime "+%Y%m%d%H%M.%S"`
        stopTimeTouch=`date -d $stopTime "+%Y%m%d%H%M.%S"`

        sudo sh -c "echo '$NENUFRB_CMD $parset 2>&1 | tee $outDir/stdio.log' | at -q m -t $startTimeTouch"
        echo "$PROCESS_DUMP_CMD $outDir" | at -q p -t $stopTimeTouch
    else
        echo "We are past $parset, ignoring"
    fi
done
