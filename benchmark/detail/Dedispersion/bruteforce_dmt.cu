#include <Dedispersion/BruteforceDmt.cuh>
#include "../benchmark.cuh"
#include <cute/layout.hpp>

namespace detail {
template <>
struct BenchmarkContext::Storage<Mode::BruteforceDmt>
{
    using T = float;
    static constexpr size_t FFTLEN = 1;
    static constexpr size_t TIME_INTEGRATION = 1024;
    static constexpr size_t FOLD_FACTOR = FFTLEN * TIME_INTEGRATION;

    using InputEngine  = cute::ViewEngine<cute::gmem_ptr<T>>;
    using OutputEngine = cute::ViewEngine<cute::gmem_ptr<T>>;

    using InputShape  = cute::Shape<size_t, size_t>;
    using OutputShape = cute::Shape<size_t, size_t>;

    using InputLayout  = cute::Layout<InputShape,  cute::GenRowMajor::Apply<InputShape>>;
    using OutputLayout = cute::Layout<OutputShape, cute::GenRowMajor::Apply<OutputShape>>;

    using InputTensor  = cute::Tensor<InputEngine,  InputLayout>;
    using OutputTensor = cute::Tensor<OutputEngine, OutputLayout>;

    Storage(const size_t _N_f, const size_t _N_t_in,
        const size_t _N_dm, const size_t _N_t_out,
        
        const size_t N,
        const float fmin, const float fmax,
        const float DMmin, const float DMmax) : N_f(_N_f), N_t_in(_N_t_in), N_dm(_N_dm), N_t_out(_N_t_out),
        in(InputEngine{NULL}, InputLayout{}), out(OutputEngine{NULL}, OutputLayout{})
    {
        cfg.N = N;
        cfg.fmin = fmin;
        cfg.fmax = fmax;
        cfg.DMmin = DMmin;
        cfg.DMmax = DMmax;
        cfg.dt = 1024.0f * TIME_INTEGRATION / 200e6f;
    }

    InputTensor  in;
    OutputTensor out;

    const size_t N_f, N_t_in, N_dm, N_t_out;
    Dmt::Config cfg;
};


template <>
void BenchmarkContext::alloc<Mode::BruteforceDmt>(BenchmarkContext::Storage<Mode::BruteforceDmt>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::BruteforceDmt>;
    storage.in = alloc_tensor<typename Storage::InputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_f, storage.N_t_in),
            cute::GenRowMajor{}
        )
    );
    
    storage.out = alloc_tensor<typename Storage::OutputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_dm, storage.N_t_out),
            cute::GenRowMajor{}
        )
    );
}

template <>
void BenchmarkContext::pre<Mode::BruteforceDmt>(BenchmarkContext::Storage<Mode::BruteforceDmt>& storage)
{
    if (fill)
        fill_random(storage.in, 0.0f, 50.0f);
}

template <>
void BenchmarkContext::run<Mode::BruteforceDmt>(BenchmarkContext::Storage<Mode::BruteforceDmt>& storage)
{
    int i;
    checkCudaErrors(cudaGetDevice(&i));
    cudaStream_t stream = gpu_ctxs[i].stream;

    BruteforceDmt<Dmt::T0Ref::FMAX>::run(storage.in, storage.out, storage.cfg, stream);
}

template <>
void BenchmarkContext::free<Mode::BruteforceDmt>(BenchmarkContext::Storage<Mode::BruteforceDmt>& storage)
{
    free_tensor(std::move(storage.out));
    free_tensor(std::move(storage.in));
}

template std::pair<float, float> BenchmarkContext::bench<Mode::BruteforceDmt,
    size_t, size_t, size_t, size_t, size_t,
    float, float, float, float>(const size_t _N_f, const size_t _N_t_in,
        const size_t _N_dm, const size_t _N_t_out,
        
        const size_t N,
        const float fmin, const float fmax,
        const float DMmin, const float DMmax);
} // namespace detail


