#include "benchmark.cuh"
#include <numeric>
#include <algorithm>

namespace detail {
BenchmarkContext::BenchmarkContext(const size_t _n_rep, const bool _fill) : n_rep(_n_rep), records(_n_rep), fill(_fill)
{
    int n_gpus;
    checkCudaErrors(cudaGetDeviceCount(&n_gpus));

    std::vector<int> gpus(n_gpus);
    std::iota(gpus.begin(), gpus.end(), 0);

    construct(gpus, _n_rep, _fill);
}

BenchmarkContext::BenchmarkContext(std::vector<int>& gpus, const size_t _n_rep, const bool _fill) : BenchmarkContext(_n_rep, _fill)
{
    construct(gpus, _n_rep, _fill);
}

void BenchmarkContext::construct(std::vector<int>& gpus, const size_t n_rep, const bool fill)
{
    std::sort(gpus.begin(), gpus.end());
    auto new_end = std::unique(gpus.begin(), gpus.end());
    gpus.erase(new_end, gpus.end());

    for (int gpu : gpus)
    {
        checkCudaErrors(cudaSetDevice(gpu));

        curandGenerator_t _gen;
        curandCreateGenerator(&_gen, CURAND_RNG_PSEUDO_DEFAULT);

        cudaStream_t _stream;
        checkCudaErrors(cudaStreamCreate(&_stream));

        cudaEvent_t _start, _stop;
        checkCudaErrors(cudaEventCreateWithFlags(&_start, cudaEventBlockingSync));
        checkCudaErrors(cudaEventCreateWithFlags(&_stop, cudaEventBlockingSync));

        curandSetStream(_gen, _stream);

        cudaMemPool_t _mempool;
        checkCudaErrors(cudaDeviceGetDefaultMemPool(&_mempool, gpu));

        gpu_ctxs.insert({gpu, {_gen, _stream, _start, _stop, _mempool}});
    }
}

BenchmarkContext::~BenchmarkContext()
{
    for (auto& item : gpu_ctxs)
    {
        checkCudaErrors(cudaSetDevice(item.first));
        auto& gpu_ctx = item.second;

        checkCudaErrors(cudaStreamSynchronize(gpu_ctx.stream));

        curandDestroyGenerator(gpu_ctx.gen);
        checkCudaErrors(cudaStreamDestroy(gpu_ctx.stream));
        checkCudaErrors(cudaEventDestroy(gpu_ctx.start));
        checkCudaErrors(cudaEventDestroy(gpu_ctx.stop));
    }
}


float BenchmarkContext::mean()
{
    return (records / static_cast<float>(n_rep)).sum();
}

float BenchmarkContext::stddev(const float mean)
{
    auto x = (records - mean);
    auto variance = (x*x).sum() / n_rep;

    return std::sqrt(variance);
}
} // namespace detail
