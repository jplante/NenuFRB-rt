#include <cuda_runtime.h>
#include <curand.h>


#include <cute/tensor.hpp>
#include <utils/helper_cuda.cuh>
#include <vector>
#include <unordered_map>
#include <valarray>
#include <utility>

enum class Mode
{
    TimeIntegration,
    MADClean,
    RmBaseline,
    RmMean,
    BruteforceDmt,
    Adaptive1D,

    InvalidMode
};

namespace detail {
class BenchmarkContext
{
private:
    float mean();
    float stddev(float _mean);

    const size_t n_rep;
    const bool fill;

    struct GpuCtx {
        curandGenerator_t gen;
        cudaStream_t stream;
        cudaEvent_t start, stop;
        cudaMemPool_t mempool;
    };
    std::unordered_map<int, GpuCtx> gpu_ctxs;

    std::valarray<float> records;
    
    void* alloc(size_t bytes)
    {
        int i;
        checkCudaErrors(cudaGetDevice(&i));
        auto& gpu_ctx = gpu_ctxs[i];

        void* ptr;
        checkCudaErrors(cudaMallocAsync(&ptr, bytes, gpu_ctx.mempool, gpu_ctx.stream));
        return ptr;
    }

    template <typename T, typename Shape, typename Stride>
    decltype(auto) alloc_tensor(cute::Layout<Shape, Stride>&& layout)
    {
        T* ptr = reinterpret_cast<T*>(alloc(cute::size(layout) * sizeof(T)));
        return cute::make_tensor(
            cute::make_gmem_ptr(ptr),
            layout
        );
    }

    void free(void* ptr)
    {
        int i;
        checkCudaErrors(cudaGetDevice(&i));
        checkCudaErrors(cudaFreeAsync(ptr, gpu_ctxs[i].stream));
    }

    template <typename Engine, typename Layout>
    void free_tensor(cute::Tensor<Engine, Layout>&& tensor)
    {
        free(tensor.data().get());
    }

    template <typename Engine, typename Layout>
    void fill_random(cute::Tensor<Engine, Layout>& tensor, float mean=0.0f, float stddev=1.0f)
    {
        int i;
        checkCudaErrors(cudaGetDevice(&i));
        auto& gpu_ctx = gpu_ctxs[i];

        curandGenerateNormal(gpu_ctx.gen,
            reinterpret_cast<float*>(tensor.data().get()),
            cute::size(tensor) * sizeof(typename Engine::value_type) / sizeof(float),
            mean, stddev);
    }

    void construct(std::vector<int>& gpus, const size_t n_rep=64, const bool fill=true);

public:
    template <Mode mode>
    struct Storage;

    template <Mode mode, typename... Args>
    struct is_valid_configuration : std::false_type {};

    template <Mode mode, typename... Args>
    std::enable_if_t<is_valid_configuration<mode, Args...>::value, std::pair<float, float>> bench(const Args... args);

    BenchmarkContext(const size_t n_rep=64, const bool fill=true);
    BenchmarkContext(std::vector<int>& gpus, const size_t n_rep=64, const bool fill=true);
    ~BenchmarkContext();

private:
    template <Mode mode>
    void alloc(Storage<mode>& storage);

    template <Mode mode>
    void pre(Storage<mode>& storage) {}
    
    template <Mode mode>
    void run(Storage<mode>& storage);
    
    template <Mode mode>
    void post(Storage<mode>& storage) {}
    
    template <Mode mode>
    void free(Storage<mode>& storage);
};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::TimeIntegration, size_t, size_t> : std::true_type {};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::MADClean, size_t, size_t> : std::true_type {};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::RmBaseline, size_t, size_t> : std::true_type {};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::RmMean, size_t, size_t> : std::true_type {};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::BruteforceDmt, size_t, size_t, size_t, size_t, size_t, float, float, float, float> : std::true_type {};

template <>
struct BenchmarkContext::is_valid_configuration<Mode::Adaptive1D, size_t, size_t, size_t> : std::true_type {};


extern template std::pair<float, float> BenchmarkContext::bench<Mode::TimeIntegration, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);

extern template std::pair<float, float> BenchmarkContext::bench<Mode::MADClean, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);

extern template std::pair<float, float> BenchmarkContext::bench<Mode::RmBaseline, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);

extern template std::pair<float, float> BenchmarkContext::bench<Mode::RmMean, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);

extern template std::pair<float, float> BenchmarkContext::bench<Mode::BruteforceDmt,
    size_t, size_t, size_t, size_t, size_t,
    float, float, float, float>(const size_t _N_f, const size_t _N_t_in,
        const size_t _N_dm, const size_t _N_t_out,
        
        const size_t N,
        const float fmin, const float fmax,
        const float DMmin, const float DMmax);

extern template std::pair<float, float> BenchmarkContext::bench<Mode::Adaptive1D, size_t, size_t, size_t>(
    const size_t N_dm, const size_t N_t_out, const size_t N_candidates);


template <Mode mode, typename... Args>
std::enable_if_t<BenchmarkContext::is_valid_configuration<mode, Args...>::value, std::pair<float, float>>
BenchmarkContext::bench(const Args... args)
{
    std::vector<Storage<mode>> storages;
    storages.reserve(gpu_ctxs.size());

    for (auto& items: gpu_ctxs)
    {
        checkCudaErrors(cudaSetDevice(items.first));
        storages.emplace_back(args...);
        alloc(storages.back());
    }
    
    for (size_t i = 0ull; i < n_rep; i += gpu_ctxs.size())
    {
        int j = i;
        auto st_itr = storages.begin();
        for (auto& items : gpu_ctxs)
        {
            if (j >= n_rep)
                break;

            auto& storage = *st_itr++;
            
            auto& gpu_ctx = items.second;
            checkCudaErrors(cudaSetDevice(items.first));

            pre(storage);

            checkCudaErrors(cudaEventRecord(gpu_ctx.start, gpu_ctx.stream));
            run(storage);
            checkCudaErrors(cudaEventRecord(gpu_ctx.stop, gpu_ctx.stream));

            post(storage);
            j++;
        }

        j = i;
        for (auto& items : gpu_ctxs)
        {
            if (j >= n_rep)
                break;

            auto& gpu_ctx = items.second;

            checkCudaErrors(cudaSetDevice(items.first));

            checkCudaErrors(cudaEventSynchronize(gpu_ctx.stop));
            float t_ms;
            checkCudaErrors(cudaEventElapsedTime(&t_ms, gpu_ctx.start, gpu_ctx.stop));

            records[j] = t_ms * 1000;

            j++;
        }
    }


    auto st_itr = storages.begin();
    for (auto& items : gpu_ctxs)
    {
        checkCudaErrors(cudaSetDevice(items.first));
        free(*st_itr++);
    }

    float _mean = mean();
    float _stddev = stddev(_mean);

    return std::make_pair(_mean, _stddev);
}
} // namespace detail
