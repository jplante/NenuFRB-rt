#include <Preprocessing/MADClean.cuh>
#include "../benchmark.cuh"
#include <cute/layout.hpp>

namespace detail {
template <>
struct BenchmarkContext::Storage<Mode::RmBaseline>
{
    using T = float;
    static constexpr size_t FFTLEN = 1;

    using Engine  = cute::ViewEngine<cute::gmem_ptr<T>>;
    using Shape  = cute::Shape<size_t, size_t>;
    using Layout  = cute::Layout<Shape,  cute::GenRowMajor::Apply<Shape>>;
    using Tensor  = cute::Tensor<Engine, Layout>;

    Storage(const size_t _N_f, const size_t _N_t_in)
        : N_f(_N_f), N_t_in(_N_t_in),
        tensor(Engine{NULL}, Layout{})
    {}

    const size_t N_f, N_t_in;

    Tensor tensor;
    void* ws;
};

template <>
void BenchmarkContext::alloc<Mode::RmBaseline>(BenchmarkContext::Storage<Mode::RmBaseline>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::RmBaseline>;

    storage.tensor = alloc_tensor<typename Storage::Engine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_f, storage.N_t_in),
            cute::GenRowMajor{}
        )
    );

    auto ws_size = RmBaseline<Storage::FFTLEN>::dry_run(storage.tensor);
    storage.ws = alloc(ws_size);
}

template <>
void BenchmarkContext::pre<Mode::RmBaseline>(BenchmarkContext::Storage<Mode::RmBaseline>& storage)
{
    if (fill)
        fill_random(storage.tensor, 240.0f, 50.0f);
}

template <>
void BenchmarkContext::run<Mode::RmBaseline>(BenchmarkContext::Storage<Mode::RmBaseline>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::RmBaseline>;
    using T = typename Storage::Engine::value_type;

    int i;
    checkCudaErrors(cudaGetDevice(&i));
    cudaStream_t stream = gpu_ctxs[i].stream;

    RmBaseline<Storage::FFTLEN>::run(storage.tensor, storage.tensor, reinterpret_cast<T*>(storage.ws), storage.ws, stream);
}

template <>
void BenchmarkContext::free<Mode::RmBaseline>(BenchmarkContext::Storage<Mode::RmBaseline>& storage)
{
    free(storage.ws);
    free_tensor(std::move(storage.tensor));
}

template std::pair<float, float> BenchmarkContext::bench<Mode::RmBaseline, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);
} // namespace detail
