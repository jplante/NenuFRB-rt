#include <Preprocessing/MADClean.cuh>
#include "../benchmark.cuh"
#include <cute/layout.hpp>

namespace detail {
template <>
struct BenchmarkContext::Storage<Mode::MADClean>
{
    using T = float;

    using Engine  = cute::ViewEngine<cute::gmem_ptr<T>>;
    using Shape  = cute::Shape<size_t, size_t>;
    using Layout  = cute::Layout<Shape,  cute::GenRowMajor::Apply<Shape>>;
    using Tensor  = cute::Tensor<Engine, Layout>;

    Storage(const size_t _N_f, const size_t _N_t_in)
        : N_f(_N_f), N_t_in(_N_t_in),
        tensor(Engine{NULL}, Layout{})
    {}

    const size_t N_f, N_t_in;

    Tensor tensor;
    void* ws;
};

template <>
void BenchmarkContext::alloc<Mode::MADClean>(BenchmarkContext::Storage<Mode::MADClean>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::MADClean>;

    storage.tensor = alloc_tensor<typename Storage::Engine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_f, storage.N_t_in),
            cute::GenRowMajor{}
        )
    );

    auto ws_size = MADClean::dry_run(storage.tensor);
    storage.ws = alloc(ws_size);
}

template <>
void BenchmarkContext::pre<Mode::MADClean>(BenchmarkContext::Storage<Mode::MADClean>& storage)
{
    if (fill)
        fill_random(storage.tensor, 240.0f, 50.0f);
}

template <>
void BenchmarkContext::run<Mode::MADClean>(BenchmarkContext::Storage<Mode::MADClean>& storage)
{
    int i;
    checkCudaErrors(cudaGetDevice(&i));
    cudaStream_t stream = gpu_ctxs[i].stream;

    MADClean::run(storage.tensor, storage.tensor, 3.0f, storage.ws, stream);
}

template <>
void BenchmarkContext::free<Mode::MADClean>(BenchmarkContext::Storage<Mode::MADClean>& storage)
{
    free(storage.ws);
    free_tensor(std::move(storage.tensor));
}

template std::pair<float, float> BenchmarkContext::bench<Mode::MADClean, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);
} // namespace detail

