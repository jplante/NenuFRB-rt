#include <Preprocessing/ChunkedFFT.cuh>
#include "../benchmark.cuh"
#include <cutlass/complex.h>
#include <cute/layout.hpp>

namespace detail {
template <>
struct BenchmarkContext::Storage<Mode::TimeIntegration>
{
    using T = float;

    static constexpr size_t FFTLEN = 1;
    static constexpr size_t TIME_INTEGRATION = 1024;
    static constexpr size_t FOLD_FACTOR = FFTLEN * TIME_INTEGRATION;

    using InputEngine  = cute::ViewEngine<cute::gmem_ptr<cutlass::complex<T>>>;
    using OutputEngine = cute::ViewEngine<cute::gmem_ptr<T>>;

    using InputShape  = cute::Shape<size_t, size_t>;
    using OutputShape = cute::Shape<size_t, size_t>;

    using InputLayout  = cute::Layout<InputShape,  cute::GenRowMajor::Apply<InputShape>>;
    using OutputLayout = cute::Layout<OutputShape, cute::GenRowMajor::Apply<OutputShape>>;

    using InputTensor  = cute::Tensor<InputEngine,  InputLayout>;
    using OutputTensor = cute::Tensor<OutputEngine, OutputLayout>;

    Storage(const size_t _N_f, const size_t _N_t_in) : N_f(_N_f), N_t_in(_N_t_in),
        in(InputEngine{NULL}, InputLayout{}), out(OutputEngine{NULL}, OutputLayout{})
    {}

    const size_t N_f, N_t_in;

    InputTensor  in;
    OutputTensor out;
};

template <>
void BenchmarkContext::alloc<Mode::TimeIntegration>(BenchmarkContext::Storage<Mode::TimeIntegration>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::TimeIntegration>;
    storage.in = alloc_tensor<typename Storage::InputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_f, storage.N_t_in),
            cute::GenRowMajor{}
        )
    );
    
    storage.out = alloc_tensor<typename Storage::OutputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_f, storage.N_t_in / Storage::FOLD_FACTOR),
            cute::GenRowMajor{}
        )
    );
}

template <>
void BenchmarkContext::pre<Mode::TimeIntegration>(BenchmarkContext::Storage<Mode::TimeIntegration>& storage)
{
    if (fill)
        fill_random(storage.in, 0.0f, 1000.0f);
}

template <>
void BenchmarkContext::run<Mode::TimeIntegration>(BenchmarkContext::Storage<Mode::TimeIntegration>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::TimeIntegration>;

    int i;
    checkCudaErrors(cudaGetDevice(&i));
    cudaStream_t stream = gpu_ctxs[i].stream;

    ChunkedFFT<Storage::FFTLEN, Storage::TIME_INTEGRATION>::run(storage.in, storage.out, stream);
}

template <>
void BenchmarkContext::free<Mode::TimeIntegration>(BenchmarkContext::Storage<Mode::TimeIntegration>& storage)
{
    free_tensor(std::move(storage.out));
    free_tensor(std::move(storage.in));
}

template std::pair<float, float> BenchmarkContext::bench<Mode::TimeIntegration, size_t, size_t>(
    const size_t N_f, const size_t N_t_in);
} // namespace detail
