#include <Detection/Adaptive1D.cuh>
#include "../benchmark.cuh"
#include <cute/layout.hpp>

namespace detail {
template <>
struct BenchmarkContext::Storage<Mode::Adaptive1D>
{
    using T = float;

    using InputEngine  = cute::ViewEngine<cute::gmem_ptr<T>>;
    using OutputEngine = cute::ViewEngine<cute::gmem_ptr<Record<T>>>;

    using InputShape  = cute::Shape<size_t, size_t>;
    using OutputShape = cute::Shape<size_t>;

    using InputLayout  = cute::Layout<InputShape,  cute::GenRowMajor::Apply<InputShape>>;
    using OutputLayout = cute::Layout<OutputShape, cute::GenRowMajor::Apply<OutputShape>>;

    using InputTensor  = cute::Tensor<InputEngine,  InputLayout>;
    using OutputTensor = cute::Tensor<OutputEngine, OutputLayout>;

    Storage(const size_t _N_dm, const size_t _N_t_out, const size_t _N_candidates)
        : N_dm(_N_dm), N_t_out(_N_t_out), N_candidates(_N_candidates),
        in(InputEngine{NULL}, InputLayout{}), out(OutputEngine{NULL}, OutputLayout{})
    {}

    const size_t N_dm, N_t_out, N_candidates;

    InputTensor  in;
    OutputTensor out;
    void* ws;
};

template <>
void BenchmarkContext::alloc<Mode::Adaptive1D>(BenchmarkContext::Storage<Mode::Adaptive1D>& storage)
{
    using Storage = BenchmarkContext::Storage<Mode::Adaptive1D>;
    storage.in = alloc_tensor<typename Storage::InputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_dm, storage.N_t_out),
            cute::GenRowMajor{}
        )
    );
    
    storage.out = alloc_tensor<typename Storage::OutputEngine::value_type>(
        cute::make_layout(
            cute::make_shape(storage.N_candidates)
        )
    );

    auto ws_size = Adaptive1D::dry_run(storage.in);
    storage.ws = alloc(ws_size);
}

template <>
void BenchmarkContext::pre<Mode::Adaptive1D>(BenchmarkContext::Storage<Mode::Adaptive1D>& storage)
{
    if (fill)
        fill_random(storage.in, 0.0f, 1000.0f);
}

template <>
void BenchmarkContext::run<Mode::Adaptive1D>(BenchmarkContext::Storage<Mode::Adaptive1D>& storage)
{
    int i;
    checkCudaErrors(cudaGetDevice(&i));
    cudaStream_t stream = gpu_ctxs[i].stream;

    Adaptive1D::run(storage.in, storage.out, 3.0f, storage.ws, stream);
}

template <>
void BenchmarkContext::free<Mode::Adaptive1D>(BenchmarkContext::Storage<Mode::Adaptive1D>& storage)
{
    free(storage.ws);
    free_tensor(std::move(storage.out));
    free_tensor(std::move(storage.in));
}

template std::pair<float, float> BenchmarkContext::bench<Mode::Adaptive1D, size_t, size_t, size_t>(
    const size_t N_dm, const size_t N_t_out, const size_t N_candidates);
} // namespace detail
