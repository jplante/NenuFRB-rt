#define PY_SSIZE_T_CLEAN
#include <Python.h>
#include <structmember.h>
#include "detail/benchmark.cuh"

extern "C"
{
typedef struct 
{
    PyObject_HEAD
    detail::BenchmarkContext* ctx;
} BenchmarkContextObject;

static void
BenchmarkContext_dealloc(BenchmarkContextObject* self)
{
    delete self->ctx;
    Py_TYPE(self)->tp_free(reinterpret_cast<PyObject*>(self));
}

static int
BenchmarkContext_init(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    unsigned long long n_rep = 64ull;
    int fill = true;

    char kw0[] = "n_rep";
    char kw1[] = "fill";

    static char* kwlist[] = { kw0, kw1, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "|Kp", kwlist, &n_rep, &fill))
        return -1;
    
    self->ctx = new detail::BenchmarkContext(n_rep, fill);

    return 0;
}

static PyObject*
BenchmarkContext_time_integration(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_f, N_t_in;
    static char kw0[] = "N_f";
    static char kw1[] = "N_t_in";
    static char* kwlist[] = { kw0, kw1, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KK", kwlist, &N_f, &N_t_in))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::TimeIntegration>(
            static_cast<size_t>(N_f), static_cast<size_t>(N_t_in));
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyObject*
BenchmarkContext_mad_clean(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_f, N_t_in;
    static char kw0[] = "N_f";
    static char kw1[] = "N_t_in";
    static char* kwlist[] = { kw0, kw1, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KK", kwlist, &N_f, &N_t_in))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::MADClean>(
            static_cast<size_t>(N_f), static_cast<size_t>(N_t_in));
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyObject*
BenchmarkContext_rm_baseline(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_f, N_t_in;
    static char kw0[] = "N_f";
    static char kw1[] = "N_t_in";
    static char* kwlist[] = { kw0, kw1, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KK", kwlist, &N_f, &N_t_in))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::RmBaseline>(
            static_cast<size_t>(N_f), static_cast<size_t>(N_t_in));
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyObject*
BenchmarkContext_rm_mean(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_f, N_t_in;
    static char kw0[] = "N_f";
    static char kw1[] = "N_t_in";
    static char* kwlist[] = { kw0, kw1, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KK", kwlist, &N_f, &N_t_in))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::RmMean>(
            static_cast<size_t>(N_f), static_cast<size_t>(N_t_in));
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyObject*
BenchmarkContext_bruteforce_dmt(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_f, N_t_in, N_dm, N_t_out, N;
    float fmin, fmax, DMmin, DMmax;
    static char kw0[] = "N_f";
    static char kw1[] = "N_t_in";
    static char kw2[] = "N_dm";
    static char kw3[] = "N_t_out";
    static char kw4[] = "N";
    static char kw5[] = "fmin";
    static char kw6[] = "fmax";
    static char kw7[] = "DMmin";
    static char kw8[] = "DMmax";
    static char* kwlist[] = { kw0, kw1, kw2, kw3, kw4, kw5, kw6, kw7, kw8, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KKKKKffff", kwlist, &N_f, &N_t_in, &N_dm, &N_t_out, &N, &fmin, &fmax, &DMmin, &DMmax))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::BruteforceDmt>(
            static_cast<size_t>(N_f), static_cast<size_t>(N_t_in), static_cast<size_t>(N_dm), static_cast<size_t>(N_t_out),
            static_cast<size_t>(N), fmin, fmax, DMmin, DMmax);
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyObject*
BenchmarkContext_adaptive1d(BenchmarkContextObject* self, PyObject* args, PyObject* kwds)
{
    if (self->ctx == NULL)
        return NULL;
    
    unsigned long long N_dm, N_t_out, N_candidates;
    static char kw0[] = "N_dm";
    static char kw1[] = "N_t_out";
    static char kw2[] = "N_candidates";
    static char* kwlist[] = { kw0, kw1, kw2, NULL };

    if (!PyArg_ParseTupleAndKeywords(args, kwds, "KKK", kwlist, &N_dm, &N_t_out, &N_candidates))
        return NULL;
    
    
    try
    {
        std::pair<float, float> stats = self->ctx->bench<Mode::Adaptive1D>(
            static_cast<size_t>(N_dm), static_cast<size_t>(N_t_out), static_cast<size_t>(N_candidates));
        return Py_BuildValue("(f,f)", stats.first, stats.second);
    }
    catch (const std::runtime_error& e)
    {
        return NULL;
    }
}

static PyMethodDef BenchmarkContext_methods[] = {
    { "time_integration", (PyCFunction)(void(*)(void))BenchmarkContext_time_integration,
        METH_VARARGS | METH_KEYWORDS, "Benchmark time integration algorithm"},
    { "mad_clean",        (PyCFunction)(void(*)(void))BenchmarkContext_mad_clean,
        METH_VARARGS | METH_KEYWORDS, "Benchmark MAD clean algorithm"},
    { "rm_baseline",      (PyCFunction)(void(*)(void))BenchmarkContext_rm_baseline,
        METH_VARARGS | METH_KEYWORDS, "Benchmark rm baseline algorithm"},
    { "rm_mean",          (PyCFunction)(void(*)(void))BenchmarkContext_rm_mean,
        METH_VARARGS | METH_KEYWORDS, "Benchmark rm mean algorithm"},
    { "bruteforce_dmt",   (PyCFunction)(void(*)(void))BenchmarkContext_bruteforce_dmt,
        METH_VARARGS | METH_KEYWORDS, "Benchmark Bruteforce Dispersion Measure Transform algorithm"},
    { "adaptive1d",       (PyCFunction)(void(*)(void))BenchmarkContext_adaptive1d,
        METH_VARARGS | METH_KEYWORDS, "Benchmark Adaptive 1D detection algorithm"},
    { NULL, NULL, 0, NULL}
};


static PyTypeObject BenchmarkContextType = {
    PyVarObject_HEAD_INIT(NULL, 0)
    .tp_name = "benchmark.BenchmarkContext",
    .tp_basicsize = sizeof(BenchmarkContextObject),
    .tp_itemsize = 0,
    .tp_dealloc = (destructor) BenchmarkContext_dealloc,
    .tp_flags = Py_TPFLAGS_DEFAULT,
    .tp_doc = PyDoc_STR("Context to benchmark NenuFRB algorithms"),
    .tp_methods = BenchmarkContext_methods,
    .tp_init = (initproc) BenchmarkContext_init,
    .tp_new = PyType_GenericNew
};

static PyModuleDef benchmarkmodule = {
    PyModuleDef_HEAD_INIT,
    .m_name = "benchmark",
    .m_doc = "Benchmark NenuFRB algorithms",
    .m_size = -1,
};

PyMODINIT_FUNC
PyInit_benchmark()
{
    PyObject* m;
    if (PyType_Ready(&BenchmarkContextType) < 0)
        return NULL;

    m = PyModule_Create(&benchmarkmodule);
    if (m == NULL)
        return NULL;

    Py_INCREF(&BenchmarkContextType);
    if (PyModule_AddObject(m, "BenchmarkContext", (PyObject*) &BenchmarkContextType) < 0) {
        Py_DECREF(&BenchmarkContextType);
        Py_DECREF(m);
        return NULL;
    }

    return m;
}
} // extern "C"