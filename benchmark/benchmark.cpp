#include <getopt.h>

#include "detail/benchmark.cuh"

#include <string>
#include <sstream>
#include <stdexcept>

Mode stom(const char* s)
{
    if (std::strcmp(s, "TimeIntegration") == 0)
        return Mode::TimeIntegration;
    else if (std::strcmp(s, "MADClean") == 0)
        return Mode::MADClean;
    else if (std::strcmp(s, "RmBaseline") == 0)
        return Mode::RmBaseline;
    else if (std::strcmp(s, "RmMean") == 0)
        return Mode::RmMean;
    else if (std::strcmp(s, "BruteforceDmt") == 0)
        return Mode::BruteforceDmt;
    else if (std::strcmp(s, "Adaptive1D") == 0)
        return Mode::Adaptive1D;
    else
    {
        std::ostringstream ss;
        ss << "Unknown mode: " << s;
        throw std::runtime_error(ss.str());
    }
}

std::ostream& operator<<(std::ostream& s, const Mode mode)
{
    switch (mode)
    {
    case Mode::TimeIntegration:
        s << "TimeIntegration";
        return s;
    case Mode::MADClean:
        s << "MADClean";
        return s;
    case Mode::RmBaseline:
        s << "RmBaseline";
        return s;
    case Mode::RmMean:
        s << "RmMean";
        return s;
    case Mode::BruteforceDmt:
        s << "BruteforceDmt";
        return s;
    case Mode::Adaptive1D:
        s << "Adaptive1D";
        return s;

    case Mode::InvalidMode:
        s << "InvalidMode";
        return s;
    }

    __builtin_unreachable(); // std::unreachable();
}

struct Config
{
    static constexpr size_t INVALID_SIZE_T = std::numeric_limits<size_t>::max();
    static constexpr float  INVALID_FLOAT  = std::numeric_limits<float>::quiet_NaN();

    Mode mode{ Mode::InvalidMode };

    size_t N_f          { INVALID_SIZE_T },
           N_t_in       { INVALID_SIZE_T },
           N_dm         { INVALID_SIZE_T },
           N_t_out      { INVALID_SIZE_T },
           N            { INVALID_SIZE_T },
           N_candidates { INVALID_SIZE_T };

    float fmin          { INVALID_FLOAT },
          fmax          { INVALID_FLOAT },
          DMmin         { INVALID_FLOAT },
          DMmax         { INVALID_FLOAT };

    size_t n_rep{ 1 };
    int fill{ true };
};

Config parse_args(int argc, char* argv[])
{
    Config cfg;
    while (true)
    {
        static struct option long_options[] = {
            { "mode",         required_argument, 0,        'm' },
            { "N_f",          required_argument, 0,        'k' },
            { "N_t_in",       required_argument, 0,        'i' },
            { "N_dm",         required_argument, 0,        'K' },
            { "N_t_out",      required_argument, 0,        'o' },
            { "N",            required_argument, 0,        'N' },
            { "N_candidates", required_argument, 0,        'c' },
            { "fmin",         required_argument, 0,        'f' },
            { "fmax",         required_argument, 0,        'F' },
            { "DMmin",        required_argument, 0,        'd' },
            { "DMmax",        required_argument, 0,        'D' },
            { "n_rep",        required_argument, 0,        'n' },
            { "fill",         no_argument,       &cfg.fill, 1  },
            { "nofill",       no_argument,       &cfg.fill, 0  },
            { 0, 0, 0, 0 }
        };

        int option_index = 0;

        int c = getopt_long(argc, argv, "n", long_options, &option_index);

        if (c == -1)
            break;

        switch (c)
        {
        case 0:
            break;
        case 'm':
            cfg.mode         = stom(optarg);
            break;
        case 'k':
            cfg.N_f          = std::stoull(optarg);
            break;
        case 'i':
            cfg.N_t_in       = std::stoull(optarg);
            break;
        case 'K':
            cfg.N_dm         = std::stoull(optarg);
            break;
        case 'o':
            cfg.N_t_out      = std::stoull(optarg);
            break;
        case 'N':
            cfg.N            = std::stoull(optarg);
            break;
        case 'c':
            cfg.N_candidates = std::stoull(optarg);
            break;
        case 'f':
            cfg.fmin         = std::stof(optarg);
            break;
        case 'F':
            cfg.fmax         = std::stof(optarg);
            break;
        case 'd':
            cfg.DMmin        = std::stof(optarg);
            break;
        case 'D':
            cfg.DMmax        = std::stof(optarg);
            break;
        case 'n':
            cfg.n_rep        = std::stoull(optarg);
            break;
        case '?':
            exit(EXIT_FAILURE);
        default:
            std::ostringstream ss;
            ss << "Unhandled case: " << static_cast<char>(c);
            throw std::runtime_error(ss.str());
        }
    }

    if (cfg.mode == Mode::InvalidMode)
        throw std::runtime_error("Specifying a mode with --mode is mandatory");


    if (cfg.N_f == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::TimeIntegration:
        case Mode::MADClean:
        case Mode::RmBaseline:
        case Mode::RmMean:
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting N_f";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.N_t_in == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::TimeIntegration:
        case Mode::MADClean:
        case Mode::RmBaseline:
        case Mode::RmMean:
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting N_t_in";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.N_dm == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
        case Mode::Adaptive1D:
            ss << cfg.mode << " requires setting N_dm";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.N_t_out == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
        case Mode::Adaptive1D:
            ss << cfg.mode << " requires setting N_t_out";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.N == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting N";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.N_candidates == Config::INVALID_SIZE_T)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::Adaptive1D:
            ss << cfg.mode << " requires setting N_candidates";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.fmin == Config::INVALID_FLOAT)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting fmin";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.fmax == Config::INVALID_FLOAT)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting fmax";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.DMmin == Config::INVALID_FLOAT)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting DMmin";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }
    
    if (cfg.DMmax == Config::INVALID_FLOAT)
    {
        std::ostringstream ss;
        switch (cfg.mode)
        {
        case Mode::BruteforceDmt:
            ss << cfg.mode << " requires setting DMmax";
            throw std::runtime_error(ss.str());
        default:
            break;
        }
    }


    return cfg;
}

int main(int argc, char* argv[])
{
    const Config cfg = parse_args(argc, argv);

    detail::BenchmarkContext ctx(cfg.n_rep, cfg.fill);

    std::pair<float, float> stats;

    switch (cfg.mode)
    {
    case Mode::TimeIntegration:
        stats = ctx.bench<Mode::TimeIntegration>(cfg.N_f, cfg.N_t_in);
        break;
    case Mode::MADClean:
        stats = ctx.bench<Mode::MADClean>(cfg.N_f, cfg.N_t_in);
        break;
    case Mode::RmBaseline:
        stats = ctx.bench<Mode::RmBaseline>(cfg.N_f, cfg.N_t_in);
        break;
    case Mode::RmMean:
        stats = ctx.bench<Mode::RmMean>(cfg.N_f, cfg.N_t_in);
        break;
    case Mode::BruteforceDmt:
        stats = ctx.bench<Mode::BruteforceDmt>(cfg.N_f, cfg.N_t_in, cfg.N_dm, cfg.N_t_out, cfg.N,
            cfg.fmin, cfg.fmax, cfg.DMmin, cfg.DMmax);
        break;
    case Mode::Adaptive1D:
        stats = ctx.bench<Mode::Adaptive1D>(cfg.N_dm, cfg.N_t_out, cfg.N_candidates);
        break;
    case Mode::InvalidMode:
        throw std::runtime_error("Invalid Mode requested");
    }

    std::cout << cfg.mode << " over " << cfg.n_rep << " iterations: " << stats.first << " +- " << stats.second << " µs" << std::endl;
    return 0;
}