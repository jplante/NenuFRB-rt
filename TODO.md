# TODO

## Rework

### Rename NenuFRB library (liblibNenuFRB.a)

The name is silly, find a way in CMake to make it libNenuFRB.a, or change name completely

(priority: low)

## Testing

### Write quality unit tests

Support for some unit tests have been dropped for a number of commits.
All tests were already of mediocre quality anyways, write new ones.

Goals:
* Automatic testing for regression
* Benchmarking possibilites
* Faster iterations when implementing new algorithms

Current tests often rely on datafiles that are not versionned, which
means that the tests are absolutely not portable.

(priority: low)
