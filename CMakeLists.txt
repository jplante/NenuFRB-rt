cmake_minimum_required(VERSION 3.25)

project(NenuFRB VERSION 0.1 LANGUAGES CXX CUDA)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_STANDARD_REQUIRED ON)
set(CMAKE_CXX_EXTENSIONS OFF)
set(CMAKE_CUDA_STANDARD 17)
set(CMAKE_CUDA_STANDARD_REQUIRED ON)
set(CMAKE_CUDA_FLAGS "${CMAKE_CUDA_FLAGS} -ccbin=${CMAKE_CXX_COMPILER} --expt-relaxed-constexpr") # -Xptxas=-v 
set(CMAKE_CUDA_FLAGS_RELEASE "${CMAKE_CUDA_FLAGS_RELEASE} -lineinfo -use_fast_math -extra-device-vectorization")
set(CMAKE_CUDA_FLAGS_DEBUG "${CMAKE_CUDA_FLAGS_DEBUG} -G")

include(FindCUDAToolkit)
include(FindPkgConfig)
# TODO: find cutlass
set(CUTLASS_INCLUDE_DIR "/home/nenufarobs/cutlass/include")
pkg_search_module(DPDK libdpdk REQUIRED)

add_subdirectory(lib)
add_subdirectory(src)
add_subdirectory(test)
add_subdirectory(benchmark)


install(PROGRAMS ${CMAKE_SOURCE_DIR}/python/process_dump.py TYPE BIN)
install(SCRIPT ${CMAKE_SOURCE_DIR}/scripts/perms.cmake)
