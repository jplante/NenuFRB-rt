#include "Config/ParsetReader.hpp"

#include <fstream>
#include <iostream>
#include <algorithm>
#include <string_view>
#include <sstream>
#include <iomanip>
#include <stdexcept>
#include <regex>


int ParsetReader::read(const std::string& fname)
{
	std::ifstream fd(fname, std::ios::in);
	return read(fd);
}

int ParsetReader::read(std::istream& stream)
{
	std::string buf;

	int nlines = 0;
	while (std::getline(stream, buf))
	{
		if (buf.empty())
			continue;

		// Find the position of the delimiters (format parSet.par=value)
		std::string::const_iterator dot_pos = std::find(buf.cbegin(), buf.cend(), '.');
		std::string::const_iterator eq_pos  = std::find(dot_pos, buf.cend(), '=');

		std::string::const_iterator objEnd = eq_pos - 1;
		std::string::const_iterator valueBegin = eq_pos + 1;

		// Strip potential white spaces around the equal sign
		while (*objEnd == ' ')
			objEnd--;
		objEnd++;

		while (*valueBegin == ' ')
			valueBegin++;


		KeyT objectName(buf.cbegin(), dot_pos),
			fieldName(dot_pos + 1, objEnd);

		std::string value(valueBegin, buf.cend());


		parset[objectName][fieldName] = Value(std::move(value));
		nlines++;
	}

	return nlines;
}


const ParsetReader::ValueT& ParsetReader::get(const std::string& object, const std::string& field) const
{
	if (parset.find(object) != parset.cend())
	{
		const ObjectT& obj = parset.at(object);

		if (obj.find(field) != obj.cend())
		{
			return obj.at(field);
		}
		else
		{
			std::stringstream ss;
			ss << "No such field in " << object << ": " << field;
			throw std::runtime_error(ss.str());
		}
	}
	else
	{
		std::stringstream ss;
		ss << "No such object in parset: " << object;
		throw std::runtime_error(ss.str());
	}
	
}

bool ParsetReader::Value::as_bool() const
{
	static const std::vector<std::string> true_strings({ "ON", "true", "1", "enable" });
	static const std::vector<std::string> false_strings({ "OFF", "false", "0", "disable" });

	bool is_true = false;
	for (const std::string& ts : true_strings)
		is_true |= std::equal(value.cbegin(), value.cend(), ts.cbegin(), ts.cend(),
				[](char a, char b) -> bool { return std::toupper(a) == std::toupper(b); });

	bool is_false = false;
	for (const std::string& fs : false_strings)
		is_false |= std::equal(value.cbegin(), value.cend(), fs.cbegin(), fs.cend(),
				[](char a, char b) -> bool {return std::toupper(a) == std::toupper(b); });

	if (!is_true && !is_false) // Undetermined, bad value
	{
		std::stringstream ss;
		ss << "ParsetReader::Value::as_bool: " << value << " cannot be interpreted as bool";

		throw std::runtime_error(ss.str());
	}

	return is_true && !is_false;
}

int ParsetReader::Value::as_int() const
{
	return std::stoi(value);
}

float ParsetReader::Value::as_float() const
{
	return std::stof(value);
}

std::vector<int> ParsetReader::Value::as_vector() const
{
	std::vector<int> vector;

	std::string::const_iterator v_itr = value.cbegin();
	if (*v_itr != '[')
	{
		std::stringstream ss;
		ss << "ParsetReader::Value::as_vector: character '[' should start a table (found '" << *v_itr << "')";

		throw std::runtime_error(ss.str());
	}
	v_itr++;

	while (v_itr != value.cend())
	{
		const char c = *v_itr;

		if (std::isdigit(c))
		{
			std::string::const_iterator v_begin = v_itr;
			std::string::const_iterator v_end   = std::find(v_begin, value.cend(), ',');
			if (v_end == value.cend())
				v_end = std::find(v_begin, value.cend(), ']');
			
			vector.push_back(std::stoi(std::string(v_begin, v_end)));
			v_itr = v_end;
		}
		else if (c == ']')
			break;
		else
			v_itr++;
	}

	return std::move(vector);
}


std::pair<int, int> ParsetReader::Value::as_range() const
{
	std::pair<int, int> range;
	std::sscanf(value.c_str(), "[%d..%d]", &range.first, &range.second);
	return range;
}

const std::string& ParsetReader::Value::as_string() const
{
	return value;
}

std::tm ParsetReader::Value::as_time() const
{
	std::tm time;
	static const char* format = "%Y-%m-%dT%TZ";
	
	std::istringstream ss(value);
	ss >> std::get_time(&time, format);
	if (ss.fail())
	{
		std::stringstream ss;
		ss << "Failed to parse " << value << " with format " << format;
		throw std::runtime_error(ss.str());
	}

	return std::move(time);
}


int ParsetReader::n_elems(std::string list_name) const
{
	std::stringstream ss;
	ss << list_name << R"(\[\d+\])";
	std::regex regex(ss.str(), std::regex::ECMAScript | std::regex::optimize);

	return std::count_if(parset.cbegin(), parset.cend(), [&regex](const ParSetT::const_iterator::value_type& pair) { 
			return std::regex_match(pair.first.cbegin(), pair.first.cend(), regex); });
}
