#include "RSP_CEP.cuh"

#include <cuda.h>
#include <cuda_runtime.h>
#include <cooperative_groups.h>
#include <cooperative_groups/memcpy_async.h>

#include <rte_ethdev.h>
#include <rte_pci.h>
#include <rte_flow.h>
#include <rte_log.h>
#include <rte_gpudev.h>
#include <rte_malloc.h>

#include <rte_ether.h>
#include <rte_ip.h>
#include <rte_udp.h>


#include "helper_cuda.cuh"

#include <thread>
#include <chrono>

namespace {
RTE_LOG_REGISTER(rsp_cep_init, rsp_cep.init, INFO);
RTE_LOG_REGISTER(rsp_cep_main_loop, rsp_cep.main_loop, INFO);
}

namespace cg = cooperative_groups;

static constexpr size_t GPU_PAGE_SHIFT = 16;
static constexpr size_t GPU_PAGE_SIZE = 1ul << GPU_PAGE_SHIFT;
static constexpr uint16_t BURST_SIZE = 64; // Number of packets received at a time
static constexpr uint16_t RX_READY_THRESH = BURST_SIZE / 2;
static constexpr uint16_t RX_RING_SIZE = 8 * BURST_SIZE;
static constexpr uint16_t MEMPOOL_SIZE = 4 * RX_RING_SIZE;

// Make this value global for convenience, but only in the implementation file so we do not pollute every other file
static constexpr auto BEAMLETS_PER_BHR = RSP_CEP::BEAMLETS_PER_BHR;
static constexpr rte_be32_t BHR_MCAST_0_IP   = RSP_CEP::BHR_MCAST_0_IP;
static constexpr rte_be16_t BHR_MCAST_0_PORT = RSP_CEP::BHR_MCAST_0_PORT;

static constexpr rte_be32_t BHR_MCAST_1_IP   = RSP_CEP::BHR_MCAST_1_IP;
static constexpr rte_be16_t BHR_MCAST_1_PORT = RSP_CEP::BHR_MCAST_1_PORT;

static constexpr rte_be32_t BHR_MCAST_2_IP   = RSP_CEP::BHR_MCAST_2_IP;
static constexpr rte_be16_t BHR_MCAST_2_PORT = RSP_CEP::BHR_MCAST_2_PORT;

static constexpr rte_be32_t BHR_MCAST_3_IP   = RSP_CEP::BHR_MCAST_3_IP;
static constexpr rte_be16_t BHR_MCAST_3_PORT = RSP_CEP::BHR_MCAST_3_PORT;

template <uint8_t BM, typename TyGroup,
	typename OutputEngine, typename OutputLayout,
	typename HeaderEngine, typename HeaderLayout>
__device__ __forceinline__ void copy_payload(const TyGroup& group,
		const uintptr_t buf_addr,
		const struct header_t* __restrict__ h,
		cute::Tensor<OutputEngine, OutputLayout>& data_ring,
		cute::Tensor<HeaderEngine, HeaderLayout>& header_ring,
		const uint32_t offset,
		const uint16_t payload_bytes,
		const uint16_t base_beamlet,
		const uint16_t nof_beamlet,
		const uint8_t nof_blocks)
{
	using TypeOut = typename OutputEngine::value_type; // Must be a complex
	constexpr uint16_t ETHER_IPV4_UDP_HDR_LEN = sizeof(struct rte_ether_hdr) + sizeof(struct rte_ipv4_hdr) + sizeof(struct rte_udp_hdr);

	const beamlet_t<BM>* beamlet = reinterpret_cast<const beamlet_t<BM>*>(buf_addr + ETHER_IPV4_UDP_HDR_LEN + sizeof(struct header_t));
	for (uint16_t k = group.thread_rank(); k < payload_bytes / sizeof(beamlet_t<BM>); k += group.num_threads())
	{
		const uint16_t beamlet_idx = k % nof_beamlet; // Idx in local BHR
		const uint16_t elem_idx    = k / nof_beamlet;

		const uint16_t full_beamlet_idx = base_beamlet * BEAMLETS_PER_BHR + beamlet_idx; // Idx over all BHRs

		if(full_beamlet_idx >= RSP_CEP::N_F)
		{
			printf("\nbad beamlet_idx: %u %u %u\n", base_beamlet, beamlet_idx, full_beamlet_idx);
		}
		else
		{
			const beamlet_t<BM> beamlet_elem = beamlet[k];
			data_ring(cute::make_coord(full_beamlet_idx, offset + elem_idx)) = 
				TypeOut(beamlet_elem.X.re, beamlet_elem.X.im)
				+ TypeOut(beamlet_elem.Y.re, beamlet_elem.Y.im);
		}
	}

	for (uint8_t k = 0; k < nof_blocks; k++)
		cg::memcpy_async(group, &header_ring(base_beamlet, offset + k), h, sizeof(struct header_t));
	cg::wait(group);
}


template <typename OutputEngine, typename OutputLayout,
	typename HeaderEngine, typename HeaderLayout>
__launch_bounds__(RSP_CEP::NUM_THREADS, RSP_CEP::NUM_BLOCKS)
__global__ void packet_processing_persistent_kernel(struct rte_gpu_comm_list* __restrict__ comm_list,
	enum rte_gpu_comm_list_status* __restrict__ packet_processing_status,
	cute::Tensor<OutputEngine, OutputLayout> data_ring,
	cute::Tensor<HeaderEngine, HeaderLayout> header_ring,
	uint64_t* latest_timestamp,
	volatile uint32_t* quit_flag)
{
	using TypeOut = typename OutputEngine::value_type;

	constexpr uint16_t ETHER_IPV4_UDP_HDR_LEN = sizeof(struct rte_ether_hdr) + sizeof(struct rte_ipv4_hdr) + sizeof(struct rte_udp_hdr);

	__shared__ struct rte_gpu_comm_list comm_list_elem;
	__shared__ char full_hdr[sizeof(struct rte_ipv4_hdr) + 2 + sizeof(struct rte_udp_hdr) + sizeof(struct header_t)];
	struct rte_ipv4_hdr* ipv4_hdr = reinterpret_cast<struct rte_ipv4_hdr*>(full_hdr + 1);
	struct rte_udp_hdr* udp_hdr = reinterpret_cast<struct rte_udp_hdr*>(full_hdr + 1 + sizeof(struct rte_ipv4_hdr) + 1);
	struct header_t* h = reinterpret_cast<struct header_t*>(full_hdr + 1 + sizeof(struct rte_ipv4_hdr) + 1 + sizeof(struct rte_udp_hdr));

	cg::grid_group   grid  = cg::this_grid();
	cg::thread_block block = cg::this_thread_block();
	cg::thread_block_tile<32, cg::thread_block> warp = cg::tiled_partition<32>(block);


	// WARNING: this is based under the assumption that the status_d vector is contiguous (true for DPDK gpudev v22.03)
	volatile enum rte_gpu_comm_list_status* const status_d = comm_list->status_d;

	int i = grid.block_rank() % MEMPOOL_SIZE;
	while (true)
	{
		while (!*quit_flag && (status_d[i] != RTE_GPU_COMM_LIST_READY))
		{
			// busy wait
		}
		
		cg::memcpy_async(block, &comm_list_elem, comm_list + i, sizeof(struct rte_gpu_comm_list));
		cg::wait(block);

		if (*quit_flag)
		{
			return;
		}
		else
		{
			if (block.thread_rank() == 0)
			{
				status_d[i] = RTE_GPU_COMM_LIST_DONE;
			}

			uint64_t max_timestamp = 0u;
			for (int j = 0; j < comm_list_elem.num_pkts; j++)
			{
				const struct rte_gpu_comm_pkt pkt = comm_list_elem.pkt_list[j];
			
				cg::memcpy_async(block, reinterpret_cast<void*>(ipv4_hdr),
						reinterpret_cast<void*>(pkt.addr + sizeof(struct rte_ether_hdr)),
						sizeof(struct rte_ipv4_hdr));
				cg::memcpy_async(block, reinterpret_cast<void*>(udp_hdr),
						reinterpret_cast<void*>(pkt.addr + sizeof(struct rte_ether_hdr) + sizeof(struct rte_ipv4_hdr)),
						sizeof(struct rte_udp_hdr) + sizeof(struct header_t));
				cg::wait(block);

				const uint16_t payload_bytes = pkt.size - ETHER_IPV4_UDP_HDR_LEN - sizeof(struct header_t);

				const uint16_t nof_beamlet = (uint16_t(h->NOF_BEAMLET_HI) << 8) + uint16_t(h->NOF_BEAMLET_LO);
				const uint8_t  nof_blocks  = h->NOF_BLOCKS;

				const uint16_t base_beamlet = ((ipv4_hdr->dst_addr == BHR_MCAST_0_IP) && (udp_hdr->dst_port == BHR_MCAST_0_PORT)) ? uint16_t(0)
				                            : ((ipv4_hdr->dst_addr == BHR_MCAST_1_IP) && (udp_hdr->dst_port == BHR_MCAST_1_PORT)) ? uint16_t(1)
				                            : ((ipv4_hdr->dst_addr == BHR_MCAST_2_IP) && (udp_hdr->dst_port == BHR_MCAST_2_PORT)) ? uint16_t(2)
				                            : ((ipv4_hdr->dst_addr == BHR_MCAST_3_IP) && (udp_hdr->dst_port == BHR_MCAST_3_PORT)) ? uint16_t(3)
				                            : uint16_t(-1);

				if (base_beamlet == uint16_t(-1))
				{
					printf("Unexpected packet received, ignoring\n");
				}
				else
				{
					const uint64_t timestamp = full_bsn<uint64_t>(h);
					const uint32_t offset    = static_cast<uint32_t>(timestamp); // Overflow in an unsigned to avoid problems with signed indices
					// if ((threadIdx.x == 0) && (base_beamlet == 0))
					// 	printf("%u,%u,%lu,%u\n", h->TIMESTAMP, h->BLOCK_SEQUENCE_NUMBER, timestamp, offset);


					assert((h->BM == 0) || (h->BM == 1) || (h->BM == 2));

					// No switch case to enable inlining (not sure if possible else because of the different allocation sizes per scope)
					if (h->BM == 0)
					{
						copy_payload<0>(block, pkt.addr, h, data_ring, header_ring, offset, payload_bytes, base_beamlet, nof_beamlet, nof_blocks);
					}
					else if (h->BM == 1)
					{
						copy_payload<1>(block, pkt.addr, h, data_ring, header_ring, offset, payload_bytes, base_beamlet, nof_beamlet, nof_blocks);
					}
					else if (h->BM == 2)
					{
						copy_payload<2>(block, pkt.addr, h, data_ring, header_ring, offset, payload_bytes, base_beamlet, nof_beamlet, nof_blocks);
					}

					max_timestamp = max(timestamp, max_timestamp);
				}
			}

			

			if (block.thread_rank() == 0)
			{
				packet_processing_status[i] = RTE_GPU_COMM_LIST_DONE;
			}

			if (block.thread_rank() == 32)
			{
				*latest_timestamp = max_timestamp;
			}


			i = (i + grid.num_blocks()) % MEMPOOL_SIZE;
		}
		
	}
}


int RSP_CEP::poll_nic(void* args)
{
	RSP_CEP* rsp_cep = reinterpret_cast<struct RSP_CEP*>(args);
	checkCudaErrors(cudaSetDevice(rsp_cep->cuda_gpu_id));

	const uint16_t port = rsp_cep->port;
	const uint16_t rxq = 0;
	volatile uint32_t* running = &rsp_cep->running;
	struct rte_gpu_comm_list* comm_list = rsp_cep->comm_list;
	volatile enum rte_gpu_comm_list_status* packet_processing_status = rsp_cep->packet_processing_status;
	auto data_ring = rsp_cep->data_ring();

	int prod_head = 0;
	int cons_head = 0;

	while (*running)
	{
		struct rte_mbuf* bufs[BURST_SIZE];

		if (RTE_GPU_VOLATILE(*comm_list[prod_head].status_h) == RTE_GPU_COMM_LIST_FREE)
		{
			uint16_t nb_rx = rte_eth_rx_burst(port, rxq, bufs, BURST_SIZE);

			if (nb_rx > 0)
			{
				rte_log(RTE_LOG_DEBUG, rsp_cep_main_loop, "RSP_CEP.MAIN_LOOP: Received %u packets\r", nb_rx);

				// Store newly received mbufs
				rte_gpu_comm_populate_list_pkts(comm_list + prod_head, bufs, nb_rx);
				prod_head = (prod_head + 1) % MEMPOOL_SIZE;
			}
			else
				std::this_thread::sleep_for(std::chrono::microseconds(1));
		}
		else
			std::this_thread::sleep_for(std::chrono::microseconds(1));


		while (packet_processing_status[cons_head] == RTE_GPU_COMM_LIST_DONE)
		{
			packet_processing_status[cons_head] = RTE_GPU_COMM_LIST_READY;
			struct rte_gpu_comm_list* comm_list_elem = comm_list + cons_head;

			rte_log(RTE_LOG_DEBUG, rsp_cep_main_loop, "RSP_CEP.MAIN_LOOP: Processed %u packets\n", comm_list_elem->num_pkts);
			rte_pktmbuf_free_bulk(comm_list_elem->mbufs, comm_list_elem->num_pkts);
			rte_gpu_comm_cleanup_list(comm_list_elem);
			cons_head = (cons_head + 1) % MEMPOOL_SIZE;
		}

	}

	return 0;
}


RSP_CEP::RSP_CEP()
{
	checkCudaErrors(cudaMalloc(&data_ring_storage,
		cute::size(full_data_ring()) * sizeof(value_type)));

	checkCudaErrors(cudaMalloc(&header_ring_storage,
		cute::size(full_header_ring()) * sizeof(struct header_t)));


	int greatestPriority;
	checkCudaErrors(cudaDeviceGetStreamPriorityRange(NULL, &greatestPriority));
	checkCudaErrors(cudaStreamCreateWithPriority(&persistent_stream, cudaStreamNonBlocking, greatestPriority));
}


int RSP_CEP::init(const struct rte_pci_addr* nic_pci_addr, const struct rte_pci_addr* gpu_pci_addr)
{
	int ret;

	// Get DPDK port from PCI addr
	char nic_pci_addr_str[13];
	rte_pci_device_name(nic_pci_addr, nic_pci_addr_str, 13);
	ret = rte_eth_dev_get_port_by_name(nic_pci_addr_str, &port);
	if (ret < 0)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: Error while selecting port: %s\n", strerror(-ret));
		return ret;
	}
	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Found port %s as port #%" PRIu16 "\n", nic_pci_addr_str, port);

	// Recover CUDA device id from PCI id
	char gpu_pci_addr_str[13];
	rte_pci_device_name(gpu_pci_addr, gpu_pci_addr_str, 13);
	checkCudaErrors(cudaDeviceGetByPCIBusId(&cuda_gpu_id, gpu_pci_addr_str));
	

	// Find the requested GPU in DPDK
	int16_t dev_id = 0;
	RTE_GPU_FOREACH(dev_id)
	{
		struct rte_gpu_info info;
		rte_gpu_info_get(dev_id, &info);

		struct rte_pci_addr gpu_addr; // scanned GPU PCI addr
		rte_pci_addr_parse(info.name, &gpu_addr);
		if (rte_pci_addr_cmp(&gpu_addr, gpu_pci_addr) == 0)
		{
			dpdk_gpu_id = dev_id;
			break;
		}
	}

	return init(port, dpdk_gpu_id);
}

int RSP_CEP::init(const int16_t nic_dpdk_id, const int16_t gpu_dpdk_id)
{
	int ret;

	port = nic_dpdk_id;
	dpdk_gpu_id = gpu_dpdk_id;


	struct rte_gpu_info gpu_info;
	rte_gpu_info_get(dpdk_gpu_id, &gpu_info);
	checkCudaErrors(cudaDeviceGetByPCIBusId(&cuda_gpu_id, gpu_info.name));

	checkCudaErrors(cudaSetDevice(cuda_gpu_id));
	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Acquisition will launch on GPU %d\n", cuda_gpu_id);


	struct rte_eth_dev_info dev_info; // Fetch device info
	ret = rte_eth_dev_info_get(port, &dev_info);
	if (ret < 0) {
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP_INIT: Error while getting device (port %u) info: %s\n",
			port, strerror(-ret));
		return ret;
	}

	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Preferred burst_size: %u (using %u)\n", dev_info.default_rxportconf.burst_size, BURST_SIZE);
	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Preferred ring_size:  %u (using %u)\n", dev_info.default_rxportconf.ring_size,  RX_RING_SIZE);
	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Preferred nb_queues:  %u (using %u)\n", dev_info.default_rxportconf.nb_queues,  1);

/// Allocate GPU mempool
	// Align elements to 32 bytes to enable vectorized memory loads/stores
	extmem = reinterpret_cast<struct rte_pktmbuf_extmem*>(malloc(sizeof(struct rte_pktmbuf_extmem)));
	extmem->elt_size = RTE_ALIGN_CEIL(MTU + RTE_PKTMBUF_HEADROOM
		+ RTE_ETHER_HDR_LEN + RTE_ETHER_CRC_LEN, 32);

	extmem->buf_len = RTE_ALIGN_CEIL(MEMPOOL_SIZE * extmem->elt_size, GPU_PAGE_SIZE);
	extmem->buf_iova = RTE_BAD_IOVA;

	extmem->buf_ptr = rte_gpu_mem_alloc(dpdk_gpu_id, extmem->buf_len, 0);

	if (!extmem->buf_ptr)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: Failed to allocate gpu mempool of %d bytes: %s\n",
			extmem->elt_size, strerror(rte_errno));
		return -ENOMEM;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init,
			"RSP_CEP.INIT: Allocated %lu bytes of GPU memory from %p\n", extmem->buf_len, extmem->buf_ptr);

	// Explain to DPDK the kind of memory we will as external memory
	ret = rte_extmem_register(extmem->buf_ptr, extmem->buf_len, NULL, extmem->buf_iova, GPU_PAGE_SIZE);
	if (ret)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: Could not register GPU memory: %s\n", strerror(-ret));
		return ret;
	}
	
	// Do the DMA mapping.
	ret = rte_dev_dma_map(dev_info.device, extmem->buf_ptr, extmem->buf_iova, extmem->buf_len);
	if (ret < 0)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: Could not DMA map EXT memory: %s\n", strerror(-ret));
		return ret;
	}

	// Create the mempool itself
	mpool = rte_pktmbuf_pool_create_extbuf("gpu_mpool", MEMPOOL_SIZE,
		0, 0, extmem->elt_size, 
		rte_socket_id(), extmem, 1);



// Port configuration

	struct rte_flow_error error;
	ret = rte_flow_isolate(port, 1, &error); // This changes the default behaviour of the application to "let everything pass through"
	if (ret < 0)                             // (instead of "capture every single packet !!")
	{                                        // I use this so the machine can still answer pings / ARP requests etc while this program runs, without having to implement everything myself
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: rte_flow_isolate failed: %s", error.message);
		return ret;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Successfully isolated flow\n");



	uint16_t nb_rxd = RX_RING_SIZE; // Number of reception (RX) descriptors that we want, aka the length of each rx queue (a FIFO for ingress traffic on the NIC) (the number is in packets, not bytes)
	uint16_t nb_txd = 0;


	  /* Now on to the Ethernet Device setup
	 * The Ethernet Device is the DPDK name for a NIC that uses Ethernet.
	 * We have to configure its RX queues, Offloads (= hardware-accelerated functions, e.g. CRC computation, Encryption, etc), and others
	 */
	uint16_t rx_rings = 1; // We only use one rx_ring (= one rx queue = one FIFO of RX_RING_SIZE) here, since we only try to receive one type of packet so this is enough. Multiple rings can probably be used to increase bandwidth, but more especially to process different kinds of packets (one ring for UDP, one ring for ARP, etc). Every ring has its own set of mempools
	int16_t tx_rings = 0;
	struct rte_eth_conf port_conf;
	memset(&port_conf, 0, sizeof(struct rte_eth_conf));
	port_conf.rxmode.mtu = MTU;

	ret = rte_eth_dev_configure(port, rx_rings, tx_rings, &port_conf); // Upload the configuration
	if (ret < 0)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init,
			"RSP_CEP.INIT: Cannot configure device: err=%s, port=%u\n", strerror(-ret),
			port);
		return ret;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Port successfully configured\n");

	// Make sure that the queues can be allocated
	ret = rte_eth_dev_adjust_nb_rx_tx_desc(port, &nb_rxd, &nb_txd);

	if (ret < 0)
	{
	    rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: Could not adjust device rx and tx queues\n");
		return ret;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init,
		"RSP_CEP.INIT: Successfully adjusted rx and tx descriptors, rxd=%d, txd=%d\n", nb_rxd, nb_rxd);


	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//// RX QUEUES
	///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	struct rte_eth_rxconf rxconf = dev_info.default_rxconf;
	rxconf.rx_free_thresh = 0;
	// rxconf.rx_deferred_start = 1; // Not supported
	rxconf.offloads = port_conf.rxmode.offloads;
	rxconf.rx_nseg = 0;
	rxconf.rx_seg = NULL;
	ret = rte_eth_rx_queue_setup(port, 0, nb_rxd, rte_socket_id(), &rxconf, mpool); // Setup the queue with the fresh configuration

	if (ret < 0)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: rte_eth_rx_queue_setup: err=%s, port=%u\n", strerror(-ret), port);
		return ret;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Successfully setup RX queue with packet splitting\n");
	
	ret = rte_eth_dev_start(port); // Actually start the port ! 
	if (ret < 0)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP.INIT: rte_eth_dev_start:err=%s, port=%u\n", strerror(-ret), port);
		return ret;
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Port started\n");


/// FLOW MANAGEMENT: Let the kernel handle any other packet that transit
/// (following to a bug by catching ARP packets and not responding, impeding communication)
/// This has to be done after the port start, to make sure everything is supported by the NIC

	struct rte_flow_attr attr = {
		.group = 0,
		.priority = 0,
		.ingress = 1,
		.egress = 0,
		.transfer = 0,
	};


	struct rte_flow_item pattern[4]; // We have to setup 4 elements (= filters) for the flow: ETHER -> IPv4 -> UDP -> END
	memset(pattern, 0, 4 * sizeof(struct rte_flow_item)); // Default everything to 0 / NULL
	pattern[0].type = RTE_FLOW_ITEM_TYPE_ETH;
	pattern[1].type = RTE_FLOW_ITEM_TYPE_IPV4;
	pattern[2].type = RTE_FLOW_ITEM_TYPE_UDP;
	pattern[3].type = RTE_FLOW_ITEM_TYPE_END;

	struct rte_flow_item_ipv4 ipv4_spec = {0};
	ipv4_spec.hdr.dst_addr = BHR_MCAST_0_IP;

	struct rte_flow_item_ipv4 ipv4_mask = {0};
	ipv4_mask.hdr.dst_addr = RTE_STATIC_BSWAP32(RTE_IPV4(255, 255, 255, 0));

	pattern[1].spec = &ipv4_spec;
	pattern[1].mask = &ipv4_mask;


	// If a packet matches the previous pattern, apply the following actions: QUEUE it in rx_ring 0, and END.
	struct rte_flow_action actions[2];
	struct rte_flow_action_queue queue_conf = {
		.index = 0
	};
	actions[0].type = RTE_FLOW_ACTION_TYPE_QUEUE;
	actions[0].conf = &queue_conf;

	actions[1].type = RTE_FLOW_ACTION_TYPE_END;
	

	ret = rte_flow_validate(port, &attr, pattern, actions, &error); // Make sure the described flow is ok
	if (ret < 0)
	{
		rte_eth_dev_stop(port);
		rte_eth_dev_close(port);
		rte_panic("Could not validate flow: %s\n", error.message);
	}
	else
		rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP.INIT: Udp only flow has been validated\n");

	udp_only_flow = rte_flow_create(port, &attr, pattern, actions, &error); // Set it up


/// GPUDEV

	comm_list = rte_gpu_comm_create_list(gpu_dpdk_id, MEMPOOL_SIZE + 3); // Last element is used only for the quit_flag
	packet_processing_status = (enum rte_gpu_comm_list_status*) rte_zmalloc("rte_gpu_comm_list_status", MEMPOOL_SIZE * sizeof(enum rte_gpu_comm_list_status), 0);
	rte_gpu_mem_register(gpu_dpdk_id, MEMPOOL_SIZE * sizeof(enum rte_gpu_comm_list_status), packet_processing_status);
	latest_timestamp_d = (uint64_t*) (comm_list[MEMPOOL_SIZE].status_d);
	latest_timestamp_h = (uint64_t*) (comm_list[MEMPOOL_SIZE].status_h);
	h_quit_flag = (uint32_t*) comm_list[MEMPOOL_SIZE + 2].status_h;
	d_quit_flag = (uint32_t*) comm_list[MEMPOOL_SIZE + 2].status_d;

	return 0;
}

int RSP_CEP::start(const unsigned lcore_id)
{
	checkCudaErrors(cudaSetDevice(cuda_gpu_id));

	if (running)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP: Reception already running\n");
		return EINPROGRESS;
	}

	*h_quit_flag = 0u;
	running = true;
	rte_gpu_wmb(dpdk_gpu_id);

	lcore = lcore_id;
	rte_eal_remote_launch(poll_nic, reinterpret_cast<void*>(this), lcore_id);

	rte_log(RTE_LOG_DEBUG, rsp_cep_init, "RSP_CEP: Launching persistent kernel on GPU %d\n", cuda_gpu_id);
	packet_processing_persistent_kernel<<<NUM_BLOCKS, NUM_THREADS, 0, persistent_stream>>>(
		comm_list,
		packet_processing_status,
		full_data_ring(),
		full_header_ring(),
		latest_timestamp_d,
		d_quit_flag);

	// rte_eth_dev_rx_queue_start(port, 0); // to use with rx_deferred_start
	// UPDATE: deferred_start not supported for mlx5 PMD up to DPDK 23:
	// https://forums.developer.nvidia.com/t/dpdk-mlx5-pmd-rx-queue-deferred-start/
	// Workaround for deferred_start
	std::this_thread::sleep_for(std::chrono::milliseconds(10)); // Wait a little bit for everything to be ready
	rte_eth_stats_reset(port);

	return 0;
}

int RSP_CEP::stop()
{
	checkCudaErrors(cudaSetDevice(cuda_gpu_id));

	if (!running)
	{
		rte_log(RTE_LOG_ERR, rsp_cep_init, "RSP_CEP: Reception not running\n");
		return EPERM;
	}

	// rte_eth_dev_rx_queue_stop(port, 0); // to use with rx_deferred_start

	*h_quit_flag = 1u;
	running = false;
	rte_gpu_wmb(dpdk_gpu_id);

	rte_eal_wait_lcore(lcore);
	checkCudaErrors(cudaStreamSynchronize(persistent_stream));

	return 0;
}

RSP_CEP::~RSP_CEP()
{
	checkCudaErrors(cudaSetDevice(cuda_gpu_id));
	checkCudaErrors(cudaStreamDestroy(persistent_stream));

	struct rte_flow_error error;
	rte_flow_destroy(dpdk_gpu_id, udp_only_flow, &error);
	rte_gpu_mem_free(dpdk_gpu_id, extmem->buf_ptr);
	rte_gpu_comm_destroy_list(comm_list, MEMPOOL_SIZE+3);
	rte_gpu_mem_unregister(dpdk_gpu_id, packet_processing_status);
	rte_free(packet_processing_status);

	free(extmem);
	rte_eth_dev_stop(port);
	rte_eth_dev_close(port);

	checkCudaErrors(cudaFree(header_ring_storage));
	checkCudaErrors(cudaFree(data_ring_storage));
}

