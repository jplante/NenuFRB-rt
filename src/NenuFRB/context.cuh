#pragma once

#include <cstdint>
#include <vector>
#include <filesystem>
#include <memory>
#include <fstream>
#include <atomic>
#include <cuda_runtime.h> // cudaStream_t
#include <cute/tensor.hpp>
#include <utils/ring_buffer_stride.hpp>
#include <Detection/Adaptive1D.cuh> // Record<T>
#include <Config/ParsetReader.hpp>
#include <Dedispersion/dmt.hpp>
#include <Acquisition/RSP_CEP.cuh> // TODO: do the opposite: use context to choose RSP_CEP params

#include <dumpers.cuh>

namespace nenufrb
{

struct StatsTightLoop
{
    float t_time_integration_ms;
};

struct StatsMediumLoop
{
    float t_mad_clean_10_ms;
    float t_rm_baseline_0_ms;
    float t_mad_clean_3_ms;
    float t_rm_baseline_1_ms;
    float t_rm_mean_ms;
};

struct StatsLooseLoop
{
    float t_dedispersion_ms;
    float t_detection_ms;
};

template <unsigned X>
struct StaticCeilLog10
{
    static constexpr int value = 1 + StaticCeilLog10<X / 10u>::value;
};
 
template <>
struct StaticCeilLog10<0>
{
    static constexpr int value = 0;
};


class Context
{
public:
    using T = float;                                       // Type used for computations

    static constexpr size_t FFTLEN = 1;                    // Size of the StFFT used for resampling. FFTLEN > 1 is not supported right now with TIME_INTEGRATION > 1
    static constexpr size_t TIME_INTEGRATION = 1 << 10;    // Time integration factor. Raw data is averaged over chunks of this value
    static constexpr size_t FOLD_FACTOR =                  // Total fold factor
        FFTLEN * TIME_INTEGRATION;

    static constexpr size_t REDUNDANCY = 2;                // Ring buffers are allocated with their size times REDUNDANCY
    // static constexpr size_t N_T_RAW = 1 << 19;          // Number of time samples in the raw signal ring buffer
    static constexpr size_t N_T_RAW = RSP_CEP::RING_WIDTH;
    static constexpr size_t N_T_TIGHT_LOOP = N_T_RAW / FOLD_FACTOR;
    static constexpr size_t N_T_MEDIUM_LOOP = 1 << 11;     // Number of time samples to compute preprocessing
    static constexpr size_t N_T_LOOSE_LOOP = 4 * N_T_MEDIUM_LOOP;     // Number of time samples to compute preprocessing
    static constexpr size_t N_T_OUT = 1 << 17;             // Number of time samples in the dedispersion output
    static constexpr size_t N_DM = 4096;                   // Number of DM trials
    static constexpr size_t N_CANDIDATES = N_DM;           // Maximum number of output FRB candidates per detection

    static constexpr float DT_RAW = 1024.0f / 200e6f;      // Time resolution of the raw input signal
    static constexpr float DT     = DT_RAW * FOLD_FACTOR;  // Time resolution after time integration
    static constexpr float DF_RAW = 200e6f / 1024.0f;      // Frequency resolution of the raw input signal
    static constexpr float DF     = DF_RAW / FFTLEN;       // Frequency resolution after time integration

    Context(int argc, char* argv[]);

	bool dump_raw{false};
    bool dump_tmp_I{false};
    bool dump_I{false};
    bool dump_A{false};
    bool dump_candidates{true};
    bool dump_timestamps{true};

    float DMmin = 0;
    float DMmax = 100;


    unsigned nb_lcores;
    int16_t port;
    int16_t dpdk_gpu_id;
    unsigned poll_lcore;
    unsigned med_loop_lcore;
    unsigned loose_loop_lcore;
    int cuda_gpu_id;
 
	std::unique_ptr<ParsetReader> pr;
    std::filesystem::path parset_file;
    std::filesystem::path out_dir;
	int n_beams;
	std::vector<std::pair<int, int>> subbands;

	std::vector<cudaStream_t> tight_loop_streams;
	std::vector<cudaStream_t> med_loop_streams;
	std::vector<cudaStream_t> loose_loop_streams;
	std::vector<cudaStream_t> dump_streams;

    struct Events
    {
        cudaEvent_t ReductionStart,
                    ReductionEnd,
                    ComputeStart,
                    ComputeEnd,
                    MAD10Start,
                    MAD10End,
                    RmBaseline0Start,
                    RmBaseline0End,
                    MAD3Start,
                    MAD3End,
                    RmBaseline1Start,
                    RmBaseline1End,
                    RmMeanStart,
                    RmMeanEnd,
                    DmtStart,
                    DmtEnd,
                    DetectionStart,
                    DetectionEnd;
        
        Events()
        {
            checkCudaErrors(cudaEventCreate(&ReductionStart));
            checkCudaErrors(cudaEventCreate(&ReductionEnd));
            checkCudaErrors(cudaEventCreate(&ComputeStart));
            checkCudaErrors(cudaEventCreate(&ComputeEnd));
            checkCudaErrors(cudaEventCreate(&MAD10Start));
            checkCudaErrors(cudaEventCreate(&MAD10End));
            checkCudaErrors(cudaEventCreate(&RmBaseline0Start));
            checkCudaErrors(cudaEventCreate(&RmBaseline0End));
            checkCudaErrors(cudaEventCreate(&MAD3Start));
            checkCudaErrors(cudaEventCreate(&MAD3End));
            checkCudaErrors(cudaEventCreate(&RmBaseline1Start));
            checkCudaErrors(cudaEventCreate(&RmBaseline1End));
            checkCudaErrors(cudaEventCreate(&RmMeanStart));
            checkCudaErrors(cudaEventCreate(&RmMeanEnd));
            checkCudaErrors(cudaEventCreate(&DmtStart));
            checkCudaErrors(cudaEventCreate(&DmtEnd));
            checkCudaErrors(cudaEventCreate(&DetectionStart));
            checkCudaErrors(cudaEventCreate(&DetectionEnd));
        }

        ~Events()
        {
            checkCudaErrors(cudaEventDestroy(ReductionStart));
            checkCudaErrors(cudaEventDestroy(ReductionEnd));
            checkCudaErrors(cudaEventDestroy(ComputeStart));
            checkCudaErrors(cudaEventDestroy(ComputeEnd));
            checkCudaErrors(cudaEventDestroy(MAD10Start));
            checkCudaErrors(cudaEventDestroy(MAD10End));
            checkCudaErrors(cudaEventDestroy(RmBaseline0Start));
            checkCudaErrors(cudaEventDestroy(RmBaseline0End));
            checkCudaErrors(cudaEventDestroy(MAD3Start));
            checkCudaErrors(cudaEventDestroy(MAD3End));
            checkCudaErrors(cudaEventDestroy(RmBaseline1Start));
            checkCudaErrors(cudaEventDestroy(RmBaseline1End));
            checkCudaErrors(cudaEventDestroy(RmMeanStart));
            checkCudaErrors(cudaEventDestroy(RmMeanEnd));
            checkCudaErrors(cudaEventDestroy(DmtStart));
            checkCudaErrors(cudaEventDestroy(DmtEnd));
            checkCudaErrors(cudaEventDestroy(DetectionStart));
            checkCudaErrors(cudaEventDestroy(DetectionEnd));
        }
    };

    std::vector<Events> events;


    using IShape = cute::Shape<int, cute::constant<size_t, N_T_LOOSE_LOOP>>;
    using IStride = cute::Stride<
        cute::constant<size_t, REDUNDANCY * N_T_LOOSE_LOOP>,
        cute::RingBufferStride<1, REDUNDANCY * N_T_LOOSE_LOOP, uint32_t>
    >;
    using ITensor = cute::Tensor<
        cute::ViewEngine<cute::gmem_ptr<T>>,
        cute::Layout<IShape, IStride>
    >;
    std::vector<ITensor> I;
    
    using ITmpShape = cute::Shape<int, cute::constant<size_t, N_T_TIGHT_LOOP>>;
    using ITmpStride = IStride;
    using ITmpTensor = cute::Tensor<
        cute::ViewEngine<cute::gmem_ptr<T>>,
        cute::Layout<ITmpShape, ITmpStride>
    >;

    using IMedLoopShape = cute::Shape<int, cute::constant<size_t, N_T_MEDIUM_LOOP>>;
    using IMedLoopStride = IStride;
    using IMedLoopTensor = cute::Tensor<
        cute::ViewEngine<cute::gmem_ptr<T>>,
        cute::Layout<IMedLoopShape, IMedLoopStride>
    >;

    using AShape = cute::Shape<cute::constant<size_t, N_DM>, cute::constant<size_t, N_T_OUT>>;
    using AStride = cute::Stride<
        cute::constant<size_t, N_T_OUT>,
        cute::RingBufferStride<1, N_T_OUT, uint32_t>
    >;
    using ATensor = cute::Tensor<
        cute::ViewEngine<cute::gmem_ptr<T>>,
        cute::Layout<AShape, AStride>
    >;
    std::vector<ATensor> A;

    using CandidatesShape = cute::Shape<cute::constant<size_t, N_CANDIDATES>>;
    using CandidatesTensor = cute::Tensor<
        cute::ViewEngine<cute::gmem_ptr<Record<T>>>,
        cute::Layout<
            CandidatesShape,
            cute::GenRowMajor::Apply<CandidatesShape>
        >
    >;
    std::vector<CandidatesTensor> candidates;
    std::vector<CandidatesTensor> candidates_dptr;


    std::vector<void*> mad_ws, rmb_ws, mean_ws, detection_ws;
    std::vector<struct Dmt::Config> dmt_cfgs;

    ~Context();

    using IDumpTensor = cute::Tensor<
        typename ITensor::engine_type,
        cute::Layout<
            IShape,
            cute::GenRowMajor::Apply<IShape>
        >
    >;
    using ITmpDumpTensor = cute::Tensor<
        typename ITmpTensor::engine_type,
        cute::Layout<
            ITmpShape,
            cute::GenRowMajor::Apply<ITmpShape>
        >
    >;
    using IMedLoopDumpTensor = cute::Tensor<
        typename IMedLoopTensor::engine_type,
        cute::Layout<
            IMedLoopShape,
            cute::GenRowMajor::Apply<IMedLoopShape>
        >
    >;
    using ADumpTensorShape = cute::Shape<cute::constant<size_t, N_DM>, cute::constant<size_t, N_T_LOOSE_LOOP>>;
    using ADumpTensor = cute::Tensor<
        typename ATensor::engine_type,
        cute::Layout<
            ADumpTensorShape,
            cute::GenRowMajor::Apply<ADumpTensorShape>
        >
    >;
    using CandidatesDumpTensor = cute::Tensor<
        typename CandidatesTensor::engine_type,
        cute::Layout<
            CandidatesShape,
            cute::GenRowMajor::Apply<CandidatesShape>
        >
    >;

    using IDumpersT = RawDeviceDump<typename ITensor::engine_type,
        typename ITensor::layout_type,
        typename IDumpTensor::engine_type,
        typename IDumpTensor::layout_type>;

    using IMedLoopDumpersT = RawDeviceDump<typename IMedLoopTensor::engine_type,
        typename IMedLoopTensor::layout_type,
        typename IMedLoopDumpTensor::engine_type,
        typename IMedLoopDumpTensor::layout_type>;

    using ADumpersT = RawDeviceDump<typename ATensor::engine_type,
        cute::Layout<ADumpTensorShape, AStride>,
        typename ADumpTensor::engine_type,
        typename ADumpTensor::layout_type>;

    using CandidatesDumpersT = RawHostDump<typename CandidatesTensor::engine_type,
        typename CandidatesTensor::layout_type,
        typename CandidatesDumpTensor::engine_type,
        typename CandidatesDumpTensor::layout_type>;

    std::vector<IDumpersT> I_dumpers;
    std::vector<IMedLoopDumpersT> med_loop_I_dumpers;
    std::vector<ADumpersT> A_dumpers;
    std::vector<CandidatesDumpersT> candidates_dumpers;

    std::vector<std::ofstream> tight_loop_stat_fds;
    std::vector<std::ofstream> med_loop_stat_fds;
    std::vector<std::ofstream> loose_loop_stat_fds;

    uint64_t ref_timestamp, stop_timestamp;
    std::ofstream timestamp_dump;

    uint32_t i_tight_loop{ 0u };
    uint32_t i_med_loop{ 0u };
	static constexpr uint32_t tight_loop_iters = N_T_MEDIUM_LOOP * FOLD_FACTOR / N_T_RAW;
	static constexpr uint32_t med_loop_iters   = N_T_LOOSE_LOOP / N_T_MEDIUM_LOOP;

    volatile std::atomic_uint32_t med_loop_avail_iters{ 0u };

private:
    void parse_args(int argc, char* argv[]);

    void init_context();
    void allocs();
    void dumpers();
    void write_conf(const std::filesystem::path& path);


    void* d_ws;      // Device workspace
    void* h_ws;      // Host workspace (pinned host memory)
    void* h_ws_dptr; // Device pointer to host workspace

    template <typename Index=uint32_t>
    typename ITensor::layout_type I_layout(const int beam, const Index head=Index()) const
    {
        const auto sb = subbands[beam];

        return cute::make_layout(
            cute::make_shape(sb.second - sb.first + 1, cute::constant<size_t, N_T_LOOSE_LOOP>{}),
            cute::make_stride(cute::constant<size_t, REDUNDANCY * N_T_LOOSE_LOOP>{},
                              cute::RingBufferStride<1, REDUNDANCY * N_T_LOOSE_LOOP, uint32_t>(head))
        );
    }

    template <typename Index=uint32_t>
    typename ATensor::layout_type A_layout(const Index head=Index()) const
    {
        return cute::make_layout(
            cute::make_shape(cute::constant<size_t, N_DM>{}, cute::constant<size_t, N_T_OUT>{}),
            cute::make_stride(cute::constant<size_t, N_T_OUT>{}, cute::RingBufferStride<1, N_T_OUT, uint32_t>(head))
        );
    }

    typename CandidatesTensor::layout_type candidates_layout() const
    {
        return {};
    }


    template <typename Index=uint32_t>
    ITensor make_I(const int beam, const Index head=Index()) const
    {
        return cute::make_tensor(
            I[beam].data(),
            I_layout(beam, head)
        );
    }

    template <typename Index=uint32_t>
    ATensor make_A(const int beam, const Index head=Index()) const
    {
        return cute::make_tensor(
            A[beam].data(),
            A_layout(head)
        );
    }

    CandidatesTensor make_candidates(const int beam) const
    {
        return cute::make_tensor(
            candidates[beam].data(),
            candidates_layout()
        );
    }

    CandidatesTensor make_candidates_dptr(const int beam) const
    {
        return cute::make_tensor(
            candidates_dptr[beam].data(),
            candidates_layout()
        );
    }

    
	int lowestCudaStreamPriority, greatestCudaStreamPriority;
    int nPriorities;
};
} // namespace nenufrb