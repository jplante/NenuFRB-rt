#pragma once

#include <cuda_runtime.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/stat.h>
#include <cufile.h>
#include <cute/tensor.hpp>

#include <helper_cuda.cuh>

#include <cooperative_groups.h>

namespace {
RTE_LOG_REGISTER(dumper, dumper, INFO);
}

namespace kernel {

namespace cg = cooperative_groups;

template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
__global__ void copy_tensor(const cute::Tensor<InputEngine, InputLayout> in,
	cute::Tensor<OutputEngine, OutputLayout> out)
{
	auto grid = cg::this_grid();
	const auto compact_in_layout = cute::make_layout( // Needed when using RingBufferStride (issue with operator/)
		in.shape(),
		cute::GenRowMajor{}
	);

	for (int i = grid.thread_rank(); i < cute::size(out); i += grid.num_threads())
	{
		const auto coord = compact_in_layout.get_hier_coord(i);

		out[coord] = in[coord];
	}
}
} // namespace kernel

template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
static void copy_tensor(const cute::Tensor<InputEngine, InputLayout>& in,
	cute::Tensor<OutputEngine, OutputLayout>& out, cudaStream_t stream=NULL)
{
	const unsigned int nThreads = 512;
	const unsigned int nBlocks = CUB_QUOTIENT_CEILING(cute::size(in), nThreads);

	kernel::copy_tensor<<<nBlocks, nThreads, 0, stream>>>(in, out);
}


template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
class BaseDumper
{
public:
	using InputTensor  = cute::Tensor<InputEngine, InputLayout>;
	using InputT       = typename InputEngine::value_type;
	using OutputTensor = cute::Tensor<OutputEngine, OutputLayout>;
	using OutputT      = typename OutputEngine::value_type;

// TODO: add more interfaces
};

template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
class BaseDeviceDumper : BaseDumper<InputEngine, InputLayout,
	OutputEngine, OutputLayout>
{
public:
	using BaseDumper_  = BaseDumper<InputEngine, InputLayout, OutputEngine, OutputLayout>;
	using InputTensor  = typename BaseDumper_::InputTensor;
	using InputT       = typename BaseDumper_::InputT;
	using OutputTensor = typename BaseDumper_::OutputTensor;
	using OutputT      = typename BaseDumper_::OutputT;

protected:
	virtual void process_to_buf(const InputTensor& in, OutputTensor& buf, cudaStream_t stream) = 0;
public:
	BaseDeviceDumper(std::filesystem::path path,
		const cute::Tensor<InputEngine, InputLayout>&, // Used only for type resolution
		const cute::Tensor<OutputEngine, OutputLayout>& out)
		: buf_size(cute::size(out) * sizeof(OutputT)),
			data(cute::make_tensor(cute::make_gmem_ptr(reinterpret_cast<OutputT*>(NULL)), typename OutputTensor::layout_type()))
	{
		std::ostringstream ss_log;
		ss_log << "DUMPER.DEVICE: Allocating with buf_size = " << buf_size << ": " << out.layout() << std::endl;
		rte_log(RTE_LOG_DEBUG, dumper, "%s", ss_log.str().c_str());
		checkCudaErrors(cudaMalloc(&data_ptr, buf_size));
		data = cute::make_tensor(
			cute::make_gmem_ptr(data_ptr),
			out.layout()
		);

		fd = open(path.c_str(), O_CREAT | O_WRONLY | O_DIRECT | O_TRUNC, 0644);

		CUfileDescr_t descr;
		memset(&descr, 0, sizeof(CUfileDescr_t));

		descr.handle.fd = fd;
		descr.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;
		checkCudaErrors(cuFileHandleRegister(&fh, &descr));
		checkCudaErrors(cuFileBufRegister(data_ptr, buf_size, 0));
	}

	BaseDeviceDumper(const BaseDeviceDumper&) = delete;
	BaseDeviceDumper(BaseDeviceDumper&&) = default;

	virtual ~BaseDeviceDumper()
	{
		checkCudaErrors(cuFileBufDeregister(data_ptr));
		cuFileHandleDeregister(fh);
		checkCudaErrors(cudaFree(data_ptr));
		close(fd);
	}


	virtual void operator()(const InputTensor& in, cudaStream_t stream=NULL) final
	{
		process_to_buf(in, data, stream);
		checkCudaErrors(cudaStreamSynchronize(stream));
		cuFileWrite(fh, data_ptr, buf_size, file_offset, 0);
		// cudaFileWriteAsync should be coming in a future CUDA version...

		file_offset += buf_size;
	}

protected:
	int fd{ 0 };
	const size_t buf_size;
	OutputT* data_ptr;
	OutputTensor data;
	size_t file_offset{ 0ull };
	CUfileHandle_t fh;
};

template <typename InputEngine, typename InputLayout, 
	typename OutputEngine, typename OutputLayout>
class RawDeviceDump : virtual public BaseDeviceDumper<InputEngine, InputLayout, OutputEngine, OutputLayout>
{
private:
	using BaseDumper_ = BaseDeviceDumper<InputEngine, InputLayout, OutputEngine, OutputLayout>;
	using InputTensor  = typename BaseDumper_::InputTensor;
	using OutputTensor = typename BaseDumper_::OutputTensor;

public:

	RawDeviceDump(std::filesystem::path path,
		const InputTensor& in,
		const OutputTensor& out)
		: BaseDumper_(path, in, out)
	{
	}

protected:
	virtual void process_to_buf(const InputTensor& in, OutputTensor& buf, cudaStream_t stream=NULL) override
	{
		copy_tensor(in, buf, stream);
	}
};


template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
class BaseHostDumper : BaseDumper<InputEngine, InputLayout,
	OutputEngine, OutputLayout>
{
public:
	using BaseDumper_  = BaseDumper<InputEngine, InputLayout, OutputEngine, OutputLayout>;
	using InputTensor  = typename BaseDumper_::InputTensor;
	using InputT       = typename BaseDumper_::InputT;
	using OutputTensor = typename BaseDumper_::OutputTensor;
	using OutputT      = typename BaseDumper_::OutputT;

protected:
	virtual void process_to_buf(const InputTensor& in, OutputTensor& buf) = 0;
public:
	BaseHostDumper(std::filesystem::path path,
	 	const cute::Tensor<InputEngine, InputLayout>&,
		const cute::Tensor<OutputEngine, OutputLayout>& out)
		: buf_size(cute::size(out) * sizeof(OutputT)), fd(path, std::ios::out | std::ios::binary),
			data(cute::make_tensor(cute::make_gmem_ptr(reinterpret_cast<OutputT*>(NULL)), typename OutputTensor::layout_type()))
	{
		std::ostringstream ss_log;
		ss_log << "DUMPER.HOST: Allocating with buf_size = " << buf_size << ": " << out.layout() << std::endl;
		rte_log(RTE_LOG_DEBUG, dumper, "%s", ss_log.str().c_str());
		data_ptr = new OutputT[buf_size];
		data = cute::make_tensor(
			cute::make_gmem_ptr(data_ptr),
			out.layout()
		);
	}

	BaseHostDumper(const BaseHostDumper&) = delete;
	BaseHostDumper(BaseHostDumper&&) = default;

	virtual ~BaseHostDumper()
	{
		fd.close();
		delete[] data_ptr;
	}


	virtual void operator()(const InputTensor& in) final
	{
		process_to_buf(in, data);
		fd.write(reinterpret_cast<char*>(data_ptr), cute::size(data) * sizeof(OutputT));
	}

protected:
	std::ofstream fd;
	const size_t buf_size;
	OutputT* data_ptr;
	OutputTensor data;
};

template <typename InputEngine, typename InputLayout,
	typename OutputEngine, typename OutputLayout>
class RawHostDump : virtual public BaseHostDumper<InputEngine, InputLayout,
	OutputEngine, OutputLayout>
{
private:
	using BaseDumper_  = BaseHostDumper<InputEngine, InputLayout, OutputEngine, OutputLayout>;
	using InputTensor  = typename BaseDumper_::InputTensor;
	using OutputTensor = typename BaseDumper_::OutputTensor;

public:

	RawHostDump(std::filesystem::path path,
		const InputTensor& in,
		const OutputTensor& out)
		: BaseDumper_(path, in, out)
	{
	}

protected:
	virtual void process_to_buf(const InputTensor& in, OutputTensor& buf) override
	{
		const auto compact_in_layout = cute::make_layout( // Needed when using RingBufferStride (issue with operator/)
			in.shape(),
			cute::GenRowMajor{}
		);

		for (int i = 0; i < cute::size(in); i++)
		{
			const auto coord = compact_in_layout.get_hier_coord(i);
			buf[coord] = in[coord];
		}
	}
};

/* Not updated
template <typename Input2DArray_>
class HeatmapDump : virtual public BaseDumper<Input2DArray_, typename Input2DArray_::value_type>
{
private:
	using BaseDumper_ = BaseDumper<Input2DArray_, typename Input2DArray_::value_type>;

	using BaseDumper_::stream;

public:
	using Input2DArray = typename BaseDumper_::Input2DArray;
	using InT          = typename BaseDumper_::InT;
	using OutT         = typename BaseDumper_::OutT;

	HeatmapDump(std::filesystem::path path, const int64_t N_t, const int64_t n_t, const int64_t max_height)
		: BaseDumper_(path, n_t, max_height), N_t(N_t)
	{
	}

protected:
	void process_to_buf(const Input2DArray ring, OutT* buf)
	{
		xfold_array<1<<11, NoOp<InT>>(buf, ring, N_t, stream);
	}

private:
	const int64_t N_t;
};

template <typename Input2DArray_>
class HeatmapDumpSqmod : virtual public BaseDumper<Input2DArray_, typename Input2DArray_::value_type::value_type>
{
private:
	using BaseDumper_ = BaseDumper<Input2DArray_, typename Input2DArray_::value_type::value_type>;

	using BaseDumper_::stream;

public:
	using Input2DArray = typename BaseDumper_::Input2DArray;
	using InT          = typename BaseDumper_::InT;
	using OutT         = typename BaseDumper_::OutT;

	HeatmapDumpSqmod(std::filesystem::path path, const int64_t N_t, const int64_t n_t, const int64_t max_height)
		: BaseDumper_(path, n_t, max_height), N_t(N_t)
	{
	}

protected:
	void process_to_buf(const Input2DArray ring, OutT* buf)
	{
		xfold_array<1<<11, SqmodOp<InT>>(buf, ring, N_t, stream);
	}

private:
	const int64_t N_t;
};
*/