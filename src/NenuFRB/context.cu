#include "context.cuh"

#include <getopt.h>
#include <sstream>
#include <cstdlib>
#include <valarray>
#include <numeric>
#include <iomanip>
#include <stdexcept>

#include <rte_eal.h>
#include <rte_ethdev.h>
#include <rte_gpudev.h>
#include <rte_version.h>
#include <cuda_runtime.h>


#include <Preprocessing/MADClean.cuh>
#include <helper_cuda.cuh>


namespace nenufrb {

namespace {
RTE_LOG_REGISTER(context, context, INFO);
}

Context::Context(int argc, char* argv[])
{
	int cudaRuntimeVersion, cudaRuntimeMajor, cudaRuntimeMinor;
	cudaRuntimeGetVersion(&cudaRuntimeVersion);
	{
		auto div = std::div(cudaRuntimeVersion, 1000);
		cudaRuntimeMajor = div.quot;
		div = std::div(div.rem, 10);
		cudaRuntimeMinor = div.quot;
	}

	rte_log(RTE_LOG_INFO, context, "CONTEXT: Compiled with CUDA %d.%d, %s\n",
		cudaRuntimeMajor, cudaRuntimeMinor, rte_version());

	parse_args(argc, argv);
	init_context();
	allocs();
	dumpers();

	write_conf(out_dir / "conf.parset");

	checkCudaErrors(cudaStreamSynchronize(tight_loop_streams[0]));
}


void Context::parse_args(int argc, char* argv[])
{
	{
		int ret;
		ret = rte_eal_init(argc, argv);

		if (ret < 0)
		{
			std::ostringstream ss;
			ss << "Cannot init EAL";
			throw std::runtime_error(ss.str());
		}

		argc -= ret;
		argv += ret;
	}

	bool help{false};
	while (true)
	{
		static struct option long_options[] =
		{
			{ "dump_raw",            no_argument,       0, 'r' },
			{ "dump_I",              no_argument,       0, 'I' },
			{ "dump_tmp_I",          no_argument,       0, 'i' },
			{ "dump_A",              no_argument,       0, 'A' },
			{ "dump_candidates",     no_argument,       0, 'c' },
			{ "out",                 required_argument, 0, 'o' },
			{ "help",                no_argument,       0, 'h' },
			{ 0, 0, 0, 0 }
		};

		int option_index = 0;

		int c = getopt_long(argc, argv, "ho:", long_options, &option_index);
		if (c == -1)
			break;

		std::ostringstream ss;
		switch (c)
		{
		case 'r':
			dump_raw = true;
			break;
		case 'I':
			dump_I = true;
			break;
		case 'i':
			dump_tmp_I = true;
			break;
		case 'A':
			dump_A = true;
			break;
		case 'c':
			dump_candidates = true;
			break;
		case 'h':
			help = true;
			break;
		case 'o':
			out_dir = std::filesystem::path(optarg);
			break;
		default:
			ss << __func__ << ": ERROR - Failed to parse command line";
			throw std::runtime_error(ss.str());
		}
	}

	if (help)
	{
		std::printf(R"help(Usage: ./NenuFRB [EAL args] -- [NenuFRB args] parset_file
FRB detection for NenuFAR
Listens on the NIC detected by DPDK for RSP CEP frames, and apply a simple detection pipeline:
Incoherent Dedispersion -> Detection.

EAL arguments are described here: https://doc.dpdk.org/guides/linux_gsg/linux_eal_parameters.html
NenuFRB arguments are:

--dump_raw                        Dump raw TF data in a file
--dump_I                          Dump TF data after preprocessing in a file
--dump_tmp_I                      Dump intermediate TF planes during preprocessing in a file
--dump_A                          Dump dedispersed data in a file
--help                            Print this message
)help");
		std::exit(EXIT_SUCCESS);
	}

	if (optind != argc - 1)
	{
		if (optind < argc - 1)
		{
			std::ostringstream ss;
			ss << __func__ << ": ERROR - Missing non-option argument parset file";
			throw std::runtime_error(ss.str());
		}
		else if (optind > argc - 1)
			std::cerr << __func__ << ": WARNING - ignoring trailing arguments" << std::endl;
	}

	parset_file = std::filesystem::path(argv[optind]);
	if (!std::filesystem::exists(parset_file))
	{
        std::ostringstream ss;
        ss << strerror(ENOENT) << ": " << parset_file;
        throw std::runtime_error(ss.str());
	}

	pr = std::make_unique<ParsetReader>(parset_file);
	n_beams = pr->n_elems("Beam");
	subbands.resize(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		std::ostringstream ss;
		ss << "Beam[" << i << ']';
		subbands[i] = pr->get(ss.str(), "subbandList").as_range();
	}

	try
	{
		std::tm stop_tm = pr->get("Observation", "stopTime").as_time();
		stop_timestamp = static_cast<uint64_t>(std::mktime(&stop_tm)) * fsx2 / 2ull;
	}
	catch (const std::runtime_error& e)
	{
		std::cerr << "stopTime not found, NenuFRB will run until receiving SIGINT (what(): " << e.what() << ')' << std::endl;
		stop_timestamp = std::numeric_limits<decltype(stop_timestamp)>::max();
	}

	dmt_cfgs.resize(n_beams);
	for (int beam = 0; beam < n_beams; beam++)
	{
		struct Dmt::Config cfg = {
			.N = 1024,

			.fmin = subbands[beam].first  * DF_RAW / 1e6f,
			.fmax = subbands[beam].second * DF_RAW / 1e6f,
			.DMmin = DMmin,
			.DMmax = DMmax,
			.dt = DT
		};

		dmt_cfgs[beam] = cfg;
	}
}


void Context::init_context()
{
	nb_lcores = rte_lcore_count();
	if (nb_lcores < 4)
	{
        std::ostringstream ss;
        ss << __func__ << ": ERROR - At least 4 lcores must be available, found " << nb_lcores;
        throw std::runtime_error(ss.str());
	}

	port = rte_eth_find_next(0);
	dpdk_gpu_id = rte_gpu_find_next(0, RTE_GPU_ID_ANY);
	poll_lcore = rte_get_next_lcore(0, true, false);
	med_loop_lcore = rte_get_next_lcore(poll_lcore, true, false);
	loose_loop_lcore = rte_get_next_lcore(med_loop_lcore, true, false);

	// Recover CUDA device id from PCI id
	struct rte_gpu_info gpu_info;
	rte_gpu_info_get(dpdk_gpu_id, &gpu_info);
	checkCudaErrors(cudaDeviceGetByPCIBusId(&cuda_gpu_id, gpu_info.name));
	checkCudaErrors(cudaSetDevice(cuda_gpu_id));

	checkCudaErrors(cudaDeviceGetStreamPriorityRange(&lowestCudaStreamPriority, &greatestCudaStreamPriority));
	nPriorities = lowestCudaStreamPriority - greatestCudaStreamPriority + 1;

	tight_loop_streams.resize(n_beams);
	for (cudaStream_t& s : tight_loop_streams)
		checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestCudaStreamPriority + 1 * nPriorities / 3));
	
	med_loop_streams.resize(n_beams);
	for (cudaStream_t& s : med_loop_streams)
		checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestCudaStreamPriority + 1 * nPriorities / 3));

	loose_loop_streams.resize(n_beams);
	for (cudaStream_t& s : loose_loop_streams)
		checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestCudaStreamPriority + 1 * nPriorities / 3));

}

void Context::allocs()
{
	I.resize(n_beams, cute::make_tensor( // Initalize with a dummy ptr, gmem_ptr does not have default constructor...
		cute::make_gmem_ptr(reinterpret_cast<typename ITensor::value_type*>(NULL)),
		typename ITensor::layout_type()));
	A.resize(n_beams, cute::make_tensor(
		cute::make_gmem_ptr(reinterpret_cast<typename ATensor::value_type*>(NULL)),
		typename ATensor::layout_type()
	));
	candidates.resize(n_beams, cute::make_tensor(
		cute::make_gmem_ptr(reinterpret_cast<typename CandidatesTensor::value_type*>(NULL)),
		typename CandidatesTensor::layout_type()
	));
	candidates_dptr.resize(n_beams, cute::make_tensor(
		cute::make_gmem_ptr(reinterpret_cast<typename CandidatesTensor::value_type*>(NULL)),
		typename CandidatesTensor::layout_type()
	));

	mad_ws.resize(n_beams);
	rmb_ws.resize(n_beams);
	mean_ws.resize(n_beams);
	detection_ws.resize(n_beams);

	// Compute size and offsets of different device workspaces
	{
		static constexpr int n_ws = 6;
		std::valarray<size_t> ws_sizes(n_beams * n_ws), offsets(n_beams * n_ws);

		for (int beam = 0; beam < n_beams; beam++)
		{
			auto& _I = I[beam];
			_I = make_I(beam); // Create a dummy tensor with the right shape but no storage (NULL)
			auto _I_slice = cute::make_tensor(
				_I.data(),
				cute::make_layout(
					cute::make_shape(cute::shape<0>(_I), cute::constant<size_t, N_T_TIGHT_LOOP>{}),
					_I.stride()
				)
			);

			ws_sizes[beam * n_ws + 0] = REDUNDANCY * cute::size(_I) * sizeof(typename ITensor::value_type);

			auto& _A = A[beam];
			_A = make_A(beam);
			ws_sizes[beam * n_ws + 1] = cute::size(_A) * sizeof(typename ATensor::value_type);

			ws_sizes[beam * n_ws + 2] = MADClean::dry_run(_I_slice);
			ws_sizes[beam * n_ws + 3] = RmBaseline<FFTLEN>::dry_run(_I_slice);
			ws_sizes[beam * n_ws + 4] = RmMean::dry_run(_I_slice);


			auto _A_slice = cute::make_tensor(
				_A.data(),
				cute::make_layout(
					cute::make_shape(cute::shape<0>(_A), cute::shape<1>(_I)),
					_A.stride()
				)
			);

			ws_sizes[beam * n_ws + 5] = Adaptive1D::dry_run(_A_slice);
		}

		ws_sizes = CUB_ROUND_UP_NEAREST(ws_sizes, sizeof(T)); // For alignment

		checkCudaErrors(cudaMalloc(&d_ws, ws_sizes.sum()));

		std::exclusive_scan(std::begin(ws_sizes), std::end(ws_sizes), std::begin(offsets), 0ull);
		
		for (int beam = 0; beam < n_beams; beam++)
		{
			const uintptr_t _d_ws = reinterpret_cast<uintptr_t>(d_ws);
			I[beam].data()     = cute::make_gmem_ptr(reinterpret_cast<typename ITensor::value_type*>(_d_ws + offsets[beam * n_ws + 0]));
			A[beam].data()     = cute::make_gmem_ptr(reinterpret_cast<typename ATensor::value_type*>(_d_ws + offsets[beam * n_ws + 1]));
			mad_ws[beam]       = reinterpret_cast<void*>(_d_ws + offsets[beam * n_ws + 2]);
			rmb_ws[beam]       = reinterpret_cast<void*>(_d_ws + offsets[beam * n_ws + 3]);
			mean_ws[beam]      = reinterpret_cast<void*>(_d_ws + offsets[beam * n_ws + 4]);
			detection_ws[beam] = reinterpret_cast<void*>(_d_ws + offsets[beam * n_ws + 5]);

			checkCudaErrors(cudaMemsetAsync(A[beam].data().get(), 0, ws_sizes[beam * n_ws + 1], tight_loop_streams[0]));
		}
	}

	// Same for host workspaces
	{
		static constexpr int n_ws = 1;
		std::valarray<size_t> ws_sizes(n_beams * n_ws), offsets(n_beams * n_ws);

		for (int beam = 0; beam < n_beams; beam++)
		{
			auto& _candidates = candidates[beam];
			_candidates = make_candidates(beam);
			ws_sizes[beam * n_ws + 0] = cute::size(_candidates) * sizeof(typename CandidatesTensor::value_type);
		}

		checkCudaErrors(cudaHostAlloc(&h_ws, ws_sizes.sum(), cudaHostAllocMapped));
		checkCudaErrors(cudaHostGetDevicePointer(&h_ws_dptr, h_ws, 0));
		
		std::exclusive_scan(std::begin(ws_sizes), std::end(ws_sizes), std::begin(offsets), 0ull);

		for (int beam = 0; beam < n_beams; beam++)
		{
			const uintptr_t _h_ws = reinterpret_cast<uintptr_t>(h_ws);
			const uintptr_t _h_ws_dptr = reinterpret_cast<uintptr_t>(h_ws_dptr);

			candidates[beam].data() = cute::make_gmem_ptr(
				reinterpret_cast<typename CandidatesTensor::value_type*>(_h_ws + offsets[beam * n_ws + 0]));
			candidates_dptr[beam].data() = cute::make_gmem_ptr(
				reinterpret_cast<typename CandidatesTensor::value_type*>(_h_ws_dptr + offsets[beam * n_ws + 0]));
		}
	}

	for (int beam = 0; beam < n_beams; beam++)
	{
		I[beam] = make_I(beam);
		A[beam] = make_A(beam);
		candidates[beam] = make_candidates(beam);
		candidates_dptr[beam] = make_candidates(beam);
	}
}

void Context::dumpers()
{
	if (out_dir.empty())
		out_dir = std::filesystem::absolute(parset_file).parent_path() / parset_file.stem();
	
	if (!std::filesystem::exists(out_dir) || std::filesystem::is_directory(out_dir))
		std::filesystem::create_directory(out_dir, std::filesystem::current_path());
	struct stat cwd_stat;
	stat(std::filesystem::current_path().c_str(), &cwd_stat);
	chown(out_dir.c_str(), cwd_stat.st_uid, cwd_stat.st_gid);

	if (parset_file != out_dir / parset_file.filename())
		std::filesystem::copy_file(parset_file, out_dir / parset_file.filename(), std::filesystem::copy_options::overwrite_existing);


	checkCudaErrors(cuFileDriverOpen());
	dump_streams.resize(n_beams);
	if (dump_raw || dump_I || dump_A)
	{
		for (auto& s : dump_streams)
			checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestCudaStreamPriority + 2 * nPriorities / 3));
	}

	events.resize(n_beams);

	if (dump_I)
		I_dumpers.reserve(n_beams);
	if (dump_tmp_I)
		med_loop_I_dumpers.reserve(n_beams);
	if (dump_A)
		A_dumpers.reserve(n_beams);
	if (dump_candidates)
		candidates_dumpers.reserve(n_beams);

	for (int beam = 0; beam < n_beams; beam++)
	{
		if (dump_I)
		{
			auto& _I = I[beam];

			std::ostringstream ss;
			ss << "I-" << beam << ".dump";

			std::ostringstream ss_log;
			ss_log << "CONTEXT.ALLOC: Allocating I dumper " << ss.str() << " for " << _I.layout() << std::endl;
			rte_log(RTE_LOG_DEBUG, context, "%s", ss_log.str().c_str());
			I_dumpers.emplace_back(out_dir / ss.str(), _I,
				cute::make_tensor(
					_I.data(),
					cute::make_layout(
						_I.shape(),
						cute::GenRowMajor{}
					)
				)
			);
		}
		
		if (dump_tmp_I)
		{
			auto& _I = I[beam];
			auto _I_slice = cute::make_tensor(
				_I.data(),
				cute::make_layout(
					cute::make_shape(cute::shape<0>(_I), cute::constant<size_t, N_T_MEDIUM_LOOP>{}),
					_I.stride()
				)
			);

			std::ostringstream ss;
			ss << "med_loop_I-" << beam << ".dump";
			std::ostringstream ss_log;
			ss_log << "CONTEXT.ALLOC: Allocating tmp_I dumper " << ss.str() << " for " << _I_slice.layout() << std::endl;
			rte_log(RTE_LOG_DEBUG, context, "%s", ss_log.str().c_str());
			med_loop_I_dumpers.emplace_back(out_dir / ss.str(), _I_slice,
				cute::make_tensor(
					_I_slice.data(),
					cute::make_layout(
						_I_slice.shape(),
						cute::GenRowMajor{}
					)
				)
			);
		}
		
		if (dump_A)
		{
			auto& _A = A[beam];
			auto  _A_slice = cute::make_tensor(
				_A.data(),
				cute::make_layout(
					ADumpTensorShape{},
					_A.stride()
				)
			);

			std::ostringstream ss;
			ss << "A-" << beam << ".dump";
			std::ostringstream ss_log;
			ss_log << "CONTEXT.ALLOC: Allocating A dumper " << ss.str() << " for " << _A.layout() << std::endl;
			rte_log(RTE_LOG_DEBUG, context, "%s", ss_log.str().c_str());
			A_dumpers.emplace_back(out_dir / ss.str(), _A_slice,
				cute::make_tensor(
					_A_slice.data(),
					cute::make_layout(
						_A_slice.shape(),
						cute::GenRowMajor{}
					)
				)
			);
		}
		
		if (dump_candidates)
		{
			auto& _candidates = candidates[beam];

			std::ostringstream ss;
			ss << "candidates-" << beam << ".dump";
			std::ostringstream ss_log;
			ss_log << "CONTEXT.ALLOC: Allocating candiates dumper " << ss.str() << " for " << _candidates.layout() << std::endl;
			rte_log(RTE_LOG_DEBUG, context, "%s", ss_log.str().c_str());
			candidates_dumpers.emplace_back(out_dir / ss.str(), _candidates,
				cute::make_tensor(
					_candidates.data(),
					cute::make_layout(
						_candidates.shape(),
						cute::GenRowMajor{}
					)
				)
			);
		}
	}

	if (dump_timestamps)
	{
		timestamp_dump.open(out_dir / "timestamps.dump", std::ios::out | std::ios::binary);
		if (!timestamp_dump)
		{
			std::cerr << "WARNING: could not open timestamps.dump" << std::endl;
		}
	}
	

	// if (dump_stats)
	{
		tight_loop_stat_fds.reserve(n_beams);
		med_loop_stat_fds.reserve(n_beams);
		loose_loop_stat_fds.reserve(n_beams);

		for (int beam = 0; beam < n_beams; beam++)
		{
			{
				std::ostringstream ss;
				ss << "tight_loop_stats-" << beam << ".dump";
				
				tight_loop_stat_fds.emplace_back(out_dir / ss.str(), std::ios::out | std::ios::binary);
			}
			
			{
				std::ostringstream ss;
				ss << "med_loop_stats-" << beam << ".dump";
				
				med_loop_stat_fds.emplace_back(out_dir / ss.str(), std::ios::out | std::ios::binary);
			}

			{
				std::ostringstream ss;
				ss << "loose_loop_stats-" << beam << ".dump";
				
				loose_loop_stat_fds.emplace_back(out_dir / ss.str(), std::ios::out | std::ios::binary);
			}
		}
	}
}


void Context::write_conf(const std::filesystem::path& path)
{
	std::ofstream fd(path, std::ios::out);

	fd << "Buffers.N_T_RAW = " << N_T_RAW << std::endl;
	fd << "Buffers.N_T_TIGHT_LOOP = " << N_T_TIGHT_LOOP << std::endl;
	fd << "Buffers.N_T_MEDIUM_LOOP = " << N_T_MEDIUM_LOOP << std::endl;
	fd << "Buffers.N_T_LOOSE_LOOP = " << N_T_LOOSE_LOOP << std::endl;
	fd << "Buffers.N_T_OUT = " << N_T_OUT << std::endl;
	fd << "Buffers.N_DM    = " << N_DM    << std::endl;
	fd << "Buffers.N_CANDIDATES = " << N_CANDIDATES << std::endl;
	fd << std::endl;

	fd << "Preprocessing.FFTLEN = " << FFTLEN << std::endl;
	fd << "Preprocessing.TIME_INTEGRATION = " << TIME_INTEGRATION << std::endl;
	fd << "Preprocessing.FOLD_FACTOR = " << FOLD_FACTOR << std::endl;
	fd << std::endl;

	constexpr auto maxp = std::numeric_limits<float>::digits10 + 1;
	fd << "Resolution.DT_RAW = " << std::setprecision(maxp) << DT_RAW << std::endl;
	fd << "Resolution.DT     = " << std::setprecision(maxp) << DT     << std::endl;
	fd << "Resolution.DF_RAW = " << std::setprecision(maxp) << DF_RAW << std::endl;
	fd << "Resolution.DF     = " << std::setprecision(maxp) << DF     << std::endl;
	fd << std::endl;

	fd << "Dmt.DMmin = " << DMmin << std::endl;
	fd << "Dmt.DMmax = " << DMmax << std::endl;
	fd << std::endl;     
}                     

Context::~Context()
{
	timestamp_dump.close();

	for (std::ofstream& fd : loose_loop_stat_fds)
		fd.close();
	for (std::ofstream& fd : med_loop_stat_fds)
		fd.close();
	for (std::ofstream& fd : loose_loop_stat_fds)
		fd.close();

	for (cudaStream_t& s : tight_loop_streams)
	{
		checkCudaErrors(cudaStreamSynchronize(s));
		checkCudaErrors(cudaStreamDestroy(s));
	}
	for (cudaStream_t& s : loose_loop_streams)
	{
		checkCudaErrors(cudaStreamSynchronize(s));
		checkCudaErrors(cudaStreamDestroy(s));
	}

	for (cudaStream_t& s : dump_streams)
		checkCudaErrors(cudaStreamDestroy(s));

	checkCudaErrors(cudaFreeHost(h_ws));
	checkCudaErrors(cudaFree(d_ws));
}
} // namespace nenufrb