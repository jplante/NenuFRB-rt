#include <cuda.h>
#include <cuda_runtime.h>

#include <rte_eal.h>
#include <rte_ethdev.h>

#include <signal.h>
#include <getopt.h>

#include <utils/Bitset.cuh>
#include <Config/ParsetReader.hpp>
#include <Config/DefaultMask.hpp>
#include <Acquisition/RSP_CEP.cuh>
#include <Preprocessing/ChunkedFFT.cuh>
#include <Preprocessing/MADClean.cuh>
#include <Dedispersion/BruteforceDmt.cuh>
#include <Detection/Adaptive1D.cuh>
#include <utils/ring_buffer_stride.hpp>

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>

#include <helper_cuda.cuh>


#include "context.cuh"


namespace {
RTE_LOG_REGISTER(nenufrb_log, nenufrb, INFO);
RTE_LOG_REGISTER(nenufrb_timestamp, nenufrb.timestamp, INFO);
RTE_LOG_REGISTER(tight_loop, nenufrb.tight_loop, INFO);
RTE_LOG_REGISTER(med_loop, nenufrb.med_loop, INFO);
RTE_LOG_REGISTER(loose_loop, nenufrb.loose_loop, INFO);
}

volatile bool stop;
void signal_handler(int sig)
{
	switch (sig)
	{
	case SIGINT:
	case SIGTERM:
	case SIGUSR1:
		stop = 1;
		break;
	default:
		rte_log(RTE_LOG_WARNING, nenufrb_log, "NENUFRB: Unknown signal %d\n", sig);
	}
}

template <typename Engine, typename Layout, typename Shape, typename Coord>
decltype(auto) subview(const cute::Tensor<Engine, Layout>& tensor, const Shape& new_shape, const Coord& offset)
{
	auto retval = cute::make_tensor(
		tensor.data() + tensor.layout()(offset),
		cute::make_layout(
			new_shape,
			cute::stride(tensor)
		)
	);

	return retval;
}

using namespace std::chrono_literals;


template <typename Layout>
struct stride_type;

template <typename Shape, typename Stride>
struct stride_type<cute::Stride<Shape, Stride>>
{
	using type = Stride;
};

struct MedLoopCoreArgs
{
	nenufrb::Context* ctx;
	size_t i_loop;
	std::vector<typename nenufrb::Context::ITensor> I;
};

int medium_loop_core(void* args)
{
	MedLoopCoreArgs* _args = reinterpret_cast<MedLoopCoreArgs*>(args);
	nenufrb::Context* ctx = _args->ctx;
	const size_t i_med_loop = _args->i_loop;

	checkCudaErrors(cudaSetDevice(ctx->cuda_gpu_id));

	for (int i = 0; i < ctx->n_beams; i++)
	{
		auto& I = _args->I[i];
		auto  I_slice = cute::make_tensor(
			I.data(),
			cute::make_layout(
				cute::make_shape(
					cute::shape<0>(I),
					cute::constant<size_t, nenufrb::Context::N_T_MEDIUM_LOOP>{}
				),
				I.stride()
			)
		);

		cute::advance_head<1>(I_slice, i_med_loop * nenufrb::Context::N_T_MEDIUM_LOOP);

		auto& mad_ws = ctx->mad_ws[i];
		auto& rmb_ws = ctx->rmb_ws[i];
		auto& mean_ws = ctx->mean_ws[i];

		auto& med_loop_I_dumper = ctx->med_loop_I_dumpers[i];
		auto& stream = ctx->med_loop_streams[i];
		auto& events = ctx->events[i];

		checkCudaErrors(cudaStreamWaitEvent(stream, events.ReductionEnd));


		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);

#define RECORD(x) checkCudaErrors(cudaEventRecord((x), stream))
		RECORD(events.MAD10Start);
		auto [median, mad] = MADClean::run(I_slice, I_slice, 10, mad_ws, stream);
		RECORD(events.MAD10End);

		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);

		RECORD(events.RmBaseline0Start);
		RmBaseline<nenufrb::Context::FFTLEN>::run(I_slice, I_slice, median, rmb_ws, stream);
		RECORD(events.RmBaseline0End);
		
		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);
		
		RECORD(events.MAD3Start);
		std::tie(median, mad) = MADClean::run(I_slice, I_slice, 3, mad_ws, stream);
		RECORD(events.MAD3End);

		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);

		RECORD(events.RmBaseline1Start);
		RmBaseline<nenufrb::Context::FFTLEN>::run(I_slice, I_slice, median, rmb_ws, stream);
		RECORD(events.RmBaseline1End);

		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);

		RECORD(events.RmMeanStart);
		RmMean::run(I_slice, I_slice, mean_ws, stream);
		RECORD(events.RmMeanEnd);

		if (ctx->dump_tmp_I)
			med_loop_I_dumper(I_slice, stream);
#undef RECORD
	}

	for (int i = 0; i < ctx->n_beams; i++)
	{
		auto& events = ctx->events[i];
		nenufrb::StatsMediumLoop stats_med_loop;

		checkCudaErrors(cudaEventSynchronize(events.RmMeanEnd));

		checkCudaErrors(cudaEventElapsedTime(&stats_med_loop.t_mad_clean_10_ms,
			events.MAD10Start, events.MAD10End));
		checkCudaErrors(cudaEventElapsedTime(&stats_med_loop.t_rm_baseline_0_ms,
			events.RmBaseline0Start, events.RmBaseline0End));
		checkCudaErrors(cudaEventElapsedTime(&stats_med_loop.t_mad_clean_3_ms,
			events.MAD3Start, events.MAD3End));
		checkCudaErrors(cudaEventElapsedTime(&stats_med_loop.t_rm_baseline_1_ms,
			events.RmBaseline1Start, events.RmBaseline1End));
		checkCudaErrors(cudaEventElapsedTime(&stats_med_loop.t_rm_mean_ms,
			events.RmMeanStart, events.RmMeanEnd));

		ctx->med_loop_stat_fds[i].write(reinterpret_cast<char*>(&stats_med_loop), sizeof(nenufrb::StatsMediumLoop));
	}

	ctx->med_loop_avail_iters++;

	return EXIT_SUCCESS;
}

struct LooseLoopCoreArgs
{
	nenufrb::Context* ctx;
	std::vector<typename nenufrb::Context::ITensor> I;
	std::vector<typename nenufrb::Context::ATensor> A;
};

int loose_loop_core(void* args)
{
	LooseLoopCoreArgs* _args = reinterpret_cast<LooseLoopCoreArgs*>(args);
	nenufrb::Context* ctx = _args->ctx;

	checkCudaErrors(cudaSetDevice(ctx->cuda_gpu_id));

	while (ctx->med_loop_avail_iters < ctx->med_loop_iters)
	{
		std::this_thread::sleep_for(std::chrono::milliseconds(1)); // Wait for enough iterations to be ready
	}
	ctx->med_loop_avail_iters -= ctx->med_loop_iters; // Consume med loop iters

	for (int i = 0; i < ctx->n_beams; i++)
	{
		auto& I = _args->I[i];
		auto& A = _args->A[i];
		auto& candidates = ctx->candidates[i];
		auto& candidates_dptr = ctx->candidates_dptr[i];
		auto& dmt_cfg = ctx->dmt_cfgs[i];
		auto& stream = ctx->loose_loop_streams[i];

		auto& I_dumper = ctx->I_dumpers[i];
		auto& A_dumper = ctx->A_dumpers[i];
		auto& candidates_dumper = ctx->candidates_dumpers[i];
		auto& dump_stream = ctx->dump_streams[i];

		auto& mad_ws = ctx->mad_ws[i];
		auto& rmb_ws = ctx->rmb_ws[i];
		auto& mean_ws = ctx->mean_ws[i];
		auto& detection_ws = ctx->detection_ws[i];

		auto& events = ctx->events[i];

		checkCudaErrors(cudaStreamWaitEvent(stream, events.RmMeanEnd));

		auto A_slice = cute::make_tensor(
			A.data(),
			cute::make_layout(
				cute::make_shape(cute::shape<0>(A), cute::shape<1>(I)),
				A.stride()
			)
		);

#define RECORD(x) checkCudaErrors(cudaEventRecord((x), stream))

		RECORD(events.ComputeStart);


		RECORD(events.DmtStart);
		BruteforceDmt<Dmt::T0Ref::FMAX>::run(I, A, dmt_cfg, stream);
		RECORD(events.DmtEnd);

		// TODO: add tiling on A to select first N_T_LOOSE_LOOP for detection.
		RECORD(events.DetectionStart);
		Adaptive1D::run(A_slice, candidates_dptr, 3.0f, detection_ws, stream);
		RECORD(events.DetectionEnd);

		RECORD(events.ComputeEnd);

		if (ctx->dump_I)
		{
			checkCudaErrors(cudaStreamWaitEvent(dump_stream, events.RmMeanEnd));
			I_dumper(I, dump_stream);
		}

		if (ctx->dump_A)
		{
			checkCudaErrors(cudaStreamWaitEvent(dump_stream, events.DmtEnd));
			A_dumper(A_slice, dump_stream);
		}
	}


	for (int i = 0; i < ctx->n_beams; i++)
	{
		const auto& candidates = ctx->candidates[i];
		auto& candidates_dumper = ctx->candidates_dumpers[i];
		const auto& dmt_cfg = ctx->dmt_cfgs[i];
		const auto& A = ctx->A[i];
		auto& events = ctx->events[i];

		checkCudaErrors(cudaEventSynchronize(events.DetectionEnd));

		if (ctx->dump_candidates)
			candidates_dumper(candidates);
		
		using RecordT = typename nenufrb::Context::CandidatesTensor::value_type;
		const int n_hit = std::count_if(candidates.data().get(),
				candidates.data().get() + cute::size(candidates),
				[](const RecordT& r) -> bool { return !std::isnan(r.snr); });

		auto max_cand = candidates(0);
		double posix_timestamp = (double) ctx->ref_timestamp * nenufrb::Context::DT_RAW + max_cand.i_w * nenufrb::Context::DT; // Fix race condition on ref_timestamp

		double posix_timestamp_seconds, posix_timestamp_milliseconds;
		posix_timestamp_milliseconds = std::modf(posix_timestamp, &posix_timestamp_seconds);

		std::time_t time(posix_timestamp_seconds);

		static constexpr int STRFTIME_MAX_SIZE = 64;
		char max_hit_time_str[STRFTIME_MAX_SIZE];
		std::strftime(max_hit_time_str, STRFTIME_MAX_SIZE, "%c", std::gmtime(&time));

		std::ostringstream ss_log;
		ss_log << '\n' << "NENUFRB.LOOSE_LOOP: " << n_hit << " hits for beam " << i << " - max: " <<
			"DM = " << max_cand.i_h * (dmt_cfg.DMmax - dmt_cfg.DMmin) / cute::size<0>(A) << "pc.cm-3, t0@fmax = "
				<< max_hit_time_str << " " << posix_timestamp_milliseconds << ", snr = " << max_cand.snr << std::endl;
		rte_log(RTE_LOG_INFO, loose_loop, "%s", ss_log.str().c_str());

		{
			nenufrb::StatsLooseLoop stats_loose_loop;

			checkCudaErrors(cudaEventElapsedTime(&stats_loose_loop.t_dedispersion_ms,
				events.DmtStart, events.DmtEnd));
			checkCudaErrors(cudaEventElapsedTime(&stats_loose_loop.t_detection_ms,
				events.DetectionStart, events.DetectionEnd));
			
			ctx->loose_loop_stat_fds[i].write(reinterpret_cast<char*>(&stats_loose_loop), sizeof(nenufrb::StatsLooseLoop));
		}
	}
#undef RECORD

return 0;
}

int main(int argc, char* argv[])
{
	stop = 0;
	signal(SIGINT,  signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGUSR1, signal_handler);

	nenufrb::Context ctx(argc, argv);


	checkCudaErrors(cudaSetDevice(ctx.cuda_gpu_id));


	RSP_CEP rsp_cep;
	auto data_ring = rsp_cep.data_ring();


	rsp_cep.init(ctx.port, ctx.dpdk_gpu_id);


	// TODO: move data_ring(s) to Context, do not allocate in RSP_CEP
	using DataRingT  = decltype(data_ring);

	/* I can't figure this type, relying on decltype to finish. TODO: do cleaner
	using DataRingsT = cute::Tensor<
		typename DataRingT::engine_type,
		cute::Layout<
			cute::Shape<uint32_t, decltype(cute::shape<1>(data_ring))>,
			decltype(cute::stride(data_ring))
		>
	>;*/

	auto offset = cute::make_coord(0, 0);
	auto dr = subview(data_ring,
		cute::make_shape(static_cast<uint32_t>((ctx.subbands[0].second - ctx.subbands[0].first + 1) * nenufrb::Context::FFTLEN), cute::shape<1>(data_ring)),
		offset);
	using DataRingsT = decltype(dr);

	std::vector<DataRingsT> data_rings;
	data_rings.reserve(ctx.n_beams);

	data_rings.push_back(dr);
	cute::get<0>(offset) += cute::shape<0>(data_rings[0]);

	auto target_layout_raw_data = cute::make_tensor(
		data_rings[0].data(),
		cute::make_layout(
			data_rings[0].shape(),
			cute::GenRowMajor{}
		)
	);
	using RawDumpTensor = decltype(target_layout_raw_data);

	using RawDataDumpT = RawDeviceDump<typename DataRingsT::engine_type,
		typename DataRingsT::layout_type,
		typename RawDumpTensor::engine_type,
		typename RawDumpTensor::layout_type>;

	for (int i = 1; i < ctx.n_beams; i++)
	{
	data_rings.push_back(subview(data_ring,
		cute::make_shape(static_cast<uint32_t>((ctx.subbands[i].second - ctx.subbands[i].first + 1) * nenufrb::Context::FFTLEN), cute::shape<1>(data_ring)),
		offset));
		cute::get<0>(offset) += cute::shape<0>(data_rings[i]);
	}
	
	std::vector<RawDataDumpT> raw_data_dumpers;
	raw_data_dumpers.reserve(ctx.n_beams);
	for (int i = 0; i < ctx.n_beams; i++)
	{
		std::ostringstream ss;
		ss << "raw_data-" << i << ".dump";

		std::ostringstream ss_log;
		ss_log << "NENUFRB.ALLOC: Allocating raw dumper " << ss.str() << " for " << data_rings[i].layout() << std::endl;
		rte_log(RTE_LOG_DEBUG, nenufrb_log, "%s", ss_log.str().c_str());
		target_layout_raw_data = cute::make_tensor(
			data_rings[i].data(),
			cute::make_layout(
				data_rings[i].shape(),
				cute::GenRowMajor{}
			)
		);
		raw_data_dumpers.emplace_back(ctx.out_dir / ss.str(), data_rings[i], target_layout_raw_data);
	}

	rsp_cep.start(ctx.poll_lcore);

	uint64_t last_timestamp = 0UL;

	// Wait for first packets to arrive
	while (!stop && !last_timestamp)
	{
		last_timestamp = rsp_cep.latest_timestamp();

		std::this_thread::sleep_for(1ms);
	}

	ctx.ref_timestamp = last_timestamp;
	if (ctx.timestamp_dump)
	{
		double posix_timestamp = (double) ctx.ref_timestamp * nenufrb::Context::DT_RAW;
		ctx.timestamp_dump.write((char*) &posix_timestamp, sizeof(double));
	}


	MedLoopCoreArgs mlcargs;
	mlcargs.ctx = &ctx;
	mlcargs.I = ctx.I; // Should be a copy

	LooseLoopCoreArgs llcargs;
	llcargs.ctx = &ctx;
	llcargs.I = ctx.I;
	llcargs.A = ctx.A;

	struct rte_eth_stats stats;
	while (!stop)
	{
		uint64_t new_timestamp = rsp_cep.latest_timestamp();

		rte_log(RTE_LOG_DEBUG, nenufrb_timestamp, "NENUFRB.TIMESTAMP: new_timestamp: %lu\n", new_timestamp);

		if (new_timestamp >= ctx.stop_timestamp)
		{
			rte_log(RTE_LOG_INFO, nenufrb_log, "NENUFRB: end of observation\n");
			stop = true;
		}

		if (new_timestamp - last_timestamp >= nenufrb::Context::N_T_RAW)
		{
			static constexpr int max_places_tight_loop = nenufrb::StaticCeilLog10<ctx.tight_loop_iters>::value;
			static constexpr int max_places_med_loop = nenufrb::StaticCeilLog10<ctx.med_loop_iters>::value;

			const std::time_t ts_posix = new_timestamp * 2ull / static_cast<uint64_t>(fsx2);

			static constexpr int STRFTIME_MAX_SIZE = 64;
			char cur_time_str[STRFTIME_MAX_SIZE];

			std::strftime(cur_time_str, STRFTIME_MAX_SIZE, "%c", std::gmtime(&ts_posix));

			rte_eth_stats_get(ctx.port, &stats);
			rte_log(RTE_LOG_INFO, tight_loop, "NENUFRB.TIGHT_LOOP: %s: Received %lu time samples (imissed = %lu) : tight loop : %*u / %u | medium loop : %*u / %u\r",
				cur_time_str,
				nenufrb::Context::N_T_RAW, stats.imissed,
				max_places_tight_loop, ctx.i_tight_loop + 1, ctx.tight_loop_iters,
				max_places_med_loop,   ctx.i_med_loop + 1,   ctx.med_loop_iters);

			for (int i = 0; i < ctx.n_beams; i++)
			{
				auto& raw_data = data_rings[i];
				auto& I = ctx.I[i];
				auto  I_slice = cute::make_tensor(
					I.data(),
					cute::make_layout(
						cute::make_shape(
							cute::shape<0>(I),
							cute::shape<1>(raw_data) / cute::constant<size_t, nenufrb::Context::FOLD_FACTOR>{}
						),
						I.stride()
					)
				);

				auto& stream = ctx.tight_loop_streams[i];
				auto& events = ctx.events[i];

				cute::set_head<1>(raw_data, last_timestamp);
				cute::advance_head<1>(I_slice, ctx.i_med_loop * nenufrb::Context::N_T_MEDIUM_LOOP
					+ ctx.i_tight_loop * nenufrb::Context::N_T_TIGHT_LOOP);

				if (ctx.dump_raw)
				{
					auto& dump_stream = ctx.dump_streams[i];
					raw_data_dumpers[i](raw_data, dump_stream);
				}

#define RECORD(x) checkCudaErrors(cudaEventRecord((x), stream))
				RECORD(events.ReductionStart);
				ChunkedFFT<nenufrb::Context::FFTLEN,
					nenufrb::Context::TIME_INTEGRATION>::run(raw_data, I_slice, stream);
				RECORD(events.ReductionEnd);
#undef RECORD
			}

			ctx.i_tight_loop++;
			if (ctx.i_tight_loop == ctx.tight_loop_iters)
			{
				mlcargs.i_loop = ctx.i_med_loop; // Used to avoid race condition... (TODO: remove i_med_loop from nenufrb::Context)
				mlcargs.I = ctx.I;               // Same, save the state of I to avoid race condition (TODO: do cleaner)
				rte_eal_remote_launch(medium_loop_core, &mlcargs, ctx.med_loop_lcore);

				ctx.i_tight_loop = 0u;

				ctx.i_med_loop++;
				if (ctx.i_med_loop == ctx.med_loop_iters)
				{
					llcargs.I = ctx.I;
					llcargs.A = ctx.A;
					rte_eal_remote_launch(loose_loop_core, &llcargs, ctx.loose_loop_lcore);

					ctx.ref_timestamp = new_timestamp;
			
					if (ctx.timestamp_dump)
					{
						double posix_timestamp = (double) ctx.ref_timestamp * nenufrb::Context::DT_RAW;
						ctx.timestamp_dump.write((char*) &posix_timestamp, sizeof(double));
					}
					ctx.i_med_loop = 0u;

					for (int i = 0; i < ctx.n_beams; i++)
					{
						cute::advance_head<1>(ctx.I[i], nenufrb::Context::N_T_LOOSE_LOOP);
						cute::advance_head<1>(ctx.A[i], nenufrb::Context::N_T_LOOSE_LOOP);
					}
				}
			}

			// Stats (TODO: make optional, but keep synchro at least between tight and loose loops !)
			for (int i = 0; i < ctx.n_beams; i++)
			{
				auto& events = ctx.events[i];
				checkCudaErrors(cudaEventSynchronize(events.ReductionEnd));

				nenufrb::StatsTightLoop stats_tight_loop;
				checkCudaErrors(cudaEventElapsedTime(&stats_tight_loop.t_time_integration_ms,
					events.ReductionStart, events.ReductionEnd));

				ctx.tight_loop_stat_fds[i].write(reinterpret_cast<char*>(&stats_tight_loop), sizeof(nenufrb::StatsTightLoop));
			}

			last_timestamp = new_timestamp;
		}
		else
			std::this_thread::sleep_for(1ms);
	}

	rsp_cep.stop();

	rte_eth_stats_get(ctx.port, &stats);
	rte_log(RTE_LOG_INFO, nenufrb_log, "\nNENUFRB.STATS: ipackets = %lu - imissed = %lu - ierrors = %lu\n", stats.ipackets, stats.imissed, stats.ierrors);


	return EXIT_SUCCESS;
}

