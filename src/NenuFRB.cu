#include <cuda.h>
#include <cuda_runtime.h>

#include <rte_eal.h>
#include <rte_ethdev.h>

#include <signal.h>
#include <getopt.h>

#include "utils/array.cuh"
#include "utils/Bitset.cuh"
#include "Config/ParsetReader.hpp"
#include "Config/DefaultMask.hpp"
#include "Acquisition/RSP_CEP.cuh"
#include "Preprocessing/ChunkedFFT.cuh"
#include "Preprocessing/MADClean.cuh"
#include "Dedispersion/BruteforceDmt.cuh"
#include "Detection/Adaptive1D.cuh"

#include <algorithm>
#include <chrono>
#include <filesystem>
#include <fstream>

#include <thrust/complex.h>

#include <fcntl.h>
#include <unistd.h>
#include <cufile.h>
#include <sys/stat.h>

#include "helper_cuda.cuh"

#define RTE_LOGTYPE_NenuFRB RTE_LOGTYPE_USER1
#define RTE_LOGTYPE_NenuFRB_timestamp RTE_LOGTYPE_USER3

constexpr size_t N_DM = 1024;
constexpr size_t N_T_IN  = 1 << 19;
constexpr size_t N_T_OUT = 1<<19;
constexpr size_t FOLD_FACTOR = 1024;

constexpr float FS_IN = 200e6f / 1024; // Received sampling frequency
constexpr float DT = 1.0f / FS;

volatile bool stop;
void signal_handler(int sig)
{
	switch (sig)
	{
	case SIGINT:
	case SIGTERM:
	case SIGUSR1:
		stop = 1;
		break;
	default:
		RTE_LOG(WARNING, NenuFRB, "Unkown signal %d\n", sig);
	}
}

template <typename Input2DArray_, typename OutT_>
class BaseDumper
{
public:
	using Input2DArray = Input2DArray_;
	using InT          = typename Input2DArray::value_type;
	using OutT         = OutT_;

protected:
	virtual void process_to_buf(const Input2DArray ring, OutT* buf) = 0;
public:
	BaseDumper(std::filesystem::path path, const int64_t N_t, const int64_t max_height)
		: N_t(N_t), max_height(max_height), buf_size(N_t * max_height * sizeof(OutT))
	{
		std::cout << "Preparing dump to " << path << ": buf_size = " << buf_size <<
			" (" << max_height << ", " << N_t << ')' << std::endl;
		checkCudaErrors(cudaMalloc(&data, buf_size));

		fd = open(path.c_str(), O_CREAT | O_WRONLY | O_DIRECT | O_TRUNC, 0644);

		CUfileDescr_t descr;
		memset(&descr, 0, sizeof(CUfileDescr_t));

		descr.handle.fd = fd;
		descr.type = CU_FILE_HANDLE_TYPE_OPAQUE_FD;
		checkCudaErrors(cuFileHandleRegister(&fh, &descr));
		checkCudaErrors(cuFileBufRegister(data, buf_size, 0));
	}

	BaseDumper(const BaseDumper&) = delete;
	BaseDumper(BaseDumper&&) = default;

	virtual void set_stream(cudaStream_t new_stream) final
	{
		stream = new_stream;
	}

	virtual ~BaseDumper()
	{
		checkCudaErrors(cuFileBufDeregister(data));
		cuFileHandleDeregister(fh);
		checkCudaErrors(cudaFree(data));
		close(fd);
	}


	virtual void operator()(const Input2DArray ring) final
	{
		process_to_buf(ring, data);
		checkCudaErrors(cudaStreamSynchronize(stream));
		cuFileWrite(fh, data, buf_size, file_offset, 0);
		// cudaFileWriteAsync should be coming in a future CUDA version...

		file_offset += buf_size;
	}

protected:
	int fd{ 0 };
	const size_t buf_size;
	OutT* data;
	const int64_t N_t, max_height;
	size_t file_offset{ 0ull };
	cudaStream_t stream{ NULL };
	CUfileHandle_t fh;
};

template <typename Input2DArray_>
class RawDump : virtual public BaseDumper<Input2DArray_, typename Input2DArray_::value_type>
{
private:
	using BaseDumper_ = BaseDumper<Input2DArray_, typename Input2DArray_::value_type>;

	using BaseDumper_::N_t;
	using BaseDumper_::stream;

public:
	using Input2DArray = typename BaseDumper_::Input2DArray;
	using InT          = typename BaseDumper_::InT;
	using OutT         = typename BaseDumper_::OutT;

	RawDump(std::filesystem::path path, const int64_t N_t, const int64_t max_height)
		: BaseDumper_(path, N_t, max_height)
	{
	}

protected:
	virtual void process_to_buf(const Input2DArray ring, OutT* buf) override
	{
		memcpy_from_array(buf, ring, N_t, stream);
	}
};


template <typename Input2DArray_>
class HeatmapDump : virtual public BaseDumper<Input2DArray_, typename Input2DArray_::value_type>
{
private:
	using BaseDumper_ = BaseDumper<Input2DArray_, typename Input2DArray_::value_type>;

	using BaseDumper_::stream;

public:
	using Input2DArray = typename BaseDumper_::Input2DArray;
	using InT          = typename BaseDumper_::InT;
	using OutT         = typename BaseDumper_::OutT;

	HeatmapDump(std::filesystem::path path, const int64_t N_t, const int64_t n_t, const int64_t max_height)
		: BaseDumper_(path, n_t, max_height), N_t(N_t)
	{
	}

protected:
	void process_to_buf(const Input2DArray ring, OutT* buf)
	{
		xfold_array<1<<11, NoOp<InT>>(buf, ring, N_t, stream);
	}

private:
	const int64_t N_t;
};

template <typename Input2DArray_>
class HeatmapDumpSqmod : virtual public BaseDumper<Input2DArray_, typename Input2DArray_::value_type::value_type>
{
private:
	using BaseDumper_ = BaseDumper<Input2DArray_, typename Input2DArray_::value_type::value_type>;

	using BaseDumper_::stream;

public:
	using Input2DArray = typename BaseDumper_::Input2DArray;
	using InT          = typename BaseDumper_::InT;
	using OutT         = typename BaseDumper_::OutT;

	HeatmapDumpSqmod(std::filesystem::path path, const int64_t N_t, const int64_t n_t, const int64_t max_height)
		: BaseDumper_(path, n_t, max_height), N_t(N_t)
	{
	}

protected:
	void process_to_buf(const Input2DArray ring, OutT* buf)
	{
		xfold_array<1<<11, SqmodOp<InT>>(buf, ring, N_t, stream);
	}

private:
	const int64_t N_t;
};


using namespace std::chrono_literals;

int main(int argc, char* argv[])
{
	int ret;

	const int n_t = 1 << 8;

	stop = 0;
	signal(SIGINT,  signal_handler);
	signal(SIGTERM, signal_handler);
	signal(SIGUSR1, signal_handler);

	ret = rte_eal_init(argc, argv);
	if (ret < 0)
	{
		RTE_LOG(ERR, NenuFRB, "Cannot init EAL\n");
		return EXIT_FAILURE;
	}

	RTE_LOG(DEBUG, NenuFRB, "Successfully init EAL\n");

	argc -= ret;
	argv += ret;

	bool dump_raw(false), dump_I(false), dump_A(false);
	bool cropped(true);
	bool help(false);
	while (true)
	{
		static struct option long_options[] =
		{
			{ "dump_raw",   no_argument, 0, 'r' },
			{ "dump_I",     no_argument, 0, 'I' },
			{ "dump_A",     no_argument, 0, 'A' },
			{ "help",       no_argument, 0, 'h' },
			{ "dont-crop",  no_argument, 0, 'c' },
			{ 0, 0, 0, 0 }
		};

		int option_index = 0;

		int c = getopt_long(argc, argv, "h", long_options, &option_index);
		if (c == -1)
			break;

		switch (c)
		{
		case 'r':
			dump_raw = true;
			break;
		case 'I':
			dump_I = true;
			break;
		case 'A':
			dump_A = true;
			break;
		case 'h':
			help = true;
			break;
		case 'c':
			cropped = false;
			break;
		default:
			RTE_LOG(ERR, NenuFRB, "Failed to parse command line\n");
			return EXIT_FAILURE;
		}
	}

	if (help)
	{
		std::printf(R"help(Usage: ./NenuFRB [EAL args] -- [NenuFRB args] parset_file
FRB detection for NenuFAR
Listens on the NIC detected by DPDK for RSP CEP frames, and apply a simple detection pipeline:
Incoherent Dedispersion -> Detection.

EAL arguments are described here: https://doc.dpdk.org/guides/linux_gsg/linux_eal_parameters.html
NenuFRB arguments are:

--dump_raw                        Dump raw TF data in a file
--dump_I                          Dump TF data after preprocessing in a file
--dump_A                          Dump dedispersed data in a file
--help                            Print this message
)help");
		return EXIT_SUCCESS;
	}

	if (optind != argc - 1)
	{
		if (optind < argc - 1)
			RTE_LOG(ERR, NenuFRB, "Missing non-option argument parset file\n");
		else if (optind > argc - 1)
			RTE_LOG(ERR, NenuFRB, "Trailing arguments\n");
		return EXIT_FAILURE;
	}

	std::filesystem::path parset_file(argv[optind]);
	if (!std::filesystem::exists(parset_file))
	{
		RTE_LOG(ERR, NenuFRB, "Unknown file %s\n", parset_file.c_str());
		return EXIT_FAILURE;
	}

	ParsetReader pr(parset_file);
	int n_beams = pr.n_elems("Beam");
	std::vector<std::pair<int, int>> subbands(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		std::ostringstream ss;
		ss << "Beam[" << i << ']';
		subbands[i] = pr.get(ss.str(), "subbandList").as_range();
	}


	unsigned nb_lcores = rte_lcore_count();
	if (nb_lcores < 2)
	{
		RTE_LOG(ERR, NenuFRB, "At least 2 lcores must be available, found %u\n", nb_lcores);
		return EXIT_FAILURE;
	}

	const int16_t port = rte_eth_find_next(0);
	const int16_t dpdk_gpu_id = rte_gpu_find_next(0, RTE_GPU_ID_ANY);
	const unsigned poll_lcore = rte_get_next_lcore(0, true, false);

	// Recover CUDA device id from PCI id
	int cuda_gpu_id;
	struct rte_gpu_info gpu_info;
	rte_gpu_info_get(dpdk_gpu_id, &gpu_info);
	checkCudaErrors(cudaDeviceGetByPCIBusId(&cuda_gpu_id, gpu_info.name));

	checkCudaErrors(cudaSetDevice(cuda_gpu_id));

	int lowestPriority, greatestPriority;
	checkCudaErrors(cudaDeviceGetStreamPriorityRange(&lowestPriority, &greatestPriority));
	const int nPriorities = lowestPriority - greatestPriority + 1;

	std::vector<cudaStream_t> main_streams(n_beams);
	for (cudaStream_t& s : main_streams)
		checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestPriority + 1 * nPriorities / 3));

	float* A;
	checkCudaErrors(cudaMalloc(&A, n_beams * N_DM * N_T_OUT * sizeof(float)));
	checkCudaErrors(cudaMemset(A, 0, n_beams * N_DM * N_T_OUT * sizeof(float)));

	using ARingType = RingAdapter2D<N_T_OUT, float>;

	std::vector<ARingType> A_rings(n_beams);
	for (int i = 0; i < n_beams; i++)
		A_rings[i].set_data(A + i * N_DM * N_T_OUT, N_DM);



	struct Record* candidates, * candidates_dptr;
	checkCudaErrors(cudaHostAlloc(&candidates, n_beams * N_DM * sizeof(struct Record), cudaHostAllocMapped));
	checkCudaErrors(cudaHostGetDevicePointer(&candidates_dptr, candidates, 0));


	int n_SM;
	checkCudaErrors(cudaDeviceGetAttribute(&n_SM, cudaDevAttrMultiProcessorCount, cuda_gpu_id));

	RSP_CEP rsp_cep;
	rsp_cep.init(port, dpdk_gpu_id);
	auto data_ring = rsp_cep.get_data_ring();
	auto header_ring = rsp_cep.get_header_ring();

	using DataRingType = decltype(data_ring);

	ChunkedFFT<256> chunked_fft;

	std::vector<Dmt::Config> dmt_cfgs(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		Dmt::Config& cfg = dmt_cfgs[i];

		cfg.N_t_in  = I_N_T;
		cfg.N_t_out = N_T_OUT;
		cfg.N_f     = (subbands[i].second - subbands[i].first + 1) * FFTLEN;
		cfg.N_dm    = N_DM;

		cfg.fmin    = subbands[i].first  / DT / 1e6f;
		cfg.fmin    = subbands[i].second / DT / 1e6f;
		cfg.DMmin   = 0;
		cfg.DMmax   = 100;
		cfg.dt      = DT;
	}
	std::vector<int> freq_offsets(n_beams);
	freq_offsets[0] = 0;
	for (int i = 1; i < n_beams; i++)
		freq_offsets[i] = freq_offsets[i - 1] + dmt_cfgs[i - 1].N_f / FFTLEN;

	std::vector<DataRingType> data_rings(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		data_rings[i] = data_ring.subview(freq_offsets[i], dmt_cfgs[i].N_f / FFTLEN);
	}



	// Get a subview on range 40-70 MHz to limit dispersive effects
	const int subband_min = 40 * 1024 / 200;
	const int subband_max = 70 * 1024 / 200;

	std::vector<DataRingType> cropped_data_view(n_beams);
	if (cropped)
	{
		for (int i = 0; i < n_beams; i++)
		{
			const int idx_min = min(max(subband_min - subbands[i].first, 0), (int)data_rings[i].height());
			const int idx_max = min(max(subband_max - subbands[i].first, 0), (int)data_rings[i].height());

			cropped_data_view[i] = data_rings[i].subview(idx_min, idx_max - idx_min);

			dmt_cfgs[i].N_f = (idx_max - idx_min) * FFTLEN; // Fix dmt_cfg with the updated parameters
			dmt_cfgs[i].fmin = (idx_min + subbands[i].first - 0.5f) * 200.0f / 1024.0f; // +- 0.5 because subbands contain central frequencies
			dmt_cfgs[i].fmax = (idx_max + subbands[i].first + 0.5f) * 200.0f / 1024.0f;

			RTE_LOG(DEBUG, NenuFRB, "Cropped beam: %f - %f MHz (%d - %d)\n", dmt_cfgs[i].fmin, dmt_cfgs[i].fmax, idx_min, idx_max);

			if (dmt_cfgs[i].N_f == 0)
				RTE_LOG(WARNING, NenuFRB, "No frequency in range 40-70 MHz for beam %d\n", i);
		}
	}


	std::vector<DataRingType*> chosen_data(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		chosen_data[i] = cropped ? &cropped_data_view[i] : &data_rings[i];
	}

	std::vector<uint64_t> I_offsets(n_beams+1, 0);
	for (int i = 0; i < n_beams; i++)
		I_offsets[i + 1] = I_offsets[i] + dmt_cfgs[i].N_f * I_N_T;


	float* I;
	checkCudaErrors(cudaMalloc(&I, I_offsets.back() * sizeof(float)));
	
	using IRingType = RingAdapter2D<I_N_T, float>;

	std::vector<IRingType> I_rings(n_beams);
	for (int i = 0; i < n_beams; i++)
	{
		I_rings[i].set_data(I + I_offsets[i], dmt_cfgs[i].N_f);
		RTE_LOG(DEBUG, NenuFRB, "I_offset = %lu, N_f = %d\n", I_offsets[i], dmt_cfgs[i].N_f);
	}


	MADClean<decltype(I_rings[0].cons_to_cute<N_T_IN / FFTLEN>())> mad_clean;
	std::vector<size_t> mad_ws_sizes(n_beams, 0ull);
	std::vector<size_t> rmb_ws_sizes(n_beams, 0ull);
	for (int i = 0; i < n_beams; i++)
	{
		auto I_ring_cute = I_rings[i].cons_to_cute<N_T_IN / FFTLEN>();
		mad_clean(I_ring_cute, I_ring_cute, 10, nullptr, mad_ws_sizes[i], main_streams[i]);
		RmBaseline::run<FFTLEN>(I_ring_cute, I_ring_cute, nullptr, nullptr, rmb_ws_sizes[i], main_streams[i]);
	}
	void* workspace;
	checkCudaErrors(cudaMalloc(&workspace, 
		std::accumulate(mad_ws_sizes.cbegin(), mad_ws_sizes.cend(), 0ull)
	  + std::accumulate(rmb_ws_sizes.cbegin(), rmb_ws_sizes.cend(), 0ull)));

	std::vector<void*> mad_workspaces(n_beams), rmb_workspaces(n_beams);
	mad_workspaces[0] = workspace;
	rmb_workspaces[0] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mad_workspaces[0]) + mad_ws_sizes[0]);

	for (int i = 1; i < n_beams; i++)
	{
		mad_workspaces[i] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(rmb_workspaces[i-1]) + rmb_ws_sizes[i-1]);
		rmb_workspaces[i] = reinterpret_cast<void*>(reinterpret_cast<uintptr_t>(mad_workspaces[i])   + mad_ws_sizes[i]);
	}

	Bitset<N_F> mask;
	mask.set_all();

	int n_masks = default_mask.n_elems("Mask");
	for (int i = 0; i < n_masks; i++)
	{
		std::ostringstream ss;
		ss << "Mask[" << i << ']';
		auto mask_elem = default_mask.get(ss.str(), "subbandList").as_range();

		for (int j = 0; j < n_beams; j++)
		{
			for (int k = mask_elem.first; k <= mask_elem.second; k++)
			{
				const int idx = k - subbands[j].first + freq_offsets[j]; // Subbands and masks does not begin at 0, but our arrays do
				if ((0 <= idx) && (idx < N_F))
					mask.reset(idx);
			}
		}
	}

	rsp_cep.set_mask(mask);

	std::vector<BruteforceDmt> dmts;
	std::vector<Adaptive1D> detectors;

	dmts.reserve(n_beams);
	detectors.reserve(n_beams);

	for (int i = 0; i < n_beams; i++)
	{
		dmts.emplace_back(dmt_cfgs[i]);
		detectors.emplace_back();

		dmts[i].set_stream(main_streams[i]);
		detectors[i].set_stream(main_streams[i]);
	}

	std::vector<cudaEvent_t> e_prep(n_beams);
	for (int i = 0; i < n_beams; i++)
		checkCudaErrors(cudaEventCreate(&e_prep[i], cudaEventDefault));

	std::vector<cudaEvent_t> e_dmts(n_beams);
	for (int i = 0; i < n_beams; i++)
		checkCudaErrors(cudaEventCreate(&e_dmts[i], cudaEventDefault));

	std::vector<cudaEvent_t> e_detection(n_beams);
	for (int i = 0; i < n_beams; i++)
		checkCudaErrors(cudaEventCreate(&e_detection[i], cudaEventDefault));



	std::filesystem::path dump_dir = parset_file.stem();
	std::filesystem::create_directory(dump_dir, std::filesystem::current_path());
	struct stat cwd_stat;
	stat(std::filesystem::current_path().c_str(), &cwd_stat);
	chown(dump_dir.c_str(), cwd_stat.st_uid, cwd_stat.st_gid);
	std::filesystem::copy_file(parset_file, dump_dir / parset_file.filename(), std::filesystem::copy_options::overwrite_existing);

	std::vector<cudaStream_t> dump_streams(n_beams);
	if (dump_raw || dump_I || dump_A)
	{
		for (auto& s : dump_streams)
			checkCudaErrors(cudaStreamCreateWithPriority(&s, cudaStreamNonBlocking, greatestPriority + 2 * nPriorities / 3));
	}

	checkCudaErrors(cuFileDriverOpen());
	std::vector<HeatmapDumpSqmod<DataRingType>> data_dumpers;
	std::vector<RawDump<IRingType>>        I_dumpers;
	std::vector<RawDump<ARingType>>        A_dumpers;

	if (dump_raw)
	{
		data_dumpers.reserve(n_beams);
		for (int i = 0; i < n_beams; i++)
		{
			std::stringstream ss;
			ss << "data_" << i << ".dump";
			data_dumpers.emplace_back(dump_dir / ss.str(), N_T_IN, n_t, chosen_data[i]->height());
			data_dumpers[i].set_stream(dump_streams[i]);
		}
	}

	if (dump_I)
	{
		I_dumpers.reserve(n_beams);
		for (int i = 0; i < n_beams; i++)
		{
			std::stringstream ss;
			ss << "I_" << i << ".dump";
			I_dumpers.emplace_back(dump_dir / ss.str(), N_T_IN/FFTLEN, I_rings[i].height());
			I_dumpers[i].set_stream(dump_streams[i]);
		}
	}

	if (dump_A)
	{
		A_dumpers.reserve(n_beams);
		for (int i = 0; i < n_beams; i++)
		{
			std::stringstream ss;
			ss << "A_" << i << ".dump";
			A_dumpers.emplace_back(dump_dir / ss.str(), I_N_T, A_rings[i].height());
			A_dumpers[i].set_stream(dump_streams[i]);
		}
	}


	rsp_cep.start(poll_lcore);

	uint64_t last_timestamp = 0UL;
	while (!stop && !last_timestamp)
	{
		last_timestamp = rsp_cep.get_latest_timestamp();

		std::this_thread::sleep_for(1ms);
	}


	struct rte_eth_stats stats;
	uint32_t I_counter = 0u;
	while (!stop)
	{
		uint64_t new_timestamp = rsp_cep.get_latest_timestamp();

		RTE_LOG(DEBUG, NenuFRB_timestamp, "new_timestamp: %lu\n", new_timestamp);
		if (new_timestamp - last_timestamp >= N_T_IN)
		{
			rte_eth_stats_get(port, &stats);
			RTE_LOG(INFO, NenuFRB, "Received %lu time samples (imissed = %lu)\n", N_T_IN, stats.imissed);

			for (int i = 0; i < n_beams; i++)
			{
				chosen_data[i]->set_cons_head(last_timestamp);
				chunked_fft(*chosen_data[i], I_rings[i], N_T_IN, main_streams[i]);

				if (dump_I) // DEBUG
				{
					checkCudaErrors(cudaStreamWaitEvent(dump_streams[i], e_prep[i], cudaEventWaitDefault));
					I_dumpers[i](I_rings[i]);
				}
				auto I_ring_cute = I_rings[i].cons_to_cute<N_T_IN / FFTLEN>();
				auto [median, mad] = mad_clean(I_ring_cute, I_ring_cute, 10, mad_workspaces[i], mad_ws_sizes[i], main_streams[i]);
				RmBaseline::run<FFTLEN>(I_ring_cute, I_ring_cute, median, rmb_workspaces[i], rmb_ws_sizes[i], main_streams[i]);
				std::tie(median, mad) = mad_clean(I_ring_cute, I_ring_cute, 3, mad_workspaces[i], mad_ws_sizes[i], main_streams[i]);
				RmBaseline::run<FFTLEN>(I_ring_cute, I_ring_cute, median, rmb_workspaces[i], rmb_ws_sizes[i], main_streams[i]);

				checkCudaErrors(cudaEventRecord(e_prep[i], main_streams[i]));
				I_rings[i].advance_prod_head(N_T_IN / FFTLEN);

				if (dump_raw)
				{
					data_dumpers[i](*chosen_data[i]);
				}

				if (dump_I)
				{
					checkCudaErrors(cudaStreamWaitEvent(dump_streams[i], e_prep[i], cudaEventWaitDefault));
					I_dumpers[i](I_rings[i]);
				}
			}

			I_counter++;

			if (I_counter >= I_N_T * FFTLEN / N_T_IN)
			{
				RTE_LOG(INFO, NenuFRB, "Received %lu sets of time samples, performing dedispersion\n", I_N_T * FFTLEN / N_T_IN);
				I_counter = 0u;
				for (int i = 0; i < n_beams; i++)
				{
					dmts[i].dmt(I_rings[i], A_rings[i]);
					checkCudaErrors(cudaEventRecord(e_dmts[i], main_streams[i]));
					I_rings[i].advance_cons_head(I_N_T);
					A_rings[i].advance_prod_head(I_N_T);

					if (dump_A)
					{
						checkCudaErrors(cudaStreamWaitEvent(dump_streams[i], e_dmts[i], cudaEventWaitDefault));
						A_dumpers[i](A_rings[i]);
					}
				}


				for (int i = 0; i < n_beams; i++)
				{
					detectors[i].detect(A_rings[i], candidates_dptr + i * N_DM, I_N_T, I_N_T);
					checkCudaErrors(cudaEventRecord(e_detection[i], main_streams[i]));
					A_rings[i].advance_cons_head(I_N_T);

				}


				for (int i = 0; i < n_beams; i++)
				{
					checkCudaErrors(cudaStreamSynchronize(main_streams[i]));
					const int n_hit = std::count_if(candidates + i * N_DM,
							candidates + (i+1) * N_DM,
							[](const struct Record& r) -> bool { return !std::isnan(r.i_h) && !std::isnan(r.i_w); });

					float t_dmt;
					checkCudaErrors(cudaEventElapsedTime(&t_dmt, e_prep[i], e_dmts[i]));
					RTE_LOG(INFO, NenuFRB, "%d hits for beam %d (dmt: %f s)\n", n_hit, i, t_dmt * 1e-3f);
				}
			}

			last_timestamp = new_timestamp;
		}
		else
			std::this_thread::sleep_for(1ms);
	}

	for (int i = 0; i < n_beams; i++)
		checkCudaErrors(cudaStreamSynchronize(dump_streams[i]));

	rsp_cep.stop();
	for (int i = 0; i < n_beams; i++)
	{
		checkCudaErrors(cudaStreamDestroy(main_streams[i]));
		checkCudaErrors(cudaStreamDestroy(dump_streams[i]));
		checkCudaErrors(cudaEventDestroy(e_prep[i]));
		checkCudaErrors(cudaEventDestroy(e_dmts[i]));
		checkCudaErrors(cudaEventDestroy(e_detection[i]));
	}


	checkCudaErrors(cudaFree(workspace));
	checkCudaErrors(cudaFreeHost(candidates));
	checkCudaErrors(cudaFree(A));


	rte_eth_stats_get(port, &stats);

	RTE_LOG(INFO, NenuFRB, "Stats: ipackets = %lu - imissed = %lu - ierrors = %lu\n", stats.ipackets, stats.imissed, stats.ierrors);


	return EXIT_SUCCESS;
}

